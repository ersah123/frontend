import fs from 'fs';
import path from 'path';
import ejs from 'ejs';
import chalk from 'chalk';
import inquirer from 'inquirer';
import kebabCase from 'lodash.kebabcase';

async function createComponent() {
  const info = message => console.error(chalk.cyan(message));
  const warning = message => console.error(chalk.yellow(message));

  const questions = [
    {
      name: 'name',
      message: 'Component name',
      default: 'ComponentName',
      validate(value) {
        const valid = /^[A-Z][a-zA-Z]+$/.test(value);
        return valid || 'Component name should match /^[A-Z][a-zA-Z]+$/';
      },
    },
    {
      name: 'isSubcomponent',
      type: 'confirm',
      message: 'Is it a Sub-Component? (IE: collection/Form/FormInput).',
      default: false,
    },
    {
      type: 'list',
      name: 'type',
      when: a => a.isSubcomponent,
      message: 'Parent component type:',
      choices: [
        'elements',
        'collections',
        'views',
        'modules',
        'behaviors',
        'addons',
      ],
      default: 'elements',
    },
    {
      name: 'subcomponent',
      message: 'Type the parent name (IE: Form):',
      when: a => a.isSubcomponent,
      validate(value) {
        const valid = /^[A-Z][a-zA-Z]+$/.test(value);
        return valid || 'Component name should match /^[A-Z][a-zA-Z]+$/';
      },
      default: '',
    },
    {
      type: 'list',
      name: 'type',
      when: a => !a.isSubcomponent,
      message: 'Component type:',
      choices: [
        'elements',
        'collections',
        'views',
        'modules',
        'behaviors',
        'addons',
      ],
      default: 'elements',
    },
    {
      name: 'styles',
      type: 'confirm',
      message: 'With styles?',
      default: true,
    },
  ];

  let src = path.resolve(__dirname, '../src/components');
  const templates = path.resolve(__dirname, './templates');

  function render(options, template, out) {
    const extendedOptions = Object.assign({}, options, {
      dashedName: kebabCase(options.name),
    });
    chalk.blue(extendedOptions);
    const input = path.join(templates, template);
    const output = path.join(src, options.name, out);

    const code = ejs.render(fs.readFileSync(input, 'utf8'), extendedOptions);
    fs.writeFileSync(output, code);
  }

  const options = await inquirer.prompt(questions);

  // Is Subcomponent
  options.subcomponent = options.isSubcomponent
    ? `/${options.subcomponent}`
    : '';

  src = path.resolve(
    __dirname,
    `../src/components/${options.type}${options.subcomponent}`,
  );

  const dirName = path.join(src, options.name);

  // TODO: Make the "type" folder or "parent" folder if not exists
  try {
    fs.mkdirSync(dirName);
  } catch (e) {
    if (e.code === 'EEXIST') {
      warning(`Component ${options.name} already exists!`);
    } else {
      throw e;
    }
  }

  render(options, 'README.md', `${options.name}.md`);
  render(options, 'Component.js', `${options.name}.js`);
  render(options, 'package.json', `package.json`);
  if (options.styles) {
    render(options, 'Component.css', `${options.name}.css`);
  }

  info(`Component ${options.name} successfully created!`);
}

export default createComponent;
