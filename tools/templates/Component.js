import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
<% if (styles) { -%>
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './<%= name %>.css';

@themableWithStyles(styles)
<% } else { -%>

<% } -%>
class <%= name %> extends PureComponent {
  static propTypes = {
<% if (styles) { -%>
    /**
     * @ignore
     */
    className: PropTypes.string,
<% } -%>
    /**
     * The content of the component.
     */
    children: PropTypes.node,
<% if (styles) { -%>
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
<% } -%>
  };

  static defaultProps = {
<% if (styles) { -%>
    className: null,
<% } -%>
    children: null,
<% if (styles) { -%>
    css: {
      root: styles.root,
    },
<% } -%>
  };

  render() {
    const {
<% if (styles) { -%>
      className: classNameProp,
<% } -%>
      children,
<% if (styles) { -%>
      css,
<% } -%>
      // The rest values
      ...rest
    } = this.props;

<% if (styles) { -%>
    const className = classNames(css.root, classNameProp);
<% } -%>
<% if (styles) { -%>
    return (
      <div className={className} {...rest}>
        {children}
      </div>
    );
<% } else { -%>
    return <div {...rest}>{children}</div>;
<% } -%>
  }
}

export default <%= name %>;
