import path from 'path';
import fs from 'fs';
import webpack from 'webpack';
import find from 'find';
import { sync as symlinkOrCopySync } from 'symlink-or-copy';

import { makeDir, cleanDir } from './lib/fs';
import { theme, styles } from './styleguide/styles';
import pkg from '../package.json';

import babelrc from '../babel.config';

const ROOT_DIR = path.resolve(__dirname, '..');
const resolvePath = (...args) => path.resolve(ROOT_DIR, ...args);
const SRC_DIR = resolvePath('src');
const TOOLS_DIR = resolvePath('tools');
const RSK_DIR = resolvePath('tools/.temp/rsk-components');

const reScript = /\.(js|jsx|mjs)$/;
const reStyle = /\.(css|less|styl|scss|sass|sss)$/;
const reImage = /\.(bmp|gif|jpg|jpeg|png|svg)$/;
const staticAssetName = '[hash:8].[ext]';

const cacheComponentList = {};

/**
 * Create the .temp dir and symlink (copy on windows) the rsk-components, for styleguide compatibility
 */
(async () => {
  await Promise.all([
    cleanDir('tools/.temp/*', {
      nosort: true,
      dot: true,
      ignore: ['tools/.git'],
    }),
  ]);
  await makeDir('tools/.temp');
  await symlinkOrCopySync(
    resolvePath('node_modules/rsk-components/src'),
    RSK_DIR,
  );
})();

/**
 * Search for each component in the rsk-components and components,
 * it will look for the package.json to determine which is an actual component.
 */
function listOfUrls(componentFolder) {
  const comp = find.fileSync(
    new RegExp(
      `(?:.*)(?:components/${componentFolder}).([A-z.]+).*(package.json)$`,
      'i',
    ),
    SRC_DIR,
  );

  const rsk = find.fileSync(
    new RegExp(
      `(?:.*)(?:./.temp/rsk-components/${componentFolder}).([A-z.]+).*(package.json)$`,
      'i',
    ),
    RSK_DIR,
  );

  // Only load in the files with a package.json
  const list = Object.assign(
    {},
    ...[...rsk, ...comp].map(packagePath => {
      const { name, main } = JSON.parse(fs.readFileSync(packagePath, 'utf8'));
      const componentPath = packagePath.replace(
        'package.json',
        main.replace('./', ''),
      );
      return { [name]: componentPath };
    }),
  );

  return Object.keys(list).map(val => list[val]);
}

// Cache the component url list in an object
function cacheComponents(compName) {
  if ({}.hasOwnProperty.call(cacheComponentList, compName)) {
    return cacheComponentList[compName];
  }

  cacheComponentList[compName] = listOfUrls(compName);

  return cacheComponentList[compName];
}

// The list with the function to create the url list of the components to load
const componentList = [
  {
    name: 'Components',
    sections: [
      {
        name: 'Elements',
        components() {
          return cacheComponents('elements');
        },
      },
      {
        name: 'Collections',
        components() {
          return cacheComponents('collections');
        },
      },
      {
        name: 'Views',
        components() {
          return cacheComponents('views');
        },
      },
      {
        name: 'Modules',
        components() {
          return cacheComponents('modules');
        },
      },
      {
        name: 'Behaviors',
        components() {
          return cacheComponents('behaviors');
        },
      },
      {
        name: 'Addons',
        components() {
          return cacheComponents('addons');
        },
      },
    ],
  },
];

module.exports = {
  require: ['@babel/polyfill', resolvePath('./tools/styleguide/styles.css')],
  title: pkg.name,
  version: pkg.version,
  editorConfig: {
    theme: 'dracula', // future config
  },
  theme,
  styles,
  pagePerSection: true,
  getComponentPathLine(componentPath) {
    const name = path.basename(componentPath, '.js');

    let componentFolder = 'components';

    if (componentPath.includes('rsk-components')) {
      componentFolder = 'rsk-components';
    }

    const componentTypeFolder = componentPath
      .split(componentFolder)[1]
      .split(name)[0]
      .slice(0, -1);

    return `import { ${name} } from '${componentFolder}${componentTypeFolder}';`;
  },
  // Override Styleguidist components
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'styleguide/components/Wrapper/Wrapper.js'),
  },
  styleguideDir: path.join(__dirname, '../styleguide'),
  webpackConfig: {
    resolve: {
      symlinks: false,
      alias: {
        'rsk-components': RSK_DIR,
      },
      // Allow absolute paths in imports, e.g. import Button from 'components/Button'
      // Keep in sync with .flowconfig, .eslintrc, jest.config.js and styleguide.config.js
      modules: ['node_modules', 'src'],
    },
    plugins: [
      // Define free variables
      // https://webpack.js.org/plugins/define-plugin/
      new webpack.DefinePlugin({
        'process.env.BROWSER': true,
        __DEV__: false,
        __TEST__: false,
      }),
    ],
    module: {
      rules: [
        {
          test: reScript,
          loader: 'babel-loader',
          include: [SRC_DIR, TOOLS_DIR],
          options: {
            presets: babelrc.presets,
            plugins: babelrc.plugins,
          },
        },
        {
          test: reStyle,
          loader: 'style-loader',
        },
        {
          test: reStyle,
          loader: 'css-loader',
          options: {
            sourceMap: true,
            // CSS Modules https://github.com/css-modules/css-modules
            modules: true,
            localIdentName: '[name]-[local]-[hash:base64:5]',
          },
        },
        {
          test: reStyle,
          loader: 'postcss-loader',
          options: {
            config: {
              path: './tools/postcss.config.js',
            },
          },
        },

        // Rules for images, if < 4kb put them inline
        {
          test: reImage,
          oneOf: [
            // Inline lightweight images into CSS
            {
              issuer: reStyle,
              oneOf: [
                // Inline lightweight SVGs as UTF-8 encoded DataUrl string
                {
                  test: /\.svg$/,
                  loader: 'svg-url-loader',
                  options: {
                    name: staticAssetName,
                    limit: 4096, // 4kb
                  },
                },

                // Inline lightweight images as Base64 encoded DataUrl string
                {
                  loader: 'url-loader',
                  options: {
                    name: staticAssetName,
                    limit: 4096, // 4kb
                  },
                },
              ],
            },

            // Or return public URL to image resource
            {
              loader: 'file-loader',
              options: {
                name: staticAssetName,
              },
            },
          ],
        },
        {
          test: /\.txt$/,
          loader: 'raw-loader',
        },

        // Return public URL for all assets unless explicitly excluded
        // DO NOT FORGET to update `exclude` list when you adding a new loader
        {
          exclude: [reScript, reStyle, reImage, /\.json$/, /\.txt$/, /\.md$/],
          loader: 'file-loader',
        },
      ],
    },
  },
  sections: [
    {
      name: 'Documentation',
      sections: [
        {
          name: 'Getting Started',
          content: '../docs/getting-started.md',
        },
        {
          name: 'Terminology',
          content: '../docs/terminology.md',
        },
        {
          name: 'Configure text editors',
          content: '../docs/how-to-configure-text-editors.md',
        },
        {
          name: 'React style guide',
          content: '../docs/react-code-style-guide.md',
        },
        {
          name: 'How to fetch data',
          content: '../docs/how-to-fetch-data.md',
        },
        {
          name: 'Multilanguage',
          content: '../docs/how-to-use-multilanguage.md',
        },
        {
          name: 'Using Redux',
          content: '../docs/how-to-use-redux.md',
        },
        {
          name: 'Routing',
          content: '../docs/how-to-use-routing.md',
        },
        {
          name: 'Build & deploy',
          content: '../docs/how-to-build-and-deploy.md',
        },
        {
          name: 'Recipes',
          sections: [
            {
              name: 'External social service',
              content:
                '../docs/recipes/how-to-integrate-external-social-service.md',
            },
            {
              name: 'Integrate React-Intl',
              content: '../docs/recipes/how-to-integrate-react-intl.md',
            },
            {
              name: 'Integrate Redux',
              content: '../docs/recipes/how-to-integrate-redux.md',
            },
          ],
        },
      ],
    },
    ...componentList,
  ],
};
