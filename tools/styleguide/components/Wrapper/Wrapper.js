import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { IntlProvider } from 'react-intl';

import { Switcher } from 'rsk-components';
import configureStore from '../../../../src/store/configureStore';
import Layout from '../../../../src/components/globals/Layout';

import s from './Wrapper.css';

import createSequence from '../../utils/createSequence';

const store = configureStore({}, { fetch });

const seq = createSequence();

class Wrapper extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isTransparent: false,
    };

    this.id = `background_toggler_${seq.next()}`;
  }

  handleBackgroundToggle = isTransparent => this.setState({ isTransparent });

  render() {
    const { children } = this.props;
    const { isTransparent } = this.state;
    const wrapperClassName = classNames(
      s.wrapper,
      isTransparent ? s.transparent : s.white,
    );

    return (
      <Provider store={store}>
        <IntlProvider locale="en">
          <div className={s.container}>
            <header className={s.header}>
              <Switcher
                id={this.id}
                name="toggle"
                className={s.toggle}
                value={isTransparent}
                onChange={this.handleBackgroundToggle}
                label="Transparent background"
              />
            </header>
            <div className={wrapperClassName}>
              <Layout contentOnly>{children}</Layout>
            </div>
          </div>
        </IntlProvider>
      </Provider>
    );
  }
}

export default Wrapper;
