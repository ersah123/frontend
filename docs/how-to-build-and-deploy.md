### Build and deploy, manual to Azure

Make sure the git build repository is hooked up to the Azure environments.
Check your package.json and look for

```md
"deploymentRepository": "https://REPO_DOMAIN/SOME_REPO.git",
```

Run deploy to specific branch (test)

```bash
$ DEPLOY_BRANCH=test yarn deploy
```

This will deploy to the specific branch in the repo that is defined in the
packge.json.

---

### Build and deploy, automated setup

#### 1. Bamboo

There is a bamboo.yml in the bamboo-specs folder this will be used by Bamboo
to setup the build plan for testing and linting the code.
