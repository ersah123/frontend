const fs = require('fs');
const { EOL } = require('os');
const stripAnsi = require('strip-ansi');

const filename = process.env.JEST_REPORT_FILE || 'mocha.json';

const output = {
  stats: {},
  failures: [],
  passes: [],
  skipped: [],
};

const formatErrorMessages = errorMessages => {
  const lines = [];

  if (errorMessages.length === 1) {
    lines.push('1 failure:');
  } else {
    lines.push(`${errorMessages.length} failures:`);
  }

  errorMessages.forEach(message => {
    lines.push(`* ${message}`);
  });

  return lines.join(EOL);
};

module.exports = results => {
  output.stats.tests = results.numTotalTests;
  output.stats.passes = results.numPassedTests;
  output.stats.failures = results.numFailedTests;
  output.stats.duration = Date.now() - results.startTime;
  output.stats.start = new Date(results.startTime);
  output.stats.end = new Date();

  const existingTestTitles = Object.create(null);

  results.testResults.forEach(suiteResult => {
    suiteResult.testResults.forEach(testResult => {
      const suiteName = stripAnsi(testResult.ancestorTitles[0] || suiteResult.testFilePath); // eslint-disable-line prettier/prettier
      let testTitle = stripAnsi(testResult.fullName); // eslint-disable-line prettier/prettier

      if (testTitle in existingTestTitles) {
        let newTestTitle;
        let counter = 1;
        do {
          counter += 1;
          newTestTitle = `${testTitle} (${counter})`;
        } while (newTestTitle in existingTestTitles);
        testTitle = newTestTitle;
      }

      existingTestTitles[testTitle] = true;

      const result = {
        title: testTitle,
        fullTitle: suiteName,
        duration: suiteResult.perfStats.end - suiteResult.perfStats.start,
        errorCount: testResult.failureMessages.length,
        error: testResult.failureMessages.length
          ? formatErrorMessages(testResult.failureMessages)
          : undefined,
      };

      switch (testResult.status) {
        case 'passed':
          output.passes.push(result);
          break;
        case 'failed':
          output.failures.push(result);
          break;
        case 'pending':
          output.skipped.push(result);
          break;
        default:
          throw new Error(`Unexpected test result status: ${testResult.status}`); // eslint-disable-line prettier/prettier
      }
    });
  });

  fs.writeFileSync(filename, JSON.stringify(output, null, 2), 'utf8');
  return results;
};
