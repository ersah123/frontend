/* eslint-disable import/prefer-default-export */
import {
  FETCH_SEARCH_PENDING,
  FETCH_SEARCH_FULFILLED,
  FETCH_SEARCH_REJECTED,
} from '../constants';

const defaultState = {
  results: [],
  status: null,
  loading: false,
  messages: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case FETCH_SEARCH_PENDING: {
      return {
        ...state,
        loading: true,
      };
    }

    case FETCH_SEARCH_FULFILLED: {
      const { payload, status, searchTerm } = action;
      const { results } = payload;

      return {
        ...state,
        results,
        searchTerm,
        loading: false,
        status,
      };
    }

    case FETCH_SEARCH_REJECTED: {
      const { messages, status } = action;
      return {
        ...state,
        loading: false,
        status,
        messages,
      };
    }

    default: {
      return state;
    }
  }
};
