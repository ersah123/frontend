import {
  FETCH_SALONS_FULFILLED,
  FETCH_SALONS_PENDING,
  FETCH_SALONS_REJECTED,
} from '../constants';

const defaultState = {
  salons: [],
  status: null,
  loading: false,
  messages: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case FETCH_SALONS_PENDING: {
      return {
        ...state,
        loading: true,
      };
    }

    case FETCH_SALONS_FULFILLED: {
      const { payload, status } = action;

      return {
        ...state,
        salons: [...payload],
        loading: false,
        status,
      };
    }

    case FETCH_SALONS_REJECTED: {
      const { messages, status } = action;
      return {
        ...state,
        loading: false,
        status,
        messages,
      };
    }

    default: {
      return state;
    }
  }
};
