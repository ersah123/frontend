/* eslint-disable import/prefer-default-export */

import Product from '../models/Product';

import {
  FETCH_PRODUCTS_FULFILLED,
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_REJECTED,
} from '../constants';

const defaultState = {
  products: {},
  status: null,
  loading: false,
  messages: [],
};

/**
 * Makes a key from filters to file products with
 * @param {Object} filters to create key from
 */
export function createFiltersKey(filters) {
  if (!filters) return null;
  return Object.entries(filters)
    .sort()
    .toString();
}

/**
 * Gets list of products based on filters (used as key)
 * @param {Object} products products returned from API
 * @param {Object} filters filters used to get these products
 */
export function getProductsByFilter(products, filters) {
  const filtersKey = createFiltersKey(filters);
  if (products) {
    return products[filtersKey];
  }
  return null;
}

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case FETCH_PRODUCTS_PENDING: {
      return {
        ...state,
        loading: true,
      };
    }

    case FETCH_PRODUCTS_FULFILLED: {
      const { payload, filters, status } = action;
      const products = payload.products.map(product => new Product(product));

      return {
        ...state,
        products: {
          ...state.products,
          [createFiltersKey(filters)]: products,
        },
        loading: false,
        status,
      };
    }

    case FETCH_PRODUCTS_REJECTED: {
      const { messages, status } = action;
      return {
        ...state,
        loading: false,
        status,
        messages,
      };
    }

    default: {
      return state;
    }
  }
};
