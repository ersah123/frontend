/* eslint-disable import/prefer-default-export */
import {
  SET_DATALAYER_DEFAULTDATA,
  UPDATE_DATALAYER_DEFAULTDATA,
  CLEAR_DATALAYER_DEFAULTDATA,
  ADD_DATALAYER_DATA,
  CLEAR_DATALAYER_DATA,
  PUSHED_TO_DATALAYER,
} from '../constants';

export const defaultState = {};

export default (state = defaultState, action = {}) => {
  const dataLayerName = action.name || 'dataLayer';
  const layerState = state[dataLayerName] || {
    data: [],
    defaultData: {},
    pushed: false,
  };

  switch (action.type) {
    case SET_DATALAYER_DEFAULTDATA: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          defaultData: {
            ...action.payload,
          },
          pushed: false,
        },
      };
    }
    case UPDATE_DATALAYER_DEFAULTDATA: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          defaultData: {
            ...layerState.defaultData,
            ...action.payload,
          },
          pushed: false,
        },
      };
    }
    case CLEAR_DATALAYER_DEFAULTDATA: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          defaultData: {},
          pushed: false,
        },
      };
    }
    case ADD_DATALAYER_DATA: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          data: [
            ...layerState.data,
            { ...layerState.defaultData, ...action.payload },
          ],
          pushed: false,
        },
      };
    }
    case CLEAR_DATALAYER_DATA: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          data: [],
          pushed: false,
        },
      };
    }
    case PUSHED_TO_DATALAYER: {
      return {
        ...state,
        [dataLayerName]: {
          ...layerState,
          data: [],
          pushed: true,
        },
      };
    }
    default:
      return state;
  }
};
