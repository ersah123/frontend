import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import analytics from './analytics.reducer';
import page from './page.reducer';
import product from './product.reducer';
import runtime from './runtime.reducer';
import salon from './salon.reducer';
import search from './search.reducer';

export default combineReducers({
  runtime,
  analytics,
  page,
  product,
  search,
  salon,
  form: reduxFormReducer,
});
