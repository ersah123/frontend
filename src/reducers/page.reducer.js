/* eslint-disable import/prefer-default-export */

import Content from '../models/Content';
import Hero from '../models/Hero';
import RangeHero from '../models/RangeHero';
import Media from '../models/Media';
import ProductHero from '../models/ProductHero';
import Promo from '../models/Promo';
import PromoDouble from '../models/PromoDouble';
import Spacer from '../models/Spacer';
import Story from '../models/Story';
import Quote from '../models/Quote';

import {
  FETCH_PAGE_FULFILLED,
  FETCH_PAGE_PENDING,
  FETCH_PAGE_REJECTED,
  RESET_PAGE,
} from '../constants';

const defaultState = {
  slug: '',
  status: null,
  loading: false,
  messages: [],
  data: [],
};

export function mapComponentData(data) {
  const { componentData } = data;
  const newData = data;
  const mapped = componentData.map(comp => {
    let mappedComponent = comp;
    if (comp.componentName === 'Content') {
      mappedComponent = new Content(comp);
    } else if (comp.componentName === 'Hero') {
      mappedComponent = new Hero(comp);
    } else if (comp.componentName === 'Media') {
      mappedComponent = new Media(comp);
    } else if (comp.componentName === 'ProductHero') {
      mappedComponent = new ProductHero(comp);
    } else if (comp.componentName === 'Promo') {
      mappedComponent = new Promo(comp);
    } else if (comp.componentName === 'PromoDouble') {
      mappedComponent = new PromoDouble(comp);
    } else if (comp.componentName === 'Quote') {
      mappedComponent = new Quote(comp);
    } else if (comp.componentName === 'RangeHero') {
      mappedComponent = new RangeHero(comp);
    } else if (comp.componentName === 'Spacer') {
      mappedComponent = new Spacer(comp);
    } else if (comp.componentName === 'Story') {
      mappedComponent = new Story(comp);
    }
    return mappedComponent;
  });

  newData.componentData = mapped;
  return newData;
}

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case FETCH_PAGE_PENDING: {
      return {
        ...state,
        loading: true,
      };
    }

    case FETCH_PAGE_FULFILLED: {
      const data = mapComponentData(action.payload);

      return {
        ...state,
        slug: action.slug,
        status: action.status,
        data,
        loading: false,
      };
    }

    case FETCH_PAGE_REJECTED: {
      return {
        ...defaultState,
        status: action.status,
        messages: action.messages,
        loading: false,
      };
    }

    case RESET_PAGE: {
      return {
        ...defaultState,
      };
    }

    default: {
      return state;
    }
  }
};
