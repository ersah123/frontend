import { SET_RUNTIME_VARIABLE } from '../constants';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_RUNTIME_VARIABLE:
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    default:
      return state;
  }
};

/**
 * Ask runtime reducer if we're at localhost or not.
 * @param {Object} state runtime
 */
export const isLocalhost = state => {
  let local = false;
  if (state.siteInit) {
    const { host } = state.siteInit;
    local = host && host === 'localhost';
  }
  return local;
};
