import nodeFetch from 'node-fetch';
import urlParse from 'url-parse';
import config from './config';
import assets from './asset-manifest.json'; // eslint-disable-line import/no-unresolved
import siteInitJson from '../content/application';

const isAsset = url =>
  Object.entries(assets).find(([, value]) => url.endsWith(value));

export default async (req, res, next) => {
  const asset = isAsset(req.originalUrl);
  // example content
  let { data: siteInit } = siteInitJson;

  if (Array.isArray(asset)) {
    // Overwrite the URL so we have compatibility with static routes etc
    req.url = `/${asset[1]}`;
    return next();
  }

  try {
    if (!config.api.local) {
      const resp = await nodeFetch(
        // `${config.api.url}/v1.0/applications/init?apiKey=${config.api.key}`,
        `${config.api.url}/init?apiKey=${config.api.key}`,
        {
          headers: {
            Referer: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
          },
        },
      );
      const { data } = await resp.json();
      siteInit = data;
    }

    // Add the siteInit to the req so the next route can access the values
    req.siteInit = siteInit;

    // If there isn't a site init return with an error
    if (siteInit === undefined) {
      const err = new Error(
        `The URL you are requesting does not match any sites ${
          req.protocol
        }://${req.get('host')}${req.originalUrl}`,
      );
      err.status = 418;
      throw err;
    }

    // Map the url parameters and set to undefined when empty
    const params = req.originalUrl
      .split('/')
      .splice(1)
      .map(i => (i === '' ? undefined : i));

    // Map the url structure keys
    const pKeys = siteInit.url.structure.split('/:').splice(1);
    pKeys.push('0'); // Add "0" key to fallback for all the rest routes

    // Map the parameters and url keys into an object
    const zipped = pKeys.reduce((o, p, i) => {
      // Set value of the key or when all are done use the remaining values and the default "0" params with '' instead of undefined
      const value =
        i === pKeys.length - 1 ? params.slice(i).join('/') || '' : params[i];
      return Object.assign({}, o, { [p]: value });
    }, {});

    // If the default options are set and missing or incorrect in the url parameters set the default one
    const pValues = pKeys.map(k => {
      const valueOptions = siteInit.url.options[k];
      // Check if value is inside the array else return the default value
      if (valueOptions && !valueOptions.includes(zipped[k])) {
        // Add the new value to the end of the "0" param

        if (zipped[k] !== '') {
          const newRestUrl = zipped[0].split('/').filter(v => v !== '');
          newRestUrl.unshift(zipped[k]);
          zipped[0] = newRestUrl.join('/');
        }

        return valueOptions[0];
      }

      return zipped[k];
    });

    // Check if the route is valid by checking if all values are set
    if (pValues.slice(0, -1).includes(undefined)) {
      const err = new Error(
        `The URL you are requesting does not match the URL structure supplied by the server, please use te following structure: ${
          siteInit.url.structure
        }`,
      );
      err.status = 418;
      throw err;
    }

    // Add the custom path & params & baseUrl
    req.customPath = urlParse(`/${zipped['0']}`, {}).pathname;
    req.url = `/${zipped['0']}`; // Overwrite the URL so we have compatibility with static routes etc
    req.customParams = zipped;
    req.customBaseUrl =
      pValues.length === 1 ? '' : `/${pValues.slice(0, -1).join('/')}`;

    // Remove "0" param and check is all params are set and valid in the default options when present
    const allParamsArePresentAndValid = Object.keys(zipped).every(k => {
      const valueOptions = siteInit.url.options[k];
      return (
        k === '0' ||
        (!!zipped[k] && (!valueOptions || valueOptions.includes(zipped[k])))
      );
    });

    // If the route is missing parameters do a redirect d with the new parameter
    if (!allParamsArePresentAndValid)
      return res.redirect(307, `/${pValues.join('/')}`);

    // Everything is ok! lets pass it to the next route
    return next();
  } catch (err) {
    return next(err);
  }
};
