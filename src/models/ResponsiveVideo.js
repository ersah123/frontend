class Sizes {
  constructor(media) {
    this.lg = {
      src: `https://keune-backend-test.azurewebsites.net${media[0].url}`,
      width: 1280,
      height: 1024,
      alt: media[0].title,
      poster: null,
    };
    this.xl = {
      src: `https://keune-backend-test.azurewebsites.net${media[0].url}`,
      width: 1920,
      height: 1280,
      alt: media[0].title,
      poster: null,
    };
  }
}

/**
 * ResponsiveVideo component
 */
export default class ResponsiveVideo {
  constructor(video) {
    /** @type {Number} */
    this.id = video.id;

    /** @type {String} */
    this.componentName = 'ResponsiveVideo';

    /** @type {String} */
    this.title = video.title;

    /** @type {String} */
    this.alt = video.title;

    /** @type {String} */
    this.defaultSize = 'lg';

    this.sizes = new Sizes(video.media);
  }
}
