import ResponsiveImage from './ResponsiveImage';
import ResponsiveVideo from './ResponsiveVideo';

/**
 * Hero component
 */
export default class Hero {
  constructor(hero) {
    /** @type {Number} */
    this.id = hero.id;

    /** @type {String} */
    this.componentName = 'Hero';

    /** @type {String} */
    this.title = hero.title;

    /** @type {String} */
    this.subtitle = hero.subtitle;

    /** @type {String} */
    this.text = hero.text;

    /** @type {String} */
    this.theme = hero.theme ? hero.theme.toLowerCase() : 'light';

    /** @type {String} */
    this.align = hero.textAlign || 'leftBottom';

    /** @type {String} */
    this.buttonText = hero.buttonText;

    /** @type {String} */
    this.buttonUrl = hero.buttonUrl;

    /** @type {boolean} */
    this.isFullHeight = hero.isFullHeight || true;

    /** @type {ResponsiveImage} */
    this.backgroundImage =
      // eslint-disable-next-line no-nested-ternary
      hero.media &&
      hero.media.length > 0 &&
      hero.media[0].url &&
      hero.media[0].sizes &&
      Object.keys(hero.media[0].sizes).length !== 0
        ? hero.media[0].url.indexOf('mp4') < 0
          ? new ResponsiveImage({
              id: hero.media[0].id,
              alt: hero.media[0].alt,
              title: hero.title,
              media: hero.media,
            })
          : new ResponsiveVideo({
              id: hero.id,
              title: hero.title,
              media: hero.media,
            })
        : null;
  }
}
