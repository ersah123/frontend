/**
 * Spacer component
 */
export default class Spacer {
  constructor(spacer) {
    /** @type {Number} */
    this.id = spacer.id;

    /** @type {String} */
    this.componentName = 'Spacer';

    /** @type {Number} */
    this.vertical = spacer.height;
  }
}
