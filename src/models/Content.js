import ResponsiveImage from './ResponsiveImage';
import ResponsiveVideo from './ResponsiveVideo';

/**
 * Content component
 */
export default class Content {
  constructor(content) {
    /** @type {Number} */
    this.id = content.id;

    /** @type {String} */
    this.componentName = 'Content';

    /** @type {String} */
    this.title = content.title;

    /** @type {String} */
    this.subtitle = content.subtitle;

    /** @type {String} */
    this.text = content.text;

    /** @type {String} */
    this.buttonText = content.buttonText || null;

    /** @type {String} */
    this.buttonUrl = content.buttonUrl || null;

    /** @type {Boolean} */
    this.hasMediaRight = content.hasMediaRight || false;

    /** @type {ResponsiveImage} */
    this.responsiveImage =
      // eslint-disable-next-line no-nested-ternary
      content.media &&
      content.media.length > 0 &&
      content.media[0].url &&
      content.media[0].sizes &&
      Object.keys(content.media[0].sizes).length !== 0
        ? content.media[0].url.indexOf('mp4') < 0
          ? new ResponsiveImage({
              id: content.media[0].id,
              alt: content.media[0].alt,
              title: content.title,
              media: content.media,
            })
          : new ResponsiveVideo({
              id: content.id,
              title: content.title,
              media: content.media,
            })
        : null;
  }
}
