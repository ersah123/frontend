import { convertSizes } from './ResponsiveImage';

/**
 * Media component
 */
export default class Media {
  constructor(med) {
    /** @type {Number} */
    this.id = med.id;

    /** @type {String} */
    this.componentName = 'Media';

    /** @type {String} */
    this.title = med.title;

    /** @type {Sizes} */
    this.sizes =
      med.media.length > 0 && med.media[0].sizes
        ? convertSizes(med.media[0], med.title)
        : {};

    this.defaultSize = 'lg';
  }
}
