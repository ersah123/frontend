import { convertSizes } from './ResponsiveImage';

export const getKitten = () => {
  const md = {};
  md.width = 652;
  md.height = 652;
  md.alt = 'Kitten';
  md.src = `https://loremflickr.com/652/652/kitten?${Math.floor(
    Math.random() * 99,
  )}`;
  const kitten = {};
  kitten.md = md;
  return kitten;
};

/**
 * Get from various media the sizes we need
 * @param {Object} media objects with xs, sm, md etc. images
 */
export const getSizes = media => {
  if (!media.sizes && Object.keys(media.sizes).length === 0) {
    return getKitten();
  }
  return convertSizes(media.sizes, media.alt);
};

/**
 * Product object
 */
export default class Product {
  constructor(product) {
    /** @type {Number} */
    this.id = parseInt(product.sku, 10) + Math.random();

    /** @type {String} */
    this.articleNr = product.ean;

    /** @type {String} */
    this.sku = product.sku;

    /** @type {String} */
    this.ean = product.ean;

    /** @type {String} */
    this.brand = product.brand;

    /** @type {String} */
    this.category = product.category;

    /** @type {String} */
    this.productGroup = product.productGroup;

    /** @type {String} */
    this.name = product.name;

    /** @type {String} */
    this.subtitle = product.subline;

    /** @type {String} */
    this.text = product.description;

    /** @type {String} */
    this.size = product.size;

    /** @type {Array} */
    this.labels = product.labels ? product.labels.map(label => label) : [];

    /** @type {Number} */
    this.price = product.price || 0.0;

    /** @type {Number} */
    this.promoPrice = product.promoPrice || 0.0;

    /** @type {Number} */
    this.shine = product.shineFactor ? parseInt(product.shineFactor, 10) : null;

    /** @type {Number} */
    this.hold = product.holdFactor ? parseInt(product.holdFactor, 10) : null;

    /** @type {String} */
    this.color = product.color || '#e5e9ef';

    /** @type {String} */
    this.suitableFor = product.suitableFor;

    /** @type {String} */
    this.directionsForUse = product.directionsForUse;

    /** @type {String} */
    this.ingredients = product.ingredients;

    /** @type {String} */
    this.buttonText = product.buttonText;

    /** @type {String} */
    this.buttonUrl = product.buttonUrl;

    this.media =
      product.media && product.media.length > 0
        ? product.media.reduce((accumulator, item) => {
            if (Object.keys(item.sizes).length !== 0) {
              accumulator.push(getSizes(item));
            }
            return accumulator;
          }, [])
        : [getKitten()];
  }
}
