import { getKitten, getSizes } from './Product';

/**
 * Quote component
 */
export default class Quote {
  constructor(quote) {
    /** @type {Number} */
    this.id = quote.id;

    /** @type {String} */
    this.componentName = 'Quote';

    /** @type {String} */
    this.author = quote.author;

    /** @type {String} */
    this.text = quote.text;

    /** @type {Bool} */
    this.hasMediaRight = quote.hasMediaRight || false;

    /** @type {Object} */
    this.image =
      quote.media && quote.media.length > 0
        ? quote.media.reduce((accumulator, item) => {
            if (Object.keys(item.sizes).length !== 0) {
              accumulator.push(getSizes(item));
            }
            return accumulator;
          }, [])
        : [getKitten()];
  }
}
