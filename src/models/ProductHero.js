import Product from './Product';

/**
 * ProductHero component
 */
export default class ProductHero {
  constructor(hero) {
    /** @type {Number} */
    this.id = hero.id;

    /** @type {String} */
    this.componentName = 'ProductHero';

    /** @type {Product} */
    this.product = hero.product ? new Product(hero.product) : null;

    /** @type {String} */
    this.buttonText = hero.buttonText;

    /** @type {String} */
    this.buttonUrl = hero.buttonUrl;

    /** @type {String} */
    this.holdFactorText = hero.holdFactorText;

    /** @type {String} */
    this.shineFactorText = hero.shineFactorText;

    /** @type {String} */
    this.suitableForText = hero.suitableForText;

    // this.slides = [
    //   {
    //     id: 1,
    //     src: 'images/media-04.jpg',
    //     thumb: 'images/thumb04.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 2,
    //     src: 'images/media-02.jpg',
    //     thumb: 'images/thumb02.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 3,
    //     src: 'images/media-03.jpg',
    //     thumb: 'images/thumb03.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 4,
    //     src: 'images/media-01.jpg',
    //     thumb: 'images/thumb01.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    // ];

    // this.thumbs = [
    //   {
    //     id: 1,
    //     src: 'images/thumb04.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 2,
    //     src: 'images/thumb02.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 3,
    //     src: 'images/thumb03.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    //   {
    //     id: 4,
    //     src: 'images/thumb01.png',
    //     alt: 'alt',
    //     width: '1024',
    //     height: '757',
    //   },
    // ];
  }
}
