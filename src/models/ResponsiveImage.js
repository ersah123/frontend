export const convertSizes = (sizes, alt) => {
  const converted = sizes;
  Object.keys(sizes).map(size => {
    if (sizes[size].src) return null;
    converted[size].alt = alt;
    converted[size].src = `https://keune-backend-test.azurewebsites.net${
      sizes[size].url
    }`;
    delete converted[size].url;
    return null;
  });
  if (converted.mdq) {
    converted.md = converted.mdq;
    delete converted.mdq;
  }
  if (converted.lgq) {
    converted.lg = converted.lgq;
    delete converted.lgq;
  }
  return converted;
};

/**
 * ResponsiveImage component
 */
export default class ResponsiveImage {
  constructor(img) {
    /** @type {Number} */
    this.id = img.id;

    /** @type {String} */
    this.componentName = 'ResponsiveImage';

    /** @type {String} */
    this.defaultSize = 'lg';

    this.sizes =
      img.media.length > 0 && img.media[0].sizes
        ? convertSizes(img.media[0].sizes, img.alt)
        : null;
  }
}
