import ResponsiveImage from './ResponsiveImage';
import ResponsiveVideo from './ResponsiveVideo';

/**
 * Promo component
 */
export default class Promo {
  constructor(promo) {
    /** @type {Number} */
    this.id = promo.id;

    /** @type {String} */
    this.componentName = 'Promo';

    /** @type {String} */
    this.title = promo.title;

    /** @type {String} */
    this.subtitle = promo.subtitle;

    /** @type {String} */
    this.text = promo.text;

    /** @type {String} */
    this.buttonText = promo.buttonText;

    /** @type {String} */
    this.buttonUrl = promo.buttonUrl;

    /** @type {String} */
    this.align = promo.textAlign || 'leftBottom';

    this.theme = promo.theme ? promo.theme.toLowerCase() : 'light';

    /** @type {ResponsiveImage} */
    this.backgroundImage =
      // eslint-disable-next-line no-nested-ternary
      promo.media &&
      promo.media.length > 0 &&
      promo.media[0].url &&
      promo.media[0].sizes &&
      Object.keys(promo.media[0].sizes).length !== 0
        ? promo.media[0].url.indexOf('mp4') < 0
          ? new ResponsiveImage({
              id: promo.media[0].id,
              alt: promo.media[0].alt,
              title: promo.title,
              media: promo.media,
            })
          : new ResponsiveVideo({
              id: promo.id,
              title: promo.title,
              media: promo.media,
            })
        : null;
  }
}
