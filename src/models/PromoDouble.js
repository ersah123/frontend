import Promo from './Promo';

/**
 * PromoDouble component
 */
export default class PromoDouble {
  constructor(promo) {
    /** @type {Number} */
    this.id = promo.id;

    /** @type {String} */
    this.componentName = 'PromoDouble';

    this.promo1 = new Promo(promo.promoTiles[0]);
    this.promo2 = new Promo(promo.promoTiles[1]);
  }
}
