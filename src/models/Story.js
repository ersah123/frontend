import ResponsiveImage from './ResponsiveImage';

/**
 * Story component
 */
export default class Story {
  constructor(story) {
    /** @type {Number} */
    this.id = story.id;

    /** @type {String} */
    this.componentName = 'Story';

    /** @type {String} */
    this.title = story.title;

    /** @type {String} */
    this.subtitle = story.subtitle;

    /** @type {String} */
    this.text = story.text;

    /** @type {ResponsiveImage} */
    this.responsiveImage =
      story.media &&
      story.media.length > 0 &&
      story.media[0].sizes &&
      Object.keys(story.media[0].sizes).length !== 0
        ? new ResponsiveImage({
            id: story.media[0].id,
            alt: story.media[0].alt,
            title: story.title,
            media: story.media,
          })
        : null;
  }
}
