import React from 'react';
import { IntlProvider } from 'react-intl';
import Layout from '../../components/globals/Layout';

import { search as querySearch } from '../../actions/search.action';
import { getPage } from '../../actions/page.action';

async function action({ lang, pathname, query, store }) {
  try {
    await store.dispatch(
      getPage({
        slug: pathname,
        query,
      }),
    );
  } catch (e) {
    console.error('getPage-error', e);
  }

  const {
    page,
    runtime: { mainNavigation, analytics, messages },
  } = store.getState();

  if (page.status === 404 || page.status === null) return undefined;
  if (page.status !== 200) throw new Error(page.messages);
  const { template, title, metaDescription: description, keywords } = page.data;

  let TemplateComponent = template;
  const chunks = ['generic'];

  // Load each template via an aync load with a separate dynamic import()
  switch (template) {
    case 'FormExample': {
      TemplateComponent = await import(/* webpackChunkName: 'formexample' */ '../../components/templates/FormExample');
      chunks.push('formexample');
      break;
    }
    case 'Search': {
      try {
        await store.dispatch(querySearch(query.q));
      } catch (e) {
        console.error('search-error', e);
      }
      TemplateComponent = await import(/* webpackChunkName: 'dynamic' */ '../../components/templates/Dynamic');
      chunks.push('dynamic');
      break;
    }
    default: {
      TemplateComponent = await import(/* webpackChunkName: 'dynamic' */ '../../components/templates/Dynamic');
      chunks.push('dynamic');
    }
  }

  return {
    chunks,
    title,
    description,
    keywords,
    component: (
      <IntlProvider locale={lang} messages={messages}>
        <Layout navigation={mainNavigation} analytics={analytics}>
          <TemplateComponent.default {...page.data} />
        </Layout>
      </IntlProvider>
    ),
  };
}

export default action;
