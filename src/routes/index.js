/* eslint-disable global-require */
import {
  updateDataLayerDefault,
  addDataLayerData,
} from '../actions/analytics.action';

// The top-level (parent) route
const routes = {
  path: '',
  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '(.*)', // Example dynamic content load API
      load: () => import(/* webpackChunkName: 'generic' */ './generic'),
    },

    // Wildcard routes, e.g. { path: '(.*)', ... } (must go last)
    {
      path: '(.*)',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next, store, pathname, lang, rootUrl }) {
    // const { runtime: { analytics } } = store.getState();
    // for some reason the above (nested) destructuring fails here
    const runtime = store.getState();
    const { analytics } = runtime;

    // Set analytics default data
    await store.dispatch(
      updateDataLayerDefault({
        brand: 'XXX', // TODO: Make this work from the CMS
        adobeAccount: analytics.adobe,
        googleAccount: analytics.google,
        languageCode: lang.split('-')[0],
        countryCode: lang.split('-')[1],
        pageName: `${rootUrl}${pathname}`,
      }),
    );

    // Execute each child route until one of them return the result
    const route = await next();

    // Provide default values for title, description etc.
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';

    // Send analytics pageView
    if (process.env.BROWSER) {
      await store.dispatch(
        addDataLayerData({
          event: 'pageViewed',
        }),
      );
    }

    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__ || __TEST__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
