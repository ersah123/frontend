import React from 'react';
import Layout from '../../components/globals/Layout';
import NotFound from '../../components/templates/NotFound/NotFound';

function action({ store }) {
  const {
    runtime: { mainNavigation, analytics },
  } = store.getState();

  return {
    chunks: ['not-found'],
    component: (
      <Layout navigation={mainNavigation} analytics={analytics}>
        <NotFound />
      </Layout>
    ),
    status: 404,
  };
}

export default action;
