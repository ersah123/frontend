import Cookies from 'js-cookie';

/**
 * Creates a wrapper function around the HTML5 Fetch API that provides
 * default arguments to fetch(...) and is intended to reduce the amount
 * of boilerplate code in the application. The wrapper also ties into the CMS
 * to refersh the token when it has expired and a 401 is returned.
 * https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch
 */
function createFetch(fetch, { apiUrl, cookie, lang, auth: authInit, referer }) {
  // Store the init auth values in the auth object
  let auth = { ...authInit };

  // Set the default values
  const defaults = {
    method: 'GET',
    // mode: apiUrl ? 'cors' : 'same-origin',
    // credentials: apiUrl ? 'include' : 'same-origin',
    // mode: 'same-origin',
    // credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(lang ? { 'Content-Language': lang } : null),
      ...(referer ? { Referer: referer } : null),
      ...(cookie ? { Cookie: cookie } : null),
    },
  };

  return async (fetchURL = '', fetchOptions) => {
    // Add full URL to fetch('/api') calls
    // ? `${apiUrl}${fetchURL.replace('/api', '')}`
    const url = fetchURL.startsWith('/api') ? `${apiUrl}${fetchURL}` : fetchURL;
    console.info(`#FETCH URL: ${url}`);

    // Merge the new fetch('/api', {OPTIONS}) with the default ones and
    // add Authorization to headers if present
    const options = {
      ...defaults,
      ...fetchOptions,
      headers: {
        ...defaults.headers,
        ...(auth && authInit
          ? { Authorization: `${auth.tokenType} ${auth.accessToken}` }
          : null),
        ...(fetchOptions && fetchOptions.headers),
      },
    };

    try {
      // Fetch the response
      const response = await fetch(url, options);

      // Check if the response is with 401 and check if its an API call
      if (response.status === 401 && fetchURL.startsWith('/api')) {
        // Fetch the new auth data
        const authFetch = await fetch(`${apiUrl}/v1.0/auth/refreshToken/`, {
          ...defaults,
          method: 'POST',
          body: JSON.stringify({
            refreshAccessToken: auth && auth.refreshAccessToken,
          }),
        });

        const { data: authData } = await authFetch.json();
        // If data is successfull save the authData
        if (authData) {
          auth = authData; // Set the upper auth with the new data

          // Set the cookie data
          Cookies.set('auth', authData, {
            expires: new Date(authData.expires),
            secure: !__DEV__,
          });

          // Fetch with the new updated header
          return fetch(url, {
            ...options,
            headers: {
              ...(options && options.headers),
              Authorization: `${authData.tokenType} ${authData.accessToken}`,
            },
          });
        }
      }

      // Return the original response
      return Promise.resolve(response);
    } catch (err) {
      // If there is an error send it back
      return Promise.reject(err);
    }
  };
}

export default createFetch;
