import PropTypes from 'prop-types';

export const HeroPropTypes = {
  /**
   * The height of the component
   */
  isFullHeight: PropTypes.bool,
  /**
   * The subtitle or headline string
   */
  subtitle: PropTypes.string,
  /**
   * The title string
   */
  title: PropTypes.string,
  /**
   * The title string
   */
  text: PropTypes.string,
  /**
   * The button text string
   */
  buttonText: PropTypes.string,
  /**
   * The button text string
   */
  buttonUrl: PropTypes.string,
  /**
   * The theme of hero
   */
  theme: PropTypes.oneOf(['light', 'dark']),
  /**
   * The component for the backgroundImage slot
   */
  backgroundImage: PropTypes.node.isRequired,
  /**
   * The position of the background
   */
  backgroundPosition: PropTypes.oneOf(['left', 'center', 'right']),
  /**
   * The horizontal position of the content
   */
  justify: PropTypes.oneOf(['start', 'center', 'end']),
  /**
   * The vertical position of the content.
   */
  align: PropTypes.oneOf(['leftTop', 'leftMiddle', 'leftBottom']),
  /**
   * @ignore
   */
  className: PropTypes.string,
  /**
   * The content of the component.
   */
  children: PropTypes.node,
};

export const HeroDefaultPropTypes = {
  title: null,
  subtitle: null,
  text: null,
  backgroundPosition: 'center',
  justify: 'start',
  align: 'leftBottom',
  buttonText: null,
  buttonUrl: null,
  children: null,
  className: null,
  theme: 'light',
};
