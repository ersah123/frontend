/* eslint-disable react/no-danger */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as analyticsActions from '../actions/analytics.action';

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...analyticsActions }, dispatch);

const mapStateToProps = ({ analytics }, { name }) => ({
  analytics: analytics[name],
});

@connect(
  mapStateToProps,
  mapDispatchToProps,
)
class GTMDataLayer extends PureComponent {
  static propTypes = {
    gtmId: PropTypes.string.isRequired,
    name: PropTypes.string,
    analytics: PropTypes.shape({
      data: PropTypes.arrayOf(PropTypes.shape()),
      defaultData: PropTypes.shape(),
      pushed: PropTypes.bool,
    }),
    pushedToDataLayer: PropTypes.func.isRequired,
  };

  static defaultProps = {
    name: 'dataLayer',
    analytics: {
      data: [],
      defaultData: {},
      pushed: false,
    },
  };

  state = {
    isMounted: false,
  };

  componentDidMount() {
    const { name } = this.props;
    window[name] = window[name] || [];
    // Push any data there is onMount
    this.pushToWindow();
    this.setState({ isMounted: true });
  }

  componentDidUpdate() {
    const { analytics } = this.props;
    if (analytics.data.length > 0 && !analytics.pushed) this.pushToWindow();
  }

  pushToWindow = () => {
    const { name, analytics, pushedToDataLayer } = this.props;
    const { data } = analytics;

    if (!window) return; // Only when there is a window
    if (!name) {
      console.error(
        `Can't push to dataLayer no name set "pushToDataLayer(name: String, event: String)"`,
      );
      return;
    }

    data.forEach(event => {
      window[name].push(event);
      return event;
    });

    pushedToDataLayer(name);
  };

  render() {
    const { gtmId, name } = this.props;
    const { isMounted } = this.state;

    if (isMounted) return null;

    return (
      <script
        dangerouslySetInnerHTML={{
          __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','${name}','${gtmId}');`,
        }}
      />
    );
  }
}

export default GTMDataLayer;
