import React, { Fragment, PureComponent } from 'react';

import PropTypes from 'prop-types';
import serialize from 'serialize-javascript';
import { googleMapsApiKey } from '../constants/index';
/* eslint-disable react/no-danger */

class Html extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    keywords: PropTypes.string,
    styles: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        cssText: PropTypes.string.isRequired,
      }).isRequired,
    ),
    scripts: PropTypes.arrayOf(PropTypes.string.isRequired),
    app: PropTypes.object, // eslint-disable-line
    children: PropTypes.string.isRequired,
    analytics: PropTypes.shape({
      google: PropTypes.string,
      adobe: PropTypes.string,
      mopinion: PropTypes.string,
    }),
  };

  static defaultProps = {
    keywords: null,
    styles: [],
    scripts: [],
    analytics: {},
  };

  render() {
    const {
      title,
      description,
      keywords,
      styles,
      scripts,
      app,
      children,
      analytics,
    } = this.props;

    const lang = app && app.lang ? app.lang.split('-') : ['en', 'US'];

    return (
      <html lang={lang[0]}>
        <head>
          <base href={app && app.baseUrl ? `${app.baseUrl}/` : '/'} />
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <title>{title}</title>
          <meta name="description" content={description} />
          {keywords && <meta name="keywords" content={keywords} />}
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
          <link rel="manifest" href="site.webmanifest" />
          <link rel="apple-touch-icon" href="icon.png" />
          <script
            src={`https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}&libraries=places`}
          />
          {analytics &&
            analytics.adobe && (
              <script src="//assets.adobedtm.com/a1c41f6ae736082fa7028aba7f54e67d18981903/satelliteLib-5ce4c5f069171752ee545e2f0dda5f3392220826.js" />
            )}
          {scripts.map(script => (
            <link key={script} rel="preload" href={script} as="script" />
          ))}
          {styles.map(style => (
            <style
              key={style.id}
              id={style.id}
              dangerouslySetInnerHTML={{ __html: style.cssText }}
            />
          ))}
        </head>
        <body>
          <div id="app" dangerouslySetInnerHTML={{ __html: children }} />
          <script
            id="initialState"
            dangerouslySetInnerHTML={{ __html: `window.App=${serialize(app)}` }}
          />
          {scripts.map(script => (
            <script key={script} src={script} />
          ))}
          {analytics &&
            analytics.google && (
              <Fragment>
                <script
                  dangerouslySetInnerHTML={{
                    __html:
                      'window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;' +
                      `ga('create','${
                        analytics.google
                      }','auto');ga('send','pageview')`,
                  }}
                />
                <script
                  src="//www.google-analytics.com/analytics.js"
                  async
                  defer
                />
              </Fragment>
            )}
          {analytics &&
            analytics.adobe && (
              <script
                dangerouslySetInnerHTML={{
                  __html: `if(window._satellite) _satellite.pageBottom();`,
                }}
              />
            )}
          {analytics &&
            analytics.mopinion && (
              <script
                dangerouslySetInnerHTML={{
                  __html: `(function(){var id="${
                    analytics.mopinion
                  }";var js=document.createElement("script");js.setAttribute("type","text/javascript");js.setAttribute("src","//deploy.mopinion.com/js/pastease.js");js.async=true;document.getElementsByTagName("head")[0].appendChild(js);var t=setInterval(function(){try{new Pastease.load(id);clearInterval(t)}catch(e){}},50)})()`,
                }}
              />
            )}
        </body>
      </html>
    );
  }
}

export default Html;
