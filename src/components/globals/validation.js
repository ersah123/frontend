// Dutch Licence Plate options
const dutchRegistrationPlateRegex = [
  /^([a-zA-Z]{2})([\d]{2})([\d]{2})$/, // XX-99-99
  /^([\d]{2})([\d]{2})([a-zA-Z]{2})$/, // 99-99-XX
  /^([\d]{2})([a-zA-Z]{2})([\d]{2})$/, // 99-XX-99
  /^([a-zA-Z]{2})([\d]{2})([a-zA-Z]{2})$/, // XX-99-XX
  /^([a-zA-Z]{2})([a-zA-Z]{2})([\d]{2})$/, // XX-XX-99
  /^([\d]{2})([a-zA-Z]{2})([a-zA-Z]{2})$/, // 99-XX-XX
  /^([\d]{2})([a-zA-Z]{3})([\d]{1})$/, // 99-XXX-9
  /^([\d]{1})([a-zA-Z]{3})([\d]{2})$/, // 9-XXX-99
  /^([a-zA-Z]{2})([\d]{3})([a-zA-Z]{1})$/, // XX-999-X
  /^([a-zA-Z]{1})([\d]{3})([a-zA-Z]{2})$/, // X-999-XX
  /^([a-zA-Z]{3})([\d]{2})([a-zA-Z]{1})$/, // XXX-99-X
  /^([a-zA-Z]{1})([\d]{2})([a-zA-Z]{3})$/, // X-99-XXX
  /^([\d]{1})([a-zA-Z]{2})([\d]{3})$/, // 9-XX-999
  /^([\d]{3})([a-zA-Z]{2})([\d]{1})$/, // 999-XX-9
  /^(CD)([ABFJNST][0-9]{1,3})$/, // CD-B1 or CD-J45
];

function matchRegistrationPlate(registrationPlate) {
  return dutchRegistrationPlateRegex.some(f =>
    registrationPlate
      .toUpperCase()
      .replace(/-/g, '')
      .match(f),
  );
}

export function registrationPlateFormatter(registrationPlate) {
  let formatted = registrationPlate;

  if (registrationPlate !== '' || matchRegistrationPlate(registrationPlate)) {
    const rawregistrationPlate = registrationPlate
      .toUpperCase()
      .replace(/-/g, '');
    dutchRegistrationPlateRegex.forEach(regexItem => {
      if (rawregistrationPlate.match(regexItem)) {
        formatted = rawregistrationPlate.replace(regexItem, '$1-$2-$3');
      }
    });
  }

  return formatted;
}

export const validation = {
  required: value => (value ? undefined : 'Required'),
  maxLength: max => value =>
    value && value.length > max
      ? `Must be ${max} characters or less`
      : undefined,
  minLength: min => value =>
    value && value.length < min
      ? `Must be ${min} characters or more`
      : undefined,
  number: value =>
    value && Number.isNaN(Number(value)) ? 'Must be a number' : undefined,
  minValue: min => value =>
    value && value < min ? `Must be at least ${min}` : undefined,
  email: value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined,
  alphaNumeric: value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
      ? 'Only alphanumeric characters'
      : undefined,
  postalCode: value =>
    value && !/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i.test(value)
      ? 'Invalid postal code'
      : undefined,
  phoneNumber: value =>
    value &&
    !/^((\+|00(\s|\s?-\s?)?)31(\s|\s?-\s?)?(\(0\)[-\s]?)?|0)[1-9]((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/i.test(
      value,
    )
      ? 'Invalid phone number, must be 10 digits'
      : undefined,
  registrationPlate: value =>
    value && !matchRegistrationPlate(value)
      ? 'Invalid licence plate'
      : undefined,
};

export default validation;
