The RenderComponent

This component is being used to render from the headless API to a React component

This component is supposed to be used only inside of the "Templates" not in other components

If the component property is an Array it will render them inside of an Fragment

```js static
<RenderComponent />
```
