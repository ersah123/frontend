import * as Addons from 'components/addons';
import * as Behaviors from 'components/behaviors';
import * as Collections from 'components/collections';
import * as Elements from 'components/elements';
import * as Modules from 'components/modules';
import * as Views from 'components/views';

import React, { Fragment, PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import uniqid from 'uniqid';
import styles from './RenderComponent.css';

let gutterStyling = null;
let spacerClass = null;
const spacerClasses = ['spacerOne', 'spacerTwo', 'spacerThree', 'spacerFour'];

export const componentShape = PropTypes.shape({
  componentName: PropTypes.string.isRequired,
  componentData: PropTypes.arrayOf(PropTypes.shape()),
});

@themableWithStyles(styles)
class RenderComponent extends PureComponent {
  static propTypes = {
    component: PropTypes.oneOfType([
      PropTypes.arrayOf(componentShape),
      componentShape,
    ]).isRequired,
    children: PropTypes.node,
    rootListIndex: PropTypes.number,
    rootList: PropTypes.arrayOf(componentShape),
    fullWidthComponents: PropTypes.arrayOf(PropTypes.string),
    css: PropTypes.shape(),
  };

  static defaultProps = {
    children: null,
    rootListIndex: null,
    rootList: null,
    fullWidthComponents: [
      'Content',
      'Course',
      'Banner',
      'Hero',
      'Media',
      'Promo',
      'PromoDouble',
      'SubNav',
      'ProductCarousel',
      'ProductHero',
      'Story',
      'Quote',
      'RangeHero',
      'SalonFinder',
      'SearchResult',
    ],
    css: {},
  };

  static getComponent = componentName =>
    Elements[componentName] ||
    Views[componentName] ||
    Collections[componentName] ||
    Modules[componentName] ||
    Behaviors[componentName] ||
    Addons[componentName] ||
    null;

  render() {
    const {
      component,
      rootListIndex,
      rootList,
      fullWidthComponents,
      css,
      children: childrenProp,
      ...wrapperRest
    } = this.props;

    // Check if it is an array else render them separately
    if (Array.isArray(component)) {
      return (
        <Fragment>
          {component.map(comp => (
            <RenderComponent component={comp} key={uniqid()} />
          ))}
        </Fragment>
      );
    }

    // If you are rendering the rootList the components need the rootListIndex as index
    if (rootList && rootListIndex === null) {
      console.error(
        '(RenderComponent) If you are rendering the rootList the components need the rootListIndex as index',
      );
      return null;
    }

    const { id, componentName, componentData, ...componentRest } = component;

    // Go through all brand components
    const Component = RenderComponent.getComponent(componentName);

    let children = childrenProp;

    if (rootList && rootListIndex < rootList.length - 1) {
      if (rootListIndex === 0 && componentName === 'Spacer') {
        // If spacer is the first component add a marginTop gutter
        const pos = parseInt(component.vertical, 10) - 1;
        spacerClass = spacerClasses[pos];
        gutterStyling = {
          ...gutterStyling,
          // ...{ marginTop: `${component.vertical}rem` },
        };
      } else if (
        rootList[rootListIndex + 1] &&
        rootList[rootListIndex + 1].componentName === 'Spacer'
      ) {
        // If next component is spacer add a marginBottom gutter
        const pos = parseInt(rootList[rootListIndex + 1].vertical, 10) - 1;
        spacerClass = spacerClasses[pos];
        gutterStyling = {
          ...gutterStyling,
          // ...{ marginBottom: `${rootList[rootListIndex + 1].vertical}rem` },
        };
      }
    }

    if (!Component) return null;

    // If the first level of the property is a component render it and put it in place
    Object.entries(componentRest).forEach(([k, v]) => {
      if (v && {}.hasOwnProperty.call(v, 'componentName')) {
        componentRest[k] = <RenderComponent component={v} key={v.id} />;
      }
    });

    // If nested componentData convert to react children
    if (componentData && componentData.length > 0) {
      children = componentData.map(c => (
        <RenderComponent component={c} key={c.id} />
      ));
    }

    // Wrap the component in a div if it is the first level deep
    if (rootList) {
      const className = classNames(
        styles.root,
        {
          [styles.fullWidth]: fullWidthComponents.includes(componentName),
        },
        spacerClass,
      );
      spacerClass = null;

      // Use a temp style so we can reset gutter when its used
      const style = gutterStyling;
      gutterStyling = null;
      return (
        <div className={className} style={style}>
          {
            <Component {...componentRest} {...wrapperRest} key={id}>
              {children}
            </Component>
          }
        </div>
      );
    }

    return (
      <Component {...componentRest} {...wrapperRest} key={id}>
        {children}
      </Component>
    );
  }
}

export default RenderComponent;
