/* eslint-env jest */

import React from 'react';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import thunk from 'redux-thunk';

import App from '../../App';
import Layout from './Layout';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const initialState = {
  runtime: {
    navigation: {
      navigationItems: [
        {
          title: 'Shop',
          href: '/shop',
          navigationItems: [],
        },
      ],
    },
    analytics: {},
  },
};

describe('<Layout />', () => {
  describe('is rendering', () => {
    it('children correctly', () => {
      const store = mockStore(initialState);
      const {
        runtime: { navigation, analytics },
      } = store.getState();

      const wrapper = mount(
        <App
          context={{
            lang: 'en-US',
            pathname: '',
            baseUrl: '',
            insertCss: () => {},
            fetch: () => {},
            store,
          }}
        >
          <Layout
            contentOnly
            navigation={navigation.navigationItems}
            analytics={analytics}
          >
            <div className="child" />
          </Layout>
        </App>,
      );

      expect(toJson(wrapper)).toMatchSnapshot();
    });
  });
});
