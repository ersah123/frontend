/* eslint-disable react/no-danger */
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { StickyContainer } from 'react-sticky';
import { themableWithStyles } from 'rsk-components';
import styles from './Layout.css';
import DataLayer from '../../DataLayer';
import { Header, Footer } from '../../views';

@themableWithStyles(styles)
class Layout extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    contentOnly: PropTypes.bool,
    navigation: PropTypes.arrayOf(PropTypes.shape()),
    analytics: PropTypes.shape({
      gtm: PropTypes.arrayOf(PropTypes.shape()),
    }),
  };

  static defaultProps = {
    contentOnly: false,
    navigation: [],
    analytics: {
      gtm: [],
    },
  };

  /**
   * Get footer, main menu or whatever as a type from
   * navigationMain property
   * @param type type of navigation to get, set in property `navigationType`
   *
   * @memberof Layout
   */
  getNavigationMenu = type => {
    if (!type) return null;
    const { navigation } = this.props;
    for (let i = 0; i < navigation.length; i += 1) {
      if (navigation[i].navigationType === type) {
        return navigation[i].navigationItems;
      }
    }
    return null;
  };

  renderGTM = () => {
    const {
      analytics: { gtm },
    } = this.props;

    return gtm.map(item => (
      <DataLayer gtmId={item.gtmId} name={item.gtmName} key={item.gtmId} />
    ));
  };

  render() {
    const { contentOnly, children, analytics } = this.props;
    const { template } = children.props;

    const isSticky = template === 'Home';
    const isCompact = template === 'Layout';
    const isMinimal = !isSticky;

    const navigationMain = this.getNavigationMenu('main');
    const navigationFooter = isCompact
      ? null
      : this.getNavigationMenu('footer');
    const socialLinks = this.getNavigationMenu('socialLinks');

    return (
      <StickyContainer>
        {!contentOnly &&
          navigationMain && (
            <Header
              isSticky={isSticky}
              isMinimal={isMinimal}
              isCompact={isCompact}
              mainMenu={navigationMain}
              topMenu={this.getNavigationMenu('top')}
              subMenu={this.getNavigationMenu('sub')}
            />
          )}
        {children}
        {!contentOnly &&
          navigationFooter && (
            <Footer
              isMinimal={false}
              menuData={navigationFooter}
              socialLinks={socialLinks}
            />
          )}
        {!contentOnly && analytics && analytics.gtm && this.renderGTM()}
      </StickyContainer>
    );
  }
}

export default Layout;
