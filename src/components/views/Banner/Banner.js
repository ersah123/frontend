import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { WithLink } from '../../behaviors';
import styles from './Banner.css';
import { Button, Text } from '../../elements';

@themableWithStyles(styles)
class Banner extends PureComponent {
  static propTypes = {
    /**
     * The title of banner
     */
    title: PropTypes.string,
    /**
     * The title of banner
     */
    text: PropTypes.string,
    /**
     * The url string for button
     */
    buttonUrl: PropTypes.string,
    /**
     * The button text
     */
    buttonText: PropTypes.string,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      content: PropTypes.string,
      action: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: null,
    text: null,
    buttonUrl: null,
    buttonText: null,
    className: null,
    css: {
      root: styles.root,
      content: styles.content,
      action: styles.action,
    },
  };

  render() {
    const {
      className: classNameProp,
      css,
      title,
      text,
      buttonUrl,
      buttonText,
    } = this.props;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <WithLink link={buttonUrl}>
          <div className={css.content}>
            <Text variant="display1" headlineMapping={{ display1: 'h3' }}>
              {title}
            </Text>
            <Text variant="body1" gutterBottom>
              {text}
            </Text>
          </div>
        </WithLink>
        {buttonUrl &&
          buttonText && (
            <div className={css.action}>
              <Button theme="default" to={buttonUrl} wide iconAfter="arrow">
                {buttonText}
              </Button>
            </div>
          )}
      </div>
    );
  }
}

export default Banner;
