Basic Banner component

```js
const props = {
  title: 'Get your personal hair consult',
  text: 'Prepare your next Keune Salon visit with a personal hair consult',
  buttonUrl: '/actionUrl',
  buttonText: 'LOREM IPSUM GALOR',
};

<Banner {...props} />;
```
