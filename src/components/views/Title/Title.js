import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import Text from '../../elements/Text';
import styles from './Title.css';

@themableWithStyles(styles)
class Title extends PureComponent {
  static propTypes = {
    /**
     * Title of component
     */
    title: PropTypes.string,
    /**
     * Subtitle of component
     */
    subtitle: PropTypes.string,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      content: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: null,
    subtitle: null,
    className: null,
    children: null,
    css: {
      root: styles.root,
      content: styles.content,
    },
  };

  render() {
    const {
      title,
      subtitle,
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className} {...rest}>
        <div className={css.content}>
          <Text variant="headline" gutterBottom>
            {subtitle}
          </Text>
          <Text variant="display1">{title}</Text>
        </div>
      </div>
    );
  }
}

export default Title;
