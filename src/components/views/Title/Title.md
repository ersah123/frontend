Basic Title component

```js
const props = {
  title: 'In love with hair since 1922',
  subtitle: 'About Keune',
};

<Title {...props} />;
```
