import { themableWithStyles } from 'rsk-components';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import slugify from 'slugify';

import { FormattedCurrency, Image, Link, Text } from '../../elements';

import styles from './ProductTile.css';

@themableWithStyles(styles)
class ProductTile extends PureComponent {
  static propTypes = {
    /**
     * A `Product` object.
     */
    product: PropTypes.shape().isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      title: PropTypes.string,
      priceSection: PropTypes.string,
      content: PropTypes.string,
      productText: PropTypes.string,
      mediaWrapper: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    css: {
      root: styles.root,
      title: styles.title,
      priceSection: styles.priceSection,
      content: styles.content,
      productText: styles.productText,
      mediaWrapper: styles.mediaWrapper,
    },
  };

  render() {
    const { className: classNameProp, css, product } = this.props;

    const { name, text, price, media, sku } = product;

    let img;
    if (media && media.md) img = media.md;
    if (!img && media[0] && media[0].md) img = media[0].md;

    const link = `/en/product/${sku}/${slugify(name, '-').toLowerCase()}`;

    const className = classNames(css.root, classNameProp);

    return (
      <Link className={className} to={link}>
        <figure className={css.mediaWrapper}>
          <Image {...img} />
        </figure>
        <div className={css.content}>
          <Text
            variant="subheading"
            className={css.title}
            headlineMapping={{ subheading: 'h3' }}
          >
            {name}
          </Text>
          <Text variant="body1" className={css.productText}>
            {text}
          </Text>
          <div className={css.priceSection}>
            {price > 0 && <FormattedCurrency currency="EUR" value={price} />}
            {/* <KeuneIcon name="favourite" /> */}
          </div>
        </div>
      </Link>
    );
  }
}

export default ProductTile;
