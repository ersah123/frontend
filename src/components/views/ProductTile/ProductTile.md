Basic Tile component

```js
const productImage = require('./assets/model.jpg');

const props = {
  product: {
    sku: '27431',
    name: 'Style Dry Shampoo',
    text: 'Leaves hair refreshed and shiny',
    price: 12.1,
    media: {
      md: {
        width: 652,
        height: 652,
        alt: 'Keune',
        src: productImage,
      },
    },
  },
};

<ProductTile {...props} />;
```
