Content component

```js
const image = require('./assets/model2.jpg');

const respImage = {
  id: 1234,
  componentName: 'ResponsiveImage',
  title: 'Test ResponsiveImage Title',
  alt: 'Test ResponsiveImage Alt',
  defaultSize: 'xl',
  sizes: {
    xl: {
      src: image,
      width: 900,
      height: 733,
    },
  },
};

const props = {
  hasMediaRight: false,
  subtitle: 'Clean and cool',
  title: 'Refreshes cool blonde and silver tones',
  text:
    '<p>If you’ve got beautiful cool blonde hair, you’ll want to keep it that way. Care Silver Savior Shampoo contains brass-busting violet pigments to neutralize unwanted warm tones and nourishing Provitamin B5 to keep your hair looking and feeling silky soft. Wheat Proteins protect the inner hair structure and add volume. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process. Here’s a tip: you can even use Care Silver Savior Shampoo to tone after a lightening process.</p>',
  buttonText: 'LOREM IPSUM GALOR',
  youtubeVideoId: '_Ub_w3MyrOw',
  buttonUrl: '/go',
  responsiveImage: <ResponsiveImage {...respImage} />,
};

<Content {...props} />;
```
