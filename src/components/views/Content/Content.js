import { Modal, themableWithStyles } from 'rsk-components';
import React, { PureComponent } from 'react';

import HTMLEllipsis from 'react-lines-ellipsis/lib/html';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withSizes from 'react-sizes';
import { WithLink } from '../../behaviors';
import styles from './Content.css';
import { Button, KeuneIcon, Text } from '../../elements';

@themableWithStyles(styles)
class Content extends PureComponent {
  state = { isOpen: false };

  static propTypes = {
    /**
     * The title string
     */
    title: PropTypes.string,
    /**
     * The subtitle string
     */
    subtitle: PropTypes.string,
    /**
     * The text string
     */
    text: PropTypes.string,
    /**
     * The buttonUrl string url to go
     */
    buttonUrl: PropTypes.string,
    /**
     * The buttonText string
     */
    buttonText: PropTypes.string,
    /**
     * Media - image or video - left or right of text content.
     */
    hasMediaRight: PropTypes.bool,
    /**
     * YouTube video id
     */
    youtubeVideoId: PropTypes.string,
    /**
     * Component for image
     */
    responsiveImage: PropTypes.node.isRequired,
    /**
     * Number of rows of text, depends on viewport
     */
    rowCount: PropTypes.number,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      hasMediaRight: PropTypes.string,
      wrapper: PropTypes.string,
      media: PropTypes.string,
      image: PropTypes.string,
      playButton: PropTypes.string,
      icon: PropTypes.string,
      modal: PropTypes.string,
      embeddedVideo: PropTypes.string,
      content: PropTypes.string,
      text: PropTypes.string,
    }),
  };

  static defaultProps = {
    youtubeVideoId: null,
    className: null,
    children: null,
    title: null,
    subtitle: null,
    text: null,
    rowCount: 15,
    css: {
      root: styles.root,
      hasMediaRight: styles.hasMediaRight,
      wrapper: styles.wrapper,
      media: styles.media,
      image: styles.image,
      playButton: styles.playButton,
      icon: styles.icon,
      modal: styles.modal,
      embeddedVideo: styles.embeddedVideo,
      content: styles.content,
      text: styles.text,
    },
    hasMediaRight: false,
    buttonText: null,
    buttonUrl: null,
  };

  /**
   * If there is a button, remove 2 more rows
   */
  getRowCountWithBtn = (rowCount, isBtn) => (isBtn ? rowCount - 2 : rowCount);

  handleOpen = () => this.setState({ isOpen: true });

  handleClose = () => this.setState({ isOpen: false });

  render() {
    const {
      className: classNameProp,
      children,
      css,
      hasMediaRight,
      subtitle,
      title,
      text,
      responsiveImage,
      buttonText,
      buttonUrl,
      rowCount,
      youtubeVideoId,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      { [css.hasMediaRight]: hasMediaRight },
      classNameProp,
    );

    let buttonUrlOverride = buttonUrl;
    let videoEmbedSrc = null;
    if (youtubeVideoId) {
      videoEmbedSrc = `https://www.youtube.com/embed/${youtubeVideoId}`;
      buttonUrlOverride = '#';
    }

    const { isOpen } = this.state;

    return (
      <div className={className} {...rest}>
        <div className={css.compact}>
          <div className={css.wrapper}>
            <WithLink
              link={buttonUrlOverride}
              onClick={videoEmbedSrc ? this.handleOpen : null}
              className={css.media}
            >
              <div className={css.media}>
                {responsiveImage && (
                  <responsiveImage.type
                    {...responsiveImage.props}
                    className={css.image}
                  />
                )}
                {/* <ResponsiveImage className={css.image} {...responsiveImage} /> */}
                {videoEmbedSrc && (
                  <div className={css.playButton}>
                    <KeuneIcon
                      className={css.icon}
                      name="play"
                      onClick={this.handleOpen}
                    />
                  </div>
                )}
              </div>
            </WithLink>
            <div>
              {isOpen ? (
                <Modal onClose={this.handleClose} className={css.modal}>
                  <div className={css.embeddedVideo}>
                    <iframe
                      title="video"
                      width="720"
                      height="405"
                      src={videoEmbedSrc}
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    />
                  </div>
                </Modal>
              ) : null}
            </div>
            <div className={css.content}>
              <div>
                <Text variant="headline" gutterBottom>
                  {subtitle}
                </Text>
                <Text variant="display3" gutterBottom>
                  {title}
                </Text>
                {/* <RichText html={text} className={css.text} /> */}
                <HTMLEllipsis
                  className={css.text}
                  unsafeHTML={text}
                  maxLine={this.getRowCountWithBtn(
                    rowCount,
                    buttonUrl && buttonText,
                  )}
                  ellipsis="..."
                  basedOn="words"
                />
                {buttonUrl &&
                  buttonText && (
                    <Button iconAfter="arrow" theme="default" to={buttonUrl}>
                      {buttonText}
                    </Button>
                  )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapRowCountToProps = ({ width }) => {
  let rowCount;
  if (width < 480) {
    rowCount = 15;
  } else if (width < 768) {
    rowCount = 15;
  } else if (width < 1024) {
    rowCount = 12;
  } else if (width < 1280) {
    rowCount = 6;
  } else if (width < 1480) {
    rowCount = 10;
  } else if (width < 1920) {
    rowCount = 13;
  } else {
    rowCount = 15;
  }

  return {
    rowCount,
  };
};

export default withSizes(mapRowCountToProps)(Content);
