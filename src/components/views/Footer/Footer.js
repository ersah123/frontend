import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withSizes from 'react-sizes';
import { themableWithStyles } from 'rsk-components';

import { Accordion, AccordionContent, LanguageSwitcher } from '../../modules';
import { Link, Text } from '../../elements';
import { SocialLinks, VisitLocation } from '../../collections';

import logoSrc from './assets/logo-KEUNE.svg';
import styles from './Footer.css';

@themableWithStyles(styles)
@withSizes(({ width }) => ({ isMobile: width < 1280 }))
class Footer extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * Only show the bottom footer
     */
    isMinimal: PropTypes.bool,
    /**
     * A json structure of the menu
     */
    menuData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * A json structure of social links
     */
    socialLinks: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    isMinimal: false,
    css: {
      root: styles.root,
    },
  };

  isActive = tabKey => (tabKey !== undefined ? tabKey : false); // by default all accordion tabs are closed ('false')

  renderMenuItems = col =>
    col.navigationItems.map(({ href, title, type }, i) => {
      const key = `${title}${i}`;
      return (
        <Text
          component={href ? Link : 'p'}
          to={href}
          variant="body1"
          key={key}
          {...(href ? { isExternal: type === 'external' } : {})}
        >
          {title}
        </Text>
      );
    });

  getColClass = colIndex => {
    switch (colIndex) {
      case 0:
        return classNames(styles.threeCol);
      case 1:
        return classNames(styles.fiveCol);
      case 2:
        return classNames(styles.fourCol);
      default:
    }
    return null;
  };

  handleChange = (guid, tabKey) => {
    if (tabKey !== undefined) {
      this.setState({ [guid]: !tabKey });
    } else {
      this.setState({ [guid]: true });
    }
  };

  // check if all tabs are open
  renderMenuColumns = list => {
    const { isMobile } = this.props;
    return list.map((col, i) => {
      const guid = `${col.title}${i}`;
      const { [guid]: tabKey } = { ...this.state };

      return (
        <Accordion key={guid} className={this.getColClass(i)}>
          <AccordionContent
            title={
              <Text key={guid} className={styles.title} variant="title">
                {col.title}
              </Text>
            }
            className={styles.accordionTitle}
            isActive={!isMobile || this.isActive(tabKey)}
            onClick={() => this.handleChange(guid, tabKey)}
            {...(isMobile ? { toggleIcon: 'plus' } : {})}
          >
            {this.renderMenuItems(col)}
          </AccordionContent>
        </Accordion>
      );
    });
  };

  render() {
    const {
      className: classNameProp,
      children,
      isMinimal,
      css,
      menuData,
      isMobile,
      socialLinks,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);

    const mainNavigation = menuData;

    return (
      <footer className={className} {...rest}>
        {!isMinimal && (
          <div className={styles.main}>
            <div className={styles.inner}>
              {this.renderMenuColumns(mainNavigation)}
              <div className={classNames(styles.socialCol)}>
                <VisitLocation />
                {socialLinks && <SocialLinks links={socialLinks} />}
              </div>
            </div>
          </div>
        )}
        <div className={styles.bottom}>
          <div className={styles.main}>
            <div className={styles.inner}>
              <span>
                Copyright © 2019 Keune International
                <a href="/#">Privacy &amp; Cookie Policy</a>
                <a href="/#">Terms of use</a>
              </span>
              <LanguageSwitcher />
            </div>
          </div>
        </div>
        <div>
          <img src={logoSrc} alt="Logo KEUNE" className={styles.logo} />
          <Text className={styles.payoff} variant="headline">
            Your best you
          </Text>
        </div>
      </footer>
    );
  }
}

export default Footer;
