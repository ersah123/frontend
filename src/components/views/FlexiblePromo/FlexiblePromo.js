import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './FlexiblePromo.css';
import Hero from '../Hero/Hero';
import { HeroPropTypes, HeroDefaultPropTypes } from '../../../types';

@themableWithStyles(styles)
class FlexiblePromo extends PureComponent {
  static propTypes = {
    ...HeroPropTypes,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      logoImage: PropTypes.string,
      alignCenter: PropTypes.string,
      contentWrapper: PropTypes.string,
      content: PropTypes.string,
      subtitle: PropTypes.string,
      title: PropTypes.string,
      text: PropTypes.string,
      button: PropTypes.string,
      bgWrapper: PropTypes.string,
      bg: PropTypes.string,
      bgOverlay: PropTypes.string,
    }),
  };

  static defaultProps = {
    ...HeroDefaultPropTypes,
    align: 'leftTop',
    className: null,
    children: null,
    css: {
      root: styles.root,
      logoImage: styles.logoImage,
      alignCenter: styles.alignCenter,
      contentWrapper: styles.contentWrapper,
      content: styles.content,
      subtitle: styles.subtitle,
      title: styles.title,
      text: styles.text,
      button: styles.button,
      bgWrapper: styles.bgWrapper,
      bg: styles.bg,
      bgOverlay: styles.bgOverlay,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <Hero
          {...rest}
          overrideCss={css}
          align="leftBottom"
          hasScrollTo={false}
        >
          {children}
        </Hero>
      </div>
    );
  }
}

export default FlexiblePromo;
