Basic FlexiblePromo component

```js
const backgroundImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const props = {
  justify: 'start',
  align: 'leftBottom',
  isFullHeight: true,
  headline: 'Our new Ultimate Blonde line',
  title: 'Go Brighter',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'LOREM IPSUM GALOR',
  buttonUrl: '/go',
  theme: 'light',
  backgroundImage: <ResponsiveImage {...backgroundImage} />,
};
<FlexiblePromo {...props} />;
```
