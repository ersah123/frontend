import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Input, themableWithStyles } from 'rsk-components';
import { KeuneIcon } from '../../elements';

import styles from './SearchInput.css';

const mapStateToProps = ({ form, search, runtime }) => ({
  searchInput: form.search,
  searchTerm: search.searchTerm,
  runtime,
});

@connect(mapStateToProps)
@reduxForm({
  form: 'searchInput',
})
@themableWithStyles(styles)
class SearchInput extends PureComponent {
  static propTypes = {
    /**
     * Prop for q
     */
    searchInput: PropTypes.shape({}),
    /**
     * Prop for searchTerm
     */
    searchTerm: PropTypes.string,
    /**
     * Prop for placeholder
     */
    placeholder: PropTypes.string,
    /**
     * Prop for runtime
     */
    runtime: PropTypes.shape({}),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      magnifier: PropTypes.string,
    }),
  };

  static defaultProps = {
    searchInput: null,
    searchTerm: null,
    placeholder: null,
    runtime: {},
    className: null,
    css: {
      root: styles.root,
      magnifier: styles.magnifier,
    },
  };

  render() {
    const {
      runtime,
      searchTerm,
      placeholder,
      className: classNameProp,
      css,
    } = this.props;

    const searchPage = runtime.siteInit.pages.search
      ? runtime.siteInit.pages.search
      : '/';

    const className = classNames(css.root, classNameProp);
    return (
      <div key="searchInput" className={className}>
        <form
          key="searchForm"
          action={searchPage}
          method="GET"
          ref={el => {
            this.form = el;
          }}
        >
          <Input
            key="searchField"
            type="text"
            name="q"
            ref={el => {
              this.input = el;
            }}
            label=""
            value={searchTerm}
            placeholder={placeholder}
          />
          <KeuneIcon
            name="search"
            className={css.magnifier}
            onClick={() => this.form.submit()}
          />
        </form>
      </div>
    );
  }
}

export default SearchInput;
