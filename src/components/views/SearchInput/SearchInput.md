SearchInput component

```js
const { Provider } = require('react-redux');
const configureStore = require('../../../store/configureStore').default;
const initialState = {
  runtime: {
    siteInit: {
      pages: {
        search: '/#!/SearchInput',
      },
    },
  },
};
const store = configureStore(initialState, {});

const { reduxForm } = require('redux-form');
const WrappedInput = reduxForm({ form: 'simple' })(SearchInput);

<Provider store={store}>
  <WrappedInput placeholder="How can we help you?" />
</Provider>;
```
