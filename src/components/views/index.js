/* eslint-disable import/prefer-default-export */

/**
 * VIEWS
 */
export { default as Banner } from './Banner';
export { default as Content } from './Content';
export { default as Footer } from './Footer';
export { default as Header } from './Header';
export { default as CookiePopup } from './CookiePopup';
export { default as CookieDetail } from './CookieDetail';
export {
  default as StickyHeader,
} from '../collections/ProductHero/StickyHeader';
export { default as Hero } from './Hero';
export { default as RangeHero } from './RangeHero';
export { default as Promo } from './Promo';
export { default as PromoDouble } from './Promo/PromoDouble';
export { default as FlexiblePromo } from './FlexiblePromo';
export { default as Story } from './Story';
export { default as Quote } from './Quote';
export { default as Title } from './Title';
export { default as ProductTile } from './ProductTile';
export { default as ProductSpecs } from './ProductSpecs';
export { default as SearchInput } from './SearchInput';
export { default as SearchResult } from './SearchResult';
export { default as NoResult } from './SearchResult/NoResult';
export { default as SalonFinder } from './SalonFinder';
export { default as Loading } from './Loading';

/**
 * DIRECT RSK Imports
 */
export { ButtonGroup, Card, SubNav, Usp } from 'rsk-components';
