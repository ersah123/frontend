import React, { PureComponent } from 'react';
import { Swiper, themableWithStyles } from 'rsk-components';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';

import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { KeuneIcon, Link, Text } from '../../../../elements';
import styles from './SubMenuSwiper.css';

@themableWithStyles(styles)
class SubMenuSwiper extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      arrowIcon: PropTypes.string,
      breadcrumbWrapper: PropTypes.string,
      navItemWrapper: PropTypes.string,
      isSubNavCarouselHidden: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
      arrowIcon: styles.arrowIcon,
      breadcrumbWrapper: styles.breadcrumbWrapper,
      navItemWrapper: styles.navItemWrapper,
      isSubNavCarouselHidden: styles.isSubNavCarouselHidden,
    },
  };

  state = {
    breadcrumbItems: [],
    isSubNavCarouselHidden: true,
    currentMenus: [],
  };

  constructor(props) {
    super(props);
    this.swiper = null;
  }

  componentWillReceiveProps(nextProps) {
    this.initTabs(nextProps);
  }

  initTabs = ({ currentNavItem, subMenuItems, isMainNavHidden }) => {
    if (currentNavItem !== '') {
      this.setState({
        breadcrumbItems: [currentNavItem],
        isSubNavCarouselHidden: !isMainNavHidden,
        currentMenus: [subMenuItems.navigationItems],
      });
    } else {
      this.resetNav();
    }
  };

  addNavItem = ({ title, navigationItems }) => {
    const { breadcrumbItems, currentMenus } = this.state;
    if (!breadcrumbItems.includes(title) && title !== '') {
      breadcrumbItems.push(title);
      currentMenus.push(navigationItems);
      this.setState({
        breadcrumbItems,
        currentMenus,
      });
      this.forceUpdate();
    }
  };

  handleClick = (e, navItem) => {
    e.preventDefault();
    if (navItem.navigationItems) {
      // if there are sub items
      this.addNavItem(navItem);
    }
  };

  renderNavItems = (currentMenu, { css } = this.props) =>
    currentMenu.map(item => (
      <CSSTransition
        in
        timeout={2000}
        key={item.title}
        classNames="tab-content"
      >
        <div className={css.navItemWrapper}>
          <Text
            component={Link}
            to={item.href}
            variant="display3"
            role="menuitem"
            onClick={e => this.handleClick(e, item)}
          >
            {item.title}
          </Text>
        </div>
      </CSSTransition>
    ));

  renderTabPanels = ({ currentMenus, breadcrumbItems } = this.state) =>
    breadcrumbItems.map((item, i) => (
      <TabPanel key={`tabPanel-${item}`} forceRender>
        {this.renderNavItems(currentMenus[i])}
      </TabPanel>
    ));

  /**
   * remove every other menu except clicked one from end of menu list and breadcrumb
   */
  removeMenu = navTitle => {
    const { breadcrumbItems, currentMenus } = this.state;
    const clickedBreadcrumbIndex = breadcrumbItems.indexOf(navTitle) + 1;
    const nextItems = breadcrumbItems.slice(0, clickedBreadcrumbIndex);
    const nextMenus = currentMenus.slice(0, clickedBreadcrumbIndex);
    this.setState({
      breadcrumbItems: nextItems,
      currentMenus: nextMenus,
    });
  };

  handleBreadcrumbClick = navTitle => {
    const { breadcrumbItems } = this.state;
    if (breadcrumbItems.indexOf(navTitle) < breadcrumbItems.length - 1) {
      // if is it already active item = last item, dont remove
      this.removeMenu(navTitle);
    }
  };

  renderBreadcrumbItems = ({ breadcrumbItems } = this.state) =>
    breadcrumbItems.map((item, i) => {
      const key = `menuItem-${item}${i}`;
      return (
        <Tab key={key}>
          <Text
            variant="subheading"
            onClick={() => this.handleBreadcrumbClick(item)}
          >
            <CSSTransition in timeout={500} classNames="breadcrumb-item">
              <span>{item}</span>
            </CSSTransition>
          </Text>
        </Tab>
      );
    });

  resetNav = () => {
    const { resetNav } = this.props;
    this.setState({
      isSubNavCarouselHidden: true,
    });
    resetNav();
  };

  swiperRef = ref => {
    if (ref) this.swiper = ref.swiper;
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      subMenuItems,
      currentNavItem,
      resetNav,
      isMainNavHidden,
      // The rest values
      ...rest
    } = this.props;

    const params = {
      slidesPerView: 'auto',
      spaceBetween: 30,
      rebuildOnUpdate: true, // for adding 30 spacing to dynamic items
    };

    const { breadcrumbItems, isSubNavCarouselHidden } = this.state;

    const className = classNames(
      css.root,
      { [css.isSubNavCarouselHidden]: isSubNavCarouselHidden },
      classNameProp,
    );

    return (
      <div className={className} {...rest}>
        {breadcrumbItems.length > 0 &&
          subMenuItems && (
            <Tabs
              selectedIndex={breadcrumbItems.length - 1}
              onSelect={() => {}}
            >
              <div className={css.breadcrumbWrapper}>
                <KeuneIcon
                  name="arrow"
                  className={css.arrowIcon}
                  onClick={() => this.resetNav()}
                />

                <TabList>
                  <Swiper
                    swiperParams={params}
                    ref={this.swiperRef}
                    selected={breadcrumbItems.length - 1}
                  >
                    {this.renderBreadcrumbItems()}
                  </Swiper>
                </TabList>
              </div>
              <div className={styles.tabContentWrapper}>
                {this.renderTabPanels()}
              </div>
            </Tabs>
          )}
      </div>
    );
  }
}

export default SubMenuSwiper;
