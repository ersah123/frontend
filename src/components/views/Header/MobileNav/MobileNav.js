import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import SubMenuSwiper from './SubMenuSwiper';
import styles from './MobileNav.css';
import { KeuneIcon, Link, Text } from '../../../elements';
import { SearchInput } from '../..';

@themableWithStyles(styles)
class MobileNav extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    handleNavToggle: PropTypes.func,
    mainMenu: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        href: PropTypes.string,
        navigationItems: PropTypes.array,
      }),
    ),
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      nav: PropTypes.string,
      // user: PropTypes.string,
      isMainNavHidden: PropTypes.string,
      mainNavWrapper: PropTypes.string,
      menuContainer: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    mainMenu: null,
    handleNavToggle: () => {},
    css: {
      root: styles.root,
      nav: styles.nav,
      // user: styles.user,
      isMainNavHidden: styles.isMainNavHidden,
      mainNavWrapper: styles.mainNavWrapper,
      menuContainer: styles.menuContainer,
    },
  };

  state = {
    isMainNavHidden: false,
    currentNavItem: '',
    currentSubMenu: {
      navigationItems: [],
    },
  };

  getSubMenuItems(currentNavItem) {
    const { mainMenu } = this.props;
    if (currentNavItem === '') return [];
    return mainMenu.filter(item => item.title === currentNavItem)[0];
  }

  handleNavToggle = () => {
    this.setState({
      isMainNavHidden: false,
      currentNavItem: '',
    });
    const { handleNavToggle } = this.props;
    handleNavToggle();
  };

  // show main nav
  resetNav = () => {
    this.setState({
      isMainNavHidden: false,
      // currentNavItem: '',
    });
  };

  handleClick = (e, linkTitle) => {
    e.preventDefault();
    const currentSubMenu = this.getSubMenuItems(linkTitle);
    this.setState({
      isMainNavHidden: currentSubMenu.navigationItems.length > 0,
      currentNavItem: linkTitle,
      currentSubMenu,
    });
  };

  renderMainItems = list =>
    list.map(({ href, title }) => (
      <li key={title}>
        <Text
          component={Link}
          to={href}
          variant="display3"
          role="menuitem"
          onClick={e => this.handleClick(e, title)}
        >
          {title}
        </Text>
      </li>
    ));

  render() {
    const { className: classNameProp, css, mainMenu } = this.props;

    const { isMainNavHidden, currentNavItem, currentSubMenu } = this.state;

    const className = classNames(
      css.root,
      classNameProp,
      styles.mobileNavHeader,
    );

    const mainNav = classNames(css.mainNavWrapper, {
      [css.isMainNavHidden]: isMainNavHidden,
    });

    return (
      <div className={className}>
        <div className={styles.iconsSection}>
          <div>
            <KeuneIcon
              name="close"
              onClick={this.handleNavToggle}
              onKeyPress={this.handleNavToggle}
              role="menu"
              tabIndex="0"
            />
          </div>
          {/* NOT in MVP */}
          {/* <div>
            <KeuneIcon name="favourite" />
            <KeuneIcon name="bag" />
          </div> */}
        </div>
        <div className={css.menuContainer}>
          <div className={mainNav}>
            <SearchInput placeholder="How can we help you?" />
            <nav className={css.nav}>
              <ul role="menu">{this.renderMainItems(mainMenu)}</ul>
            </nav>
            {/*  NOT in MVP */}
            {/* <nav className={css.user}>
              <ul>
                <li>
                  <Text variant="body1" component={Link} to="Inloggen">Inloggen</Text>
                </li>
                <li>
                  <Text variant="body1" component={Link} to="internal-page">Switch to pro</Text>
                </li>
                <li>
                  <Text variant="body1" component={Link} to="internal-page">Customer service</Text>
                </li>
              </ul>
            </nav> */}
          </div>
          <SubMenuSwiper
            subMenuItems={currentSubMenu}
            currentNavItem={currentNavItem}
            resetNav={this.resetNav}
            isMainNavHidden={isMainNavHidden}
          />
        </div>
      </div>
    );
  }
}

export default MobileNav;
