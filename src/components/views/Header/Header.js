import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Sticky } from 'react-sticky';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import withSizes from 'react-sizes';
import { KeuneIcon, Link, Text } from '../../elements';
import { SearchInput, CookiePopup } from '..';

import DesktopNav from './DesktopNav/DesktopNav';
import MobileNav from './MobileNav/MobileNav';
import logoDesktop from './assets/logo-desktop.svg';
import logoMobile from './assets/logo-KEUNE.svg';
import { remToPixels } from '../../../utils/helperFunc';
import { LOGO_MIN_SCROLL_AMOUNT } from './constants';

import styles from './Header.css';

@themableWithStyles(styles)
@withSizes(({ width }) => ({ isMobile: width < 1280 }))
class Header extends React.Component {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
    /**
     * Main menu items.
     */
    mainMenu: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * Top menu items, most top navigation.
     */
    topMenu: PropTypes.arrayOf(PropTypes.shape()),
    /**
     * Sub menu items, next to or below main.
     */
    subMenu: PropTypes.arrayOf(PropTypes.shape()),
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      actions: PropTypes.string,
      hideDesktop: PropTypes.string,
      hideMobile: PropTypes.string,
      isOpen: PropTypes.string,
      logo: PropTypes.string,
      main: PropTypes.string,
      noScroll: PropTypes.string,
      root: PropTypes.string,
      searchInput: PropTypes.string,
      small: PropTypes.string,
      stickedLogo: PropTypes.string,
      toggle: PropTypes.string,
      top: PropTypes.string,
      compact: PropTypes.string,
    }),
    /**
     * Provided by `react-sizes`.
     */
    isMobile: PropTypes.bool.isRequired,
    /**
     * Prop to make header sticky
     */
    isSticky: PropTypes.bool,
    /**
     * Prop to make header compact
     */
    isCompact: PropTypes.bool,
    /**
     * Prop to make header minimal
     */
    isMinimal: PropTypes.bool,
  };

  static defaultProps = {
    className: null,
    css: {
      actions: styles.actions,
      hideDesktop: styles.hideDesktop,
      hideMobile: styles.hideMobile,
      isOpen: styles.isOpen,
      logo: styles.logo,
      main: styles.main,
      noScroll: styles.noScroll,
      root: styles.root,
      searchInput: styles.searchInput,
      small: styles.small,
      stickedLogo: styles.stickedLogo,
      toggle: styles.toggle,
      top: styles.top,
      compact: styles.compact,
    },
    topMenu: null,
    subMenu: null,
    isSticky: false,
    isCompact: false,
    isMinimal: false,
  };

  state = {
    isOpen: false,
  };

  componentDidMount() {
    window.addEventListener('resize', this.removeNoScroll, true);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.removeNoScroll, true);
  }

  removeNoScroll = () => {
    const { css } = this.props;
    this.setState({ isOpen: false });
    document.querySelector('html').classList.remove(css.noScroll);
  };

  handleNavToggle = () => {
    const { css } = this.props;
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    document.querySelector('html').classList.toggle(css.noScroll);
  };

  getScrolledStyleClass = distanceFromTop => {
    const { css } = this.props;
    // after 10px scroll amount, logo will be smaller
    if (distanceFromTop < -10) {
      return css.stickedLogo;
    }
    return null;
  };

  /**
   * Adds header height difference after animation when scrolled to top
   */
  addHeaderHeight = distanceFromTop => {
    // after header has position-fixed
    const header = document.getElementsByTagName('header')[0];
    const headerFixedSpacer = header.previousSibling; // <Sticky /> actually creates div before header to give spacing to, it uses padding-bottom
    if (distanceFromTop >= 0) {
      // when top of the pagef
      const { headerHeight } = this.state;
      const currentPadding = parseInt(
        headerFixedSpacer.style.paddingBottom.replace('px', ''),
        10,
      );
      headerFixedSpacer.style.paddingTop = `${headerHeight - currentPadding}px`; // add the difference between after and before animation of the header height
    } else {
      // when scrolling down
      headerFixedSpacer.style.paddingTop = `${0}px`;
    }
  };

  renderMainItems = list =>
    list.map(({ href, title, type }) => (
      <li key={title}>
        <Text
          component={Link}
          isExternal={type === 'external'}
          to={href}
          variant="body1"
          role="menuitem"
        >
          {title}
        </Text>
      </li>
    ));

  renderTopItems = list =>
    list.map(({ href, title, type }) => (
      <Text
        component={Link}
        isExternal={type === 'external'}
        to={href}
        variant="caption"
        key={title}
        role="menuitem"
      >
        {title}
      </Text>
    ));

  getScrolledStyleClass = distanceFromTop => {
    const { css } = this.props;
    // after 10px scroll amount, logo will be smaller
    if (distanceFromTop < LOGO_MIN_SCROLL_AMOUNT) {
      return css.stickedLogo;
    }
    return null;
  };

  /**
   * Adds header height difference after animation when scrolled to top
   */
  addHeaderHeight = distanceFromTop => {
    // after header has position-fixed
    const header = document.getElementsByTagName('header')[0];
    const headerFixedSpacer = header.previousSibling; // <Sticky /> actually creates div before header to give spacing to, it uses padding-bottom
    if (distanceFromTop >= 0) {
      // when top of the page
      headerFixedSpacer.style.paddingTop = `${distanceFromTop}px`; // add the difference between after and before animation of the header height
    } else {
      // when scrolling down
      headerFixedSpacer.style.paddingTop = `${0}px`;
    }
  };

  renderHeader = ({ distanceFromTop, style, isSticky, calculatedHeight }) => {
    const { isMobile, isCompact, isMinimal, css } = this.props;
    if (!isMobile && isSticky) {
      const { headerheight, headerheightsmall } = styles;
      const height =
        distanceFromTop < LOGO_MIN_SCROLL_AMOUNT
          ? remToPixels(headerheightsmall)
          : remToPixels(headerheight);
      this.addHeaderHeight(height - calculatedHeight); // side effect
    }

    const { className: classNameProp, mainMenu, subMenu, topMenu } = this.props;

    const { isOpen } = this.state;

    const className = classNames(
      css.root,
      {
        [css.isOpen]: isOpen,
      },
      classNameProp,
      isCompact ? css.compact : null,
    );
    return (
      <header className={className} style={style}>
        <CookiePopup />
        {topMenu &&
          !isCompact && (
            <nav className={css.top}>{this.renderTopItems(topMenu)}</nav>
          )}
        <div className={css.small}>
          {!isCompact && (
            <Fragment>
              <div className={classNames(css.toggle, css.hideDesktop)}>
                <KeuneIcon
                  name="menu"
                  onClick={this.handleNavToggle}
                  onKeyPress={this.handleNavToggle}
                  role="menu"
                  tabIndex="0"
                />
              </div>
              <SearchInput
                placeholder="How can we help you?"
                className={classNames(css.searchInput, css.hideMobile)}
              />
            </Fragment>
          )}
          <Link
            to="/"
            className={classNames(
              css.logo,
              this.getScrolledStyleClass(distanceFromTop),
              isMinimal ? css.stickedLogo : null,
            )}
          >
            <img
              src={logoDesktop}
              alt="Logo KEUNE"
              className={!isCompact && css.hideMobile}
            />
            {!isCompact && (
              <img
                src={logoMobile}
                alt="Logo KEUNE"
                className={css.hideDesktop}
              />
            )}
          </Link>
          {/* @TODO this div makes logo NOT centered on desktop */}
          {!isCompact && <div className={css.actions} />}
        </div>
        {!isCompact && (
          <div className={css.main}>
            {/* Navigation */}
            <MobileNav
              mainMenu={mainMenu}
              handleNavToggle={this.handleNavToggle}
            />
            <DesktopNav mainMenu={mainMenu} subMenu={subMenu} />
            {/* ICONS HERE */}
          </div>
        )}
      </header>
    );
  };

  render() {
    const { isMobile, isSticky } = this.props;
    const { isOpen } = this.state;
    if (!isOpen && (isMobile || isSticky)) {
      return <Sticky>{this.renderHeader}</Sticky>;
    }
    return this.renderHeader({
      distanceFromTop: null,
      style: null,
      isSticky: false,
    });
  }
}
export default Header;
