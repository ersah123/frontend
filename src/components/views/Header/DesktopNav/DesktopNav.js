import React, { Fragment, PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { Link, Text } from '../../../elements';
import FlyoutMenu from './FlyoutMenu';
import SubMenu from './SubMenu';
import { DEFAULT_FLYOUT } from '../constants';
import styles from './DesktopNav.css';

@themableWithStyles(styles)
class DesktopNav extends PureComponent {
  state = { activeFlyout: DEFAULT_FLYOUT };

  flyoutTimer = null;

  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
    },
  };

  handleMouseOver = target => {
    const { activeFlyout } = this.state;
    if (activeFlyout !== target) {
      if (target === DEFAULT_FLYOUT) {
        this.flyoutTimer = window.setTimeout(() => {
          this.setState({ activeFlyout: target || DEFAULT_FLYOUT });
        }, 200);
      } else {
        window.clearTimeout(this.flyoutTimer);
        this.setState({ activeFlyout: target || DEFAULT_FLYOUT });
      }
    } else {
      window.clearTimeout(this.flyoutTimer);
    }
  };

  renderMainItems = list =>
    list.map(({ href, title, type }) => (
      <li
        key={title}
        onMouseOver={() => this.handleMouseOver(title)}
        onFocus={() => this.handleMouseOver(title)}
        onMouseOut={() => this.handleMouseOver(DEFAULT_FLYOUT)}
        onBlur={() => this.handleMouseOver(DEFAULT_FLYOUT)}
      >
        <Text
          component={Link}
          isExternal={type === 'external'}
          to={href}
          variant="subheading"
          role="menuitem"
        >
          {title}
        </Text>
      </li>
    ));

  renderTopItems = list =>
    list.map(({ href, title, type }) => (
      <Text
        component={Link}
        isExternal={type === 'external'}
        to={href}
        variant="caption"
        key={title}
        role="menuitem"
      >
        {title}
      </Text>
    ));

  renderFlyout = target => {
    const { mainMenu } = this.props;

    return mainMenu.map(menuItem => {
      if (menuItem.title === target && menuItem.navigationItems.length) {
        return (
          <FlyoutMenu
            {...menuItem}
            key={menuItem.title}
            onMouseOver={() => this.handleMouseOver(menuItem.title)}
            onFocus={() => this.handleMouseOver(menuItem.title)}
            onMouseOut={() => this.handleMouseOver(DEFAULT_FLYOUT)}
            onBlur={() => this.handleMouseOver(DEFAULT_FLYOUT)}
          />
        );
      }
      return null;
    });
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      mainMenu,
      subMenu,
      // The rest values
      ...rest
    } = this.props;
    const { activeFlyout } = this.state;
    const className = classNames(css.root, classNameProp, styles.nav);

    return (
      <Fragment>
        <nav className={className} {...rest}>
          <ul role="menu">{this.renderMainItems(mainMenu)}</ul>
          <SubMenu subMenu={subMenu} />
          {this.renderFlyout(activeFlyout)}
        </nav>
      </Fragment>
    );
  }
}

export default DesktopNav;
