Basic DesktopNav component

```js
const menuMain = require('../fixtures/menu-main.json');
const menuSub = require('../fixtures/menu-sub.json');

<DesktopNav
  mainMenu={menuMain.navigationItems}
  subMenu={menuSub.navigationItems}
/>;
```
