import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './SubMenu.css';
import { Link, Text } from '../../../../elements';

@themableWithStyles(styles)
class SubMenu extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
    },
  };

  renderTopItems = list =>
    list.map(({ href, title, type }) => (
      <Text
        component={Link}
        isExternal={type === 'external'}
        to={href}
        variant="caption"
        key={title}
        role="menuitem"
      >
        {title}
      </Text>
    ));

  render() {
    const {
      className: classNameProp,
      children,
      css,
      subMenu,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className} {...rest}>
        {/* why we need this check here? is it optional? */}
        {subMenu && (
          <nav className={styles.sub}>{this.renderTopItems(subMenu)}</nav>
        )}
      </div>
    );
  }
}

export default SubMenu;
