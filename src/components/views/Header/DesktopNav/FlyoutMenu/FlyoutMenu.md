Basic FlyoutMenu component

```js
const navItems = require('../../fixtures/menu-full.json');

<FlyoutMenu navigationItems={navItems.navigationItems} />;
```
