import React, { Fragment, PureComponent } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  ResponsiveImage as ResponsiveImageComponent,
  themableWithStyles,
} from 'rsk-components';
import { KeuneIcon, Link, Text } from '../../../../elements';
import { FlexiblePromo } from '../../..';
import ResponsiveImage from '../../../../../models/ResponsiveImage';

// eslint-disable-next-line css-modules/no-unused-class
import mainPromoStyles from './MainPromo.css';
// eslint-disable-next-line css-modules/no-unused-class
import subPromoStyles from './SubPromo.css';
import styles from './FlyoutMenu.css';

@themableWithStyles(styles)
class FlyoutMenu extends PureComponent {
  state = { activeItem: '' };

  itemsIn = true;

  animationSpecs = {
    mainDuration: 300,
    subDuration: 400,
    delay: 40,
    timeoutBuffer: 40,
  };

  noMarginMenus = ['Brands'];

  fullHeightMenus = ['So Pure by Keune'];

  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      mainMenuItem: PropTypes.string,
      visible: PropTypes.string,
      mainMenu: PropTypes.string,
      mainItem: PropTypes.string,
      subMenu: PropTypes.string,
      subMenuSection: PropTypes.string,
      subMenuTitle: PropTypes.string,
      subMenuItem: PropTypes.string,
      buttonBottom: PropTypes.string,
      noMargin: PropTypes.string,
      fullHeightSubmenu: PropTypes.string,
      justifySpaceBetween: PropTypes.string,
      mainPromo: PropTypes.string,
      subPromo: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
      mainMenuItem: styles.mainMenuItem,
      visible: styles.visible,
      mainMenu: styles.mainMenu,
      mainItem: styles.mainItem,
      subMenu: styles.subMenu,
      subMenuSection: styles.subMenuSection,
      subMenuTitle: styles.subMenuTitle,
      subMenuItem: styles.subMenuItem,
      buttonBottom: styles.buttonBottom,
      noMargin: styles.noMargin,
      fullHeightSubmenu: styles.fullHeightSubmenu,
      justifySpaceBetween: styles.justifySpaceBetween,
      mainPromo: styles.mainPromo,
      subPromo: styles.subPromo,
    },
  };

  handleMouseOver = target => {
    const { activeItem } = this.state;

    if (activeItem !== target) {
      this.setState({ activeItem: target || '' });
    }
  };

  renderMainItems = () => {
    const { navigationItems, css } = this.props;
    const { activeItem } = this.state;

    return navigationItems.map((item, i) => {
      const delay = i * 30;
      return (
        <CSSTransition
          in={this.itemsIn}
          timeout={
            this.animationSpecs.mainDuration +
            delay +
            this.animationSpecs.timeoutBuffer
          }
          classNames="flyoutTitle"
          key={`submenuMainItems-${item.title}`}
        >
          <div
            className={css.mainItem}
            onMouseOver={() => this.handleMouseOver(item.title)}
            onFocus={() => this.handleMouseOver(item.title)}
            style={{ transitionDelay: `${delay}ms` }}
          >
            <Text
              variant="body2"
              component={Link}
              to={item.href ? item.href : '#'}
              className={css.mainMenuItem}
            >
              {item.title}
            </Text>
            {item.navigationItems.length > 0 && (
              <KeuneIcon
                name="arrow"
                className={activeItem === item.title ? css.visible : null}
              />
            )}
          </div>
        </CSSTransition>
      );
    });
  };

  renderSubItems = (item, { css } = this.props) =>
    item.navigationItems.map((submenu, idx) => {
      if (submenu.navigationItems && submenu.navigationItems.length) {
        const className = classNames(
          css.subMenuSection,
          this.fullHeightMenus.includes(submenu.title)
            ? css.fullHeightSubmenu
            : null,
        );
        return (
          <div className={className} key={`submenuSection-${submenu.title}`}>
            <CSSTransition
              in={this.itemsIn}
              timeout={
                this.animationSpecs.mainDuration +
                this.animationSpecs.timeoutBuffer
              }
              classNames="flyoutTitle"
              key={`submenuTitle-${submenu.title}`}
            >
              <Text
                component={Link}
                to={submenu.href}
                variant="subheading"
                className={css.subMenuTitle}
              >
                {submenu.title}
              </Text>
            </CSSTransition>
            <TransitionGroup key={`transition-${submenu.title}`}>
              <Fragment>
                {submenu.navigationItems &&
                  submenu.navigationItems.map((submenuItem, i) => {
                    const delay = i * this.animationSpecs.delay;
                    return (
                      <CSSTransition
                        in={this.itemsIn}
                        timeout={
                          this.animationSpecs.subDuration +
                          delay +
                          this.animationSpecs.timeoutBuffer
                        }
                        classNames="flyoutItem"
                        key={`submenuItem-${submenu.title}-${
                          submenuItem.title
                        }`}
                      >
                        <Text
                          component={Link}
                          to={submenuItem.href}
                          variant="body1"
                          className={css.subMenuItem}
                          style={{ transitionDelay: `${delay}ms` }}
                        >
                          {submenuItem.title}
                        </Text>
                      </CSSTransition>
                    );
                  })}
              </Fragment>
            </TransitionGroup>
          </div>
        );
      }
      const delay = idx * this.animationSpecs.delay;
      return (
        <CSSTransition
          in={this.itemsIn}
          timeout={
            this.animationSpecs.subDuration +
            delay +
            this.animationSpecs.timeoutBuffer
          }
          classNames="flyoutItem"
          key={`submenuItem-${submenu.title}`}
        >
          <Text
            component={Link}
            to={submenu.href}
            variant="body1"
            className={css.subMenuItem}
            style={{ transitionDelay: `${delay}ms` }}
          >
            {submenu.title}
          </Text>
        </CSSTransition>
      );
    });

  renderSubMenu = () => {
    const { navigationItems, css } = this.props;
    const { activeItem } = this.state;
    return navigationItems.map(item => {
      if (item.title === activeItem) {
        const className = classNames(
          css.subMenu,
          this.noMarginMenus.includes(activeItem) ? css.noMargin : null,
        );
        return (
          <Fragment key={`submenu-${item.title}`}>
            <div className={className}>
              {this.renderSubItems(item)}
              {item.buttonText && (
                <CSSTransition
                  in={this.itemsIn}
                  timeout={
                    this.animationSpecs.mainDuration +
                    this.animationSpecs.timeoutBuffer
                  }
                  classNames="flyoutTitle"
                  key={item.buttonText}
                >
                  <Link to={item.buttonUrl} className={css.buttonBottom}>
                    <Text variant="subheading">{item.buttonText}</Text>
                    <KeuneIcon name="arrow" />
                  </Link>
                </CSSTransition>
              )}
            </div>
            {this.renderPromo(subPromoStyles, 'sub')}
          </Fragment>
        );
      }
      return null;
    });
  };

  preparePromo = promo => {
    const responsiveImage = new ResponsiveImage({
      media: promo.media,
    });
    return {
      ...promo,
      backgroundImage: <ResponsiveImageComponent {...responsiveImage} />,
    };
  };

  renderPromo = (promoStyles, type = 'main') => {
    const { promo, css } = this.props;
    if (!promo) return null;
    const { activeItem } = this.state;
    const promoTheme = type === 'sub' ? 'dark' : promo.theme || 'light';
    const className = type === 'sub' ? css.subPromo : css.mainPromo;
    const animationClass = type === 'sub' ? 'flyoutTitle' : 'flyoutMainPromo';
    if (type === 'sub' && this.noMarginMenus.includes(activeItem)) return null;
    return (
      <CSSTransition
        in={this.itemsIn}
        timeout={
          this.animationSpecs.mainDuration * 2 +
          this.animationSpecs.timeoutBuffer
        }
        classNames={animationClass}
        unmountOnExit
      >
        <div
          className={className}
          style={{ transitionDelay: `${this.animationSpecs.mainDuration}ms` }}
        >
          <FlexiblePromo
            {...this.preparePromo(promo)}
            theme={promoTheme}
            css={promoStyles}
          />
        </div>
      </CSSTransition>
    );
  };

  render() {
    const {
      navigationItems,
      className: classNameProp,
      css,
      ...rest
    } = this.props;
    const { activeItem } = this.state;

    const mainPromoClass = activeItem === '' ? css.justifySpaceBetween : null;

    const className = classNames(css.root, classNameProp, mainPromoClass);
    return (
      <CSSTransition
        in={this.itemsIn}
        timeout={
          this.animationSpecs.mainDuration + this.animationSpecs.timeoutBuffer
        }
        classNames="flyoutMain"
        unmountOnExit
      >
        <div className={className} {...rest}>
          <div className={css.mainMenu}>{this.renderMainItems()}</div>
          {this.renderSubMenu()}
          {activeItem === '' && this.renderPromo(mainPromoStyles)}
        </div>
      </CSSTransition>
    );
  }
}

export default FlyoutMenu;
