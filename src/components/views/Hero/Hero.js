import { Image, ScrollTo, themableWithStyles } from 'rsk-components';
import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withSizes from 'react-sizes';
import { Button, KeuneIcon, Text } from '../../elements';

import { HeroDefaultPropTypes } from '../../../types';
import { WithLink } from '../../behaviors';
import { capitalize } from '../../../utils/helperFunc';
import styles from './Hero.css';

@themableWithStyles(styles)
@withSizes(({ width }) => ({ isMobile: width < 1280 }))
class Hero extends PureComponent {
  // @TODO All these prop.types are all in `/types/index.js` and need to be included from there but currently won't show up in Styleguide (that's why they are here)
  static propTypes = {
    /**
     * The height of the component
     */
    isFullHeight: PropTypes.bool,
    /**
     * The subtitle or subtitle string
     */
    subtitle: PropTypes.string,
    /**
     * The title string
     */
    title: PropTypes.string,
    /**
     * The title string
     */
    text: PropTypes.string,
    /**
     * The button text string
     */
    buttonText: PropTypes.string,
    /**
     * The button text string
     */
    buttonUrl: PropTypes.string,
    /**
     * The theme of hero
     */
    theme: PropTypes.oneOf(['light', 'dark']),
    /**
     * The scroll button switch
     */
    hasScrollTo: PropTypes.bool,
    /**
     * The component for the backgroundImage slot
     */
    backgroundImage: PropTypes.node.isRequired,
    /**
     * The position of the background
     */
    backgroundPosition: PropTypes.oneOf(['left', 'center', 'right']),
    /**
     * The props for the optional logoImage slot
     */
    logoImage: PropTypes.shape({
      src: PropTypes.string.isRequired,
      alt: PropTypes.string,
      width: PropTypes.string,
      height: PropTypes.string,
    }),
    /**
     * The horizontal position of the content
     */
    justify: PropTypes.oneOf(['start', 'center', 'end']),
    /**
     * The vertical position of the content.
     */
    align: PropTypes.oneOf(['leftTop', 'leftMiddle', 'leftBottom']),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      bgWrapper: PropTypes.string,
      bg: PropTypes.string,
      bgLeft: PropTypes.string,
      bgCenter: PropTypes.string,
      bgRight: PropTypes.string,
      contentWrapper: PropTypes.string,
      content: PropTypes.string,
      justifyStart: PropTypes.string,
      justifyCenter: PropTypes.string,
      justifyEnd: PropTypes.string,
      alignTop: PropTypes.string,
      alignCenter: PropTypes.string,
      alignBottom: PropTypes.string,
      isFullHeight: PropTypes.string,
      bgOverlay: PropTypes.string,
      themeDark: PropTypes.string,
      themeLight: PropTypes.string,
      logoImage: PropTypes.string,
      scrollTo: PropTypes.string,
      textBlock: PropTypes.string,
      subtitle: PropTypes.string,
      title: PropTypes.string,
      text: PropTypes.string,
    }),
  };

  static defaultProps = {
    ...HeroDefaultPropTypes,
    isFullHeight: true,
    hasScrollTo: true,
    css: {
      root: styles.root,
      bgWrapper: styles.bgWrapper,
      bg: styles.bg,
      bgLeft: styles.bgleft,
      bgCenter: styles.bgcenter,
      bgRight: styles.bgright,
      contentWrapper: styles.contentWrapper,
      content: styles.content,
      justifyStart: styles.justifyStart,
      justifyCenter: styles.justifyCenter,
      justifyEnd: styles.justifyEnd,
      leftTop: styles.leftTop,
      leftCenter: styles.leftMiddle,
      leftBottom: styles.leftBottom,
      isFullHeight: styles.isFullHeight, // It is for KEUN-73
      bgOverlay: styles.bgOverlay,
      themeDark: styles.themeDark,
      themeLight: styles.themeLight,
      logoImage: styles.logoImage,
      scrollTo: styles.scrollTo,
      textBlock: styles.textBlock,
      subtitle: styles.subtitle,
      title: styles.title,
      text: styles.text,
      button: null,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      composition,
      css,
      backgroundImage,
      backgroundPosition,
      justify,
      align,
      subtitle,
      title,
      text,
      buttonText,
      buttonUrl,
      isFullHeight,
      theme,
      logoImage,
      hasScrollTo,
      overrideCss,
      isMobile,
      // The rest values
      ...rest
    } = this.props;

    const overridenCss = { ...css, ...overrideCss };

    Object.keys(css).forEach(k => {
      if (css[k] === overridenCss[k]) {
        overridenCss[k] = '';
      }
    });

    const className = classNames(css.root, classNameProp, overridenCss.root);

    return (
      <WithLink to={buttonUrl}>
        <section className={className} {...rest}>
          {logoImage && (
            <Image
              className={classNames(
                css.logoImage,
                css.alignCenter,
                overridenCss.logoImage,
                overridenCss.alignCenter,
              )}
              {...logoImage}
            />
          )}
          <div
            className={classNames(
              css.contentWrapper,
              { [styles.isFullHeight]: isFullHeight },
              css[`justify${capitalize(justify)}`],
              css[align],
              overridenCss.contentWrapper,
              overridenCss[`justify${capitalize(justify)}`],
              overridenCss[align],
            )}
          >
            <div
              className={classNames(
                css.content,
                css[`theme${capitalize(theme)}`],
                overridenCss.content,
                overridenCss[`theme${capitalize(theme)}`],
              )}
            >
              <Text
                variant="headline"
                className={classNames(css.subtitle, overridenCss.subtitle)}
              >
                {subtitle}
              </Text>
              <Text
                variant="display1"
                className={classNames(css.title, overridenCss.title)}
              >
                {title}
              </Text>
              <div className={css.textBlock}>
                <Text
                  variant="body1"
                  className={classNames(css.text, overridenCss.text)}
                  gutterBottom
                >
                  {text}
                </Text>
              </div>
              {buttonText && (
                <Button
                  theme={theme === 'light' ? 'primary' : 'secondary'}
                  iconAfter="arrow"
                  className={classNames(css.button, overridenCss.button)}
                  ref={this.buttonRef}
                >
                  {buttonText}
                </Button>
              )}
            </div>
          </div>
          <div className={classNames(css.bgWrapper, overridenCss.bgWrapper)}>
            {backgroundImage && (
              <backgroundImage.type
                {...backgroundImage.props}
                fixedAspectratio
                className={classNames(
                  css.bg,
                  css[`bg${backgroundPosition}`],
                  overridenCss.bg,
                  overridenCss[`bg${backgroundPosition}`],
                  backgroundImage.props.className,
                )}
              />
            )}
          </div>
          <div className={classNames(css.bgOverlay, overridenCss.bgOverlay)} />
          {hasScrollTo && (
            <Fragment>
              <a href="#scrolldown" className={css.scrollTo}>
                <KeuneIcon name="chevron" title="Scroll down" />
              </a>
              <ScrollTo
                className="test"
                offset={isMobile ? -62 : -171}
                anchor="scrolldown"
              />
            </Fragment>
          )}
        </section>
      </WithLink>
    );
  }
}

export default Hero;
