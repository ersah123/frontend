Hero component

Other components `<Promo />` and `<PromoDouble />` are based on this one
and share the same properties.

```js
const backgroundImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const logoImageExample = {
  src: 'https://1922.keune.com/images/1922_by_jmkeune.svg',
  alt: 'Keune Logo',
  width: '250',
  height: '75',
};

const props = {
  justify: 'start',
  align: 'leftBottom',
  isFullHeight: true,
  subtitle: 'Our new Ultimate Blonde line',
  title: 'Go Brighter',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'LOREM IPSUM GALOR',
  buttonUrl: '/go',
  theme: 'light',
  backgroundImage: <ResponsiveImage {...backgroundImage} />,
  logoImage: logoImageExample,
};

<Hero {...props} />;
```
