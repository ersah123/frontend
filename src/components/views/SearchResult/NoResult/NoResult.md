Basic NoResult component

```js
const backgroundImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const props = {
  title: 'Oops!',
  text: 'We couldn’t find anything for ‘%s’.',
  subtitle: 'Please try another search term.',
  placeholder: 'I’m looking for...',
  searchTerm: 'searchTerm',
  backgroundImage: <ResponsiveImage {...backgroundImage} />,
};

<NoResult {...props} />;
```
