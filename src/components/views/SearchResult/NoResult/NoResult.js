import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ScrollTo, themableWithStyles } from 'rsk-components';
import { SearchInput } from '../..';
import { KeuneIcon, Text } from '../../../elements';

import styles from './NoResult.css';

@themableWithStyles(styles)
class NoResult extends PureComponent {
  static propTypes = {
    /**
     * Prop for title
     */
    title: PropTypes.string,
    /**
     * Prop for text
     */
    text: PropTypes.string,
    /**
     * Prop for subtitle
     */
    subtitle: PropTypes.string,
    /**
     * Prop for placeholder
     */
    placeholder: PropTypes.string,
    /**
     * Prop for searchTerm
     */
    searchTerm: PropTypes.string,
    /**
     * Prop for searchTerm
     */
    backgroundImage: PropTypes.node,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      searchInput: PropTypes.string,
      contentWrapper: PropTypes.string,
      content: PropTypes.string,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      bgWrapper: PropTypes.string,
      bg: PropTypes.string,
      scrollTo: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: null,
    text: null,
    subtitle: null,
    placeholder: null,
    searchTerm: null,
    className: null,
    children: null,
    backgroundImage: null,
    css: {
      root: styles.root,
      searchInput: styles.searchInput,
      contentWrapper: styles.contentWrapper,
      content: styles.content,
      title: styles.title,
      subtitle: styles.subtitle,
      bgWrapper: styles.bgWrapper,
      bg: styles.bg,
      scrollTo: styles.scrollTo,
    },
  };

  render() {
    const {
      title,
      text,
      subtitle,
      placeholder,
      searchTerm,
      backgroundImage,
      className: classNameProp,
      css,
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <Fragment>
        <div className={className}>
          <div className={css.contentWrapper}>
            <div className={css.content}>
              <Text variant="display1" className={css.title}>
                {title}
              </Text>
              <Text variant="display2" className={css.subtitle}>
                {text.replace(/%s/g, searchTerm)}
              </Text>
              <Text variant="display2" className={css.subtitle} gutterBottom>
                {subtitle}
              </Text>
              <SearchInput
                placeholder={placeholder}
                className={css.searchInput}
              />
              <a href="#scrolldown" className={css.scrollTo}>
                <KeuneIcon name="chevron" title="Scroll down" />
              </a>
            </div>
          </div>
          {backgroundImage && (
            <div className={css.bgWrapper}>
              <backgroundImage.type
                {...backgroundImage.props}
                fixedAspectratio
                className={css.bg}
              />
            </div>
          )}
        </div>
        <ScrollTo anchor="scrolldown" />
      </Fragment>
    );
  }
}

export default NoResult;
