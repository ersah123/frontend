import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  ResponsiveImage as ResponsiveImageComponent,
  themableWithStyles,
} from 'rsk-components';

import { Tile } from '../../collections';
import { Text } from '../../elements';
import Product from '../../../models/Product';
import NoResult from './NoResult/NoResult';
import ResponsiveImage from '../../../models/ResponsiveImage';

import styles from './SearchResult.css';

const mapStateToProps = ({ search }) => ({
  results: search.results,
  searchTerm: search.searchTerm,
});

@connect(
  mapStateToProps,
  null,
)
@themableWithStyles(styles)
class SearchResult extends PureComponent {
  static propTypes = {
    results: PropTypes.arrayOf(PropTypes.object).isRequired,
    searchTerm: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      content: PropTypes.string,
    }),
  };

  static defaultProps = {
    searchTerm: null,
    css: {
      root: styles.root,
      content: styles.content,
    },
  };

  fillSlots = () => {
    const { results } = this.props;
    const actualGridLength = results.length;
    const lastRowItemCount = actualGridLength % 4;
    const fakeItemCount = 4 - lastRowItemCount;
    return new Array(fakeItemCount).fill().map((_, i) => {
      const key = `fake${i}`;
      return <div key={key} className={styles.fakeItem} />;
    });
  };

  renderProducts = products =>
    products.map((product, i) => {
      const finalProduct = { ...product, media: product.media[0] };
      const key = `prod-${i}-${product.id}`;
      return <Tile key={key} product={finalProduct} />;
    });

  renderNoResults = () => {
    const { media, searchTerm } = this.props;
    const responsiveImage = media ? new ResponsiveImage(media) : null;
    const backgroundImage = responsiveImage ? (
      <ResponsiveImageComponent {...responsiveImage} />
    ) : null;
    const props = {
      title: 'Oops!',
      text: 'We couldn’t find anything for ‘%s’.',
      subtitle: 'Please try another search term.',
      placeholder: 'I’m looking for...',
      searchTerm,
      backgroundImage,
    };
    return <NoResult {...props} />;
  };

  render() {
    const { title, results, searchTerm, css } = this.props;

    if (!results.length) return this.renderNoResults();

    const products = results.map(product => new Product(product));

    return (
      <div className={css.root}>
        <Text variant="display1" gutterBottom>
          {title
            .replace('$count', results.length)
            .replace('$search', searchTerm)}
        </Text>
        <div className={css.content}>
          {this.renderProducts(products)}
          {this.fillSlots()}
        </div>
      </div>
    );
  }
}

export default SearchResult;
