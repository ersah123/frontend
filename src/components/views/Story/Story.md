Story component

```js
const responsiveImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1166,
      height: 503,
    },
  },
};

const props = {
  title: 'Lorem ipsum dolor sit amet.',
  subtitle: 'Lorem ipsum dolor sit amet.',
  text:
    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si id dicis, vicimus. Nos vero, inquit ille; Ubi ut eam caperet aut quando</p>',
  responsiveImage: <ResponsiveImage {...responsiveImage} />,
};

<Story {...props} />;
```
