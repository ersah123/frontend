import React, { PureComponent } from 'react';
import { RichText, Text } from 'components/elements';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './Story.css';

@themableWithStyles(styles)
class Story extends PureComponent {
  static propTypes = {
    /**
     * The title string
     */
    title: PropTypes.string,
    /*
     * The subtitle string
     */
    subtitle: PropTypes.string,
    /**
     * The text string
     */
    text: PropTypes.string,
    /**
     * The component for the image slot
     */
    responsiveImage: PropTypes.shape(),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      mediaWrapper: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    title: null,
    subtitle: null,
    text: null,
    responsiveImage: null,
    css: {
      root: styles.root,
      mediaWrapper: styles.mediaWrapper,
    },
  };

  render() {
    const {
      className: classNameProp,
      css,
      title,
      subtitle,
      text,
      responsiveImage,
    } = this.props;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <div className={styles.content}>
          {subtitle && (
            <Text variant="headline" gutterBottom>
              {subtitle}
            </Text>
          )}
          {title && (
            <Text variant="display1" gutterBottom>
              {title}
            </Text>
          )}
          {text && <RichText className={styles.paragraph} html={text} />}
        </div>
        {responsiveImage && (
          <div className={css.mediaWrapper}>
            <responsiveImage.type {...responsiveImage.props} />
          </div>
        )}
      </div>
    );
  }
}

export default Story;
