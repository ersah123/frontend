import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Cookies from 'universal-cookie';
import { Modal, themableWithStyles } from 'rsk-components';
import { Button, Checkbox, Link, Text } from '../../elements';
import { prepareCookies, selectCookies } from '../../../utils/helperFunc';

import styles from './CookiePopup.css';

const mapStateToProps = ({ runtime }) => ({ runtime });

@connect(mapStateToProps)
@reduxForm({
  form: 'cookiePopup',
})
@themableWithStyles(styles)
class CookiePopup extends PureComponent {
  cookies = new Cookies();

  fieldNamePrefix = 'cookiePopup';

  state = { cookiesAgreed: false };

  static propTypes = {
    /**
     * Prop for runtime
     */
    runtime: PropTypes.shape({}),
    /**
     * Placeholder for submit function
     */
    handleSubmit: PropTypes.func.isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      popupContent: PropTypes.string,
      button: PropTypes.string,
      text: PropTypes.string,
      checkbox: PropTypes.string,
    }),
  };

  static defaultProps = {
    runtime: {},
    className: null,
    css: {
      root: styles.root,
      popupContent: styles.popupContent,
      button: styles.button,
      text: styles.text,
      checkbox: styles.checkbox,
    },
  };

  submitForm = form => {
    const selectedCookies = selectCookies(form[this.fieldNamePrefix]);
    this.cookies.set('cookiesAgreed', selectedCookies, { path: '/' });
    this.setState({ cookiesAgreed: true });
    return false;
  };

  renderCookies = () => {
    const { runtime, css } = this.props;
    let cookies = null;
    if (runtime.siteInit.cookies) {
      cookies = runtime.siteInit.cookies;
    }
    return prepareCookies(cookies).map(cookie => (
      <Checkbox
        className={css.checkbox}
        key={`${this.fieldNamePrefix}[${cookie.title}]`}
        name={`${this.fieldNamePrefix}[${cookie.title}]`}
        label={`${cookie.title} (${cookie.details.length})`}
        disabled={cookie.checked}
        checked={cookie.disabled}
      />
    ));
  };

  render() {
    const { runtime, handleSubmit, className: classNameProp, css } = this.props;

    if (!runtime) return null;

    let cookies = null;
    if (runtime.siteInit.cookies) {
      cookies = runtime.siteInit.cookies;
    }

    const { title, text, linkUrl, linkText, buttonText } = cookies;

    const buttonUrl = '/';

    const { cookiesAgreed } = this.state;

    if (cookiesAgreed || this.cookies.get('cookiesAgreed')) return null;

    const className = classNames(css.root, classNameProp);
    return (
      <Modal onClose={this.handleClose} className={className}>
        <div className={css.popupContent}>
          <Text variant="display3" gutterBottom>
            {title}
          </Text>
          <Text variant="subheading" className={css.text}>
            {text}
            <Link to={linkUrl}>{linkText}</Link>
          </Text>
          <form onSubmit={handleSubmit(this.submitForm)}>
            <div className={css.cookies}>{this.renderCookies()}</div>
            <Button
              theme="tertiary"
              to={buttonUrl}
              className={css.button}
              onClick={handleSubmit(this.submitForm)}
            >
              {buttonText}
            </Button>
          </form>
        </div>
      </Modal>
    );
  }
}

export default CookiePopup;
