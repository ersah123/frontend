import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './Loading.css';

@themableWithStyles(styles)
class Loading extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * loading flag
     */
    isLoading: PropTypes.bool,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      loadingIcon: PropTypes.string,
      loadingOverlay: PropTypes.string,
      ball: PropTypes.string,
      ball1: PropTypes.string,
      ball2: PropTypes.string,
      ball3: PropTypes.string,
      hideLoading: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    isLoading: false,
    css: {
      root: styles.root,
      loadingIcon: styles.loadingIcon,
      loadingOverlay: styles.loadingOverlay,
      ball: styles.ball,
      ball1: styles.ball1,
      ball2: styles.ball2,
      ball3: styles.ball3,
      hideLoading: styles.hideLoading,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      isLoading,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      classNameProp,
      !isLoading ? css.hideLoading : '',
    );

    const ball1Class = classNames(css.ball, css.ball1);
    const ball2Class = classNames(css.ball, css.ball2);
    const ball3Class = classNames(css.ball, css.ball3);
    const ball4Class = classNames(css.ball);

    return (
      <div className={className} {...rest}>
        {children}
        <div className={css.loadingOverlay} />
        <div className={css.loadingIcon}>
          <div className={ball1Class} />
          <div className={ball2Class} />
          <div className={ball3Class} />
          <div className={ball4Class} />
        </div>
      </div>
    );
  }
}

export default Loading;
