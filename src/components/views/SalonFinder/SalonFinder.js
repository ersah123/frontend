import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { themableWithStyles } from 'rsk-components';
import * as salonAction from '../../../actions/salon.action';

import { KeuneIcon, Text } from '../../elements';

import AddressCard from './AddressCard';
import { Loading } from '..';
import Map from '../../modules/Map';
import SalonFinderSearch from './SalonFinderSearch/SalonFinderSearch';
import styles from './SalonFinder.css';

const mapStateToProps = ({ salon }) => ({
  salons: salon.salons,
  loading: salon.loading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(salonAction, dispatch);

@connect(
  mapStateToProps,
  mapDispatchToProps,
)
@themableWithStyles(styles)
class SalonFinder extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * Gets products from state (or loads them).
     */
    getPlaces: PropTypes.func.isRequired,
    /**
     * salons array from redux store
     */
    salons: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * salon pending action from redux store
     */
    loading: PropTypes.bool,
    /**
     * is it in Modal
     */
    isModal: PropTypes.bool,
    /**
     * handle close button
     */
    handleClose: PropTypes.func,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      question: PropTypes.string,
      search: PropTypes.string,
      result: PropTypes.string,
      mapWrapper: PropTypes.string,
      resultList: PropTypes.string,
      resultBlock: PropTypes.string,
      closeBtn: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    loading: false,
    isModal: false,
    handleClose: () => {},
    css: {
      root: styles.root,
      question: styles.question,
      search: styles.search,
      result: styles.result,
      mapWrapper: styles.mapWrapper,
      resultList: styles.resultList,
      resultBlock: styles.resultBlock,
      closeBtn: styles.closeBtn,
    },
  };

  state = {
    isSearched: false,
    selectedPlaceId: null,
    mapCenter: {
      lat: null,
      lng: null,
    },
    searchedOrigin: {
      lat: null,
      lng: null,
    },
  };

  componentWillReceiveProps(nextProps) {
    const { salons, loading } = nextProps;
    if (salons.length && !loading) {
      this.setState({
        isSearched: true,
      });
    }
  }

  handleSearch = ({ lat, lng }, country) => {
    const { getPlaces } = this.props;
    getPlaces(lat, lng, country);
    this.setState({
      mapCenter: {
        lat,
        lng,
      },
      searchedOrigin: {
        lat,
        lng,
      },
    });
  };

  handlePlaceClick = (place, { selectedPlaceId } = this.state) => {
    if (selectedPlaceId === place.id) {
      this.setState({
        selectedPlaceId: null,
        mapCenter: {
          lat: place.latitude,
          lng: place.longitude,
        },
      });
      return;
    }
    this.setState({
      selectedPlaceId: place.id,
      mapCenter: {
        lat: place.latitude,
        lng: place.longitude,
      },
    });
  };

  renderResults = (
    mapCenter,
    searchedOrigin,
    selectedPlaceId,
    { css, salons, loading } = this.props,
  ) => (
    <Loading isLoading={loading}>
      <div className={css.result}>
        <div className={css.resultBlock}>
          <Text variant="body1" gutterBottom>
            {`Showing ${salons.length} salons near you`}
          </Text>
          <div className={css.resultList}>
            {salons.map(salon => (
              <AddressCard
                key={salon.id}
                salon={salon}
                isExpanded={selectedPlaceId === salon.id}
                handlePlaceClick={this.handlePlaceClick}
              />
            ))}
          </div>
        </div>
        <div className={css.mapWrapper}>
          <Map
            salons={salons}
            handleMarkerClick={this.handlePlaceClick}
            selectedPlaceId={selectedPlaceId}
            mapCenter={mapCenter}
            searchedOrigin={searchedOrigin}
            loading={loading}
          />
        </div>
      </div>
    </Loading>
  );

  render() {
    const {
      className: classNameProp,
      children,
      css,
      getPlaces,
      salons,
      loading,
      isModal,
      handleClose,
      // The rest values
      ...rest
    } = this.props;

    const {
      isSearched,
      selectedPlaceId,
      mapCenter,
      searchedOrigin,
    } = this.state;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className} {...rest}>
        {isModal && (
          <div
            className={css.closeBtn}
            onClick={handleClose}
            onKeyUp={e => (e.keyCode === 27 ? handleClose : null)}
            role="button"
            tabIndex="-1"
          >
            <Text variant="body1">Close</Text>
            <KeuneIcon name="close" />
          </div>
        )}
        <div className={css.search}>
          {/* <div className={css.question}>
            <Text variant="body2" headlineMapping={{ body2: 'h4' }} gutterBottom>Hi, did you find what you were looking for?</Text>
          </div>
          <ul className={css.selection}>
            <li><Button iconAfter="location">YES, TAKE ME TO A KEUNE SALON</Button></li>
            <li><Button>I WANT TO BECOME A CLIENT</Button></li>
            <li><Button>I WANT TO JOIN THE FAMILY</Button></li>
          </ul> */}
          <div className={css.question}>
            <Text
              variant="body2"
              headlineMapping={{ body2: 'h4' }}
              gutterBottom
            >
              Looking for a Keune Salon?
            </Text>
          </div>
          <SalonFinderSearch handleSearch={this.handleSearch} />
        </div>
        {!isSearched && loading && <Loading isLoading={loading} />}
        {isSearched &&
          this.renderResults(mapCenter, searchedOrigin, selectedPlaceId)}
      </div>
    );
  }
}

export default SalonFinder;
