import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './SalonFinderSearch.css';
import { Loading } from '../..';
import { KeuneIcon, Text } from '../../../elements';

@themableWithStyles(styles)
class SalonFinderSearch extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * handle search
     */
    handleSearch: PropTypes.func.isRequired,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      autocompleteDropdownContainer: PropTypes.string,
      suggestionItem: PropTypes.string,
      suggestionItemActive: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
      autocompleteDropdownContainer: styles.autocompleteDropdownContainer,
      suggestionItem: styles.suggestionItem,
      suggestionItemActive: styles.suggestionItemActive,
    },
  };

  activeItem = '';

  state = { address: '', country: 'nl' };

  handleChange = address => {
    this.setState({ address });
  };

  handleSearchIconClick = () => {
    this.handleSelect(this.activeItem.description);
  };

  handleSelect = address => {
    this.setState({ address });
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        const { handleSearch } = this.props;
        const { country } = this.state;
        handleSearch(latLng, country);
      })
      .catch(error => console.error('Error', error));
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      handleSearch,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    const { address, country } = this.state;

    return (
      <PlacesAutocomplete
        value={address}
        highlightFirstSuggestion
        onChange={this.handleChange}
        onSelect={this.handleSelect}
        searchOptions={{
          componentRestrictions: {
            country,
          },
        }}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div className={className} {...rest}>
            <input
              {...getInputProps({
                placeholder: 'Not yet, I’m looking for…',
                className: 'location-search-input',
              })}
              autoComplete="off"
            />
            <KeuneIcon
              name="search"
              onClick={() => this.handleSearchIconClick(address)}
            />
            <div className={css.autocompleteDropdownContainer}>
              {loading && <Loading isLoading={loading} />}
              {suggestions.map(suggestion => {
                const suggestClassName = classNames(
                  css.suggestionItem,
                  suggestion.active ? css.suggestionItemActive : null,
                );

                if (suggestion.active) {
                  this.activeItem = suggestion;
                }

                return (
                  <div
                    {...getSuggestionItemProps(suggestion)}
                    className={suggestClassName}
                  >
                    <Text variant="body1" gutterBottom>
                      {suggestion.description}
                    </Text>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default SalonFinderSearch;
