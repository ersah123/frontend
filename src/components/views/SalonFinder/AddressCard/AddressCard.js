import React, { PureComponent } from 'react';

import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { goTo } from '../../../../utils/helperFunc';
import styles from './AddressCard.css';
import { Button, KeuneIcon, Text } from '../../../elements';

@themableWithStyles(styles)
class AddressCard extends PureComponent {
  static propTypes = {
    /**
     * Address card
     */
    isExpanded: PropTypes.bool,
    /**
     * address card click handler
     */
    handlePlaceClick: PropTypes.func.isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    salon: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      addressLine: PropTypes.string,
      zipCode: PropTypes.string,
      city: PropTypes.string,
      country: PropTypes.string,
      phoneNumber: PropTypes.string,
      website: PropTypes.string,
      latitude: PropTypes.number,
      longitude: PropTypes.number,
      loyalty: PropTypes.bool,
      rangeCare: PropTypes.bool,
      rangeStyle: PropTypes.bool,
      rangeSoPure: PropTypes.bool,
      range1922: PropTypes.bool,
      rangeBlend: PropTypes.bool,
      rangeColor: PropTypes.bool,
      rangeKeuneYou: PropTypes.bool,
    }).isRequired,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      title: PropTypes.string,
      closeBlock: PropTypes.string,
      contentBlock: PropTypes.string,
      textWithIcon: PropTypes.string,
      distanceNumber: PropTypes.string,
      addressLine: PropTypes.string,
      addressBlock: PropTypes.string,
      infoButtons: PropTypes.string,
      distance: PropTypes.string,
      brands: PropTypes.string,
      expanded: PropTypes.string,
      expandedBlock: PropTypes.string,
      more: PropTypes.string,
      phoneNumber: PropTypes.string,
      city: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    isExpanded: false,
    css: {
      root: styles.root,
      title: styles.title,
      closeBlock: styles.closeBlock,
      contentBlock: styles.contentBlock,
      textWithIcon: styles.textWithIcon,
      distanceNumber: styles.distanceNumber,
      addressLine: styles.addressLine,
      addressBlock: styles.addressBlock,
      infoButtons: styles.infoButtons,
      distance: styles.distance,
      brands: styles.brands,
      expanded: styles.expanded,
      expandedBlock: styles.expandedBlock,
      more: styles.more,
      phoneNumber: styles.phoneNumber,
      city: styles.city,
    },
  };

  renderBrands = ranges =>
    ranges.map((range, i) => {
      if (ranges.length !== i + 1) {
        // not last item
        return `${range}, `;
      }
      return range;
    });

  handleDirectionsClick = url => {
    goTo(
      `https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(
        url,
      )}`,
    );
  };

  renderInfo(salon, { css } = this.props) {
    return (
      <div className={css.more}>
        <div className={css.addressBlock}>
          <Text variant="body1">
            <span className={css.addressLine}>{salon.addressLine}</span>
            {salon.zipCode} {salon.city}
          </Text>
        </div>
        <div className={css.infoButtons}>
          {salon.website && (
            <Button
              to={salon.website}
              onClick={() => goTo(salon.website)}
              theme="secondary"
            >
              WEBSITE
            </Button>
          )}
          <Button
            onClick={() => this.handleDirectionsClick(salon.addressLine)}
            theme="secondary"
          >
            DIRECTIONS
          </Button>
        </div>
        {salon.ranges.length ? (
          <div className={css.brands}>
            <Text variant="body1">
              <span>Has the following brands:</span>
              {this.renderBrands(salon.ranges)}
            </Text>
          </div>
        ) : null}
      </div>
    );
  }

  render() {
    const {
      className: classNameProp,
      children,
      css,
      salon,
      isExpanded,
      handlePlaceClick,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      classNameProp,
      isExpanded ? css.expandedBlock : '',
    );
    const closeBlockClass = classNames(
      css.closeBlock,
      isExpanded ? css.expanded : '',
    );

    return (
      <div
        className={className}
        {...rest}
        onClick={() => handlePlaceClick(salon)}
        role="presentation"
        onKeyDown={() => {}}
      >
        <div className={css.contentBlock}>
          {salon.loyalty && (
            <div className={css.textWithIcon}>
              <KeuneIcon name="keune" />
              <Text variant="body1">Keune Ambassador Salon</Text>
            </div>
          )}

          <div className={css.title}>
            <Text variant="subheading">{salon.name}</Text>
          </div>
          <div className={css.textWithIcon}>
            {salon.phoneNumber && (
              <div className={css.phoneNumber}>
                <KeuneIcon name="phone" />
                <Text variant="body1">{salon.phoneNumber}</Text>
              </div>
            )}
            <span className={css.city}>
              <Text variant="body1">{salon.city}</Text>
            </span>
          </div>
          <CSSTransition
            timeout={500}
            classNames="addressCardMore"
            in={isExpanded}
          >
            {this.renderInfo(salon)}
          </CSSTransition>
          <div className={css.distance}>
            <div className={css.distanceNumber}>
              <Text variant="title">{salon.distance}</Text>
            </div>
            <Text variant="body1">{salon.distanceUnit}</Text>
          </div>
        </div>
        <div className={closeBlockClass}>
          <KeuneIcon name="close" />
        </div>
      </div>
    );
  }
}

export default AddressCard;
