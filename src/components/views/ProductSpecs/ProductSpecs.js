import React, { PureComponent } from 'react';
import { StickyContainer, Sticky } from 'react-sticky';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ButtonBase, Image, themableWithStyles } from 'rsk-components';
import { Icon, Button, FormattedCurrency, Text } from '../../elements';

import styles from './ProductSpecs.css';

@themableWithStyles(styles)
class ProductSpecs extends PureComponent {
  static propTypes = {
    /**
     * Function that will be run when the modal is requested to be closed.
     */
    onClose: PropTypes.func.isRequired,
    /**
     * Initial open status
     */
    isOpen: PropTypes.bool.isRequired,
    /**
     * Specifications title, probably 'Specifications'.
     */
    title: PropTypes.string,
    /**
     * The specifications in name / value pairs.
     */
    specs: PropTypes.arrayOf(PropTypes.shape()),
    /**
     * Ingredients title, probably 'Ingredients'.
     */
    titleIngredients: PropTypes.string,
    /**
     * Ingredients text.
     */
    textIngredients: PropTypes.string,
    /**
     * Product icons.
     */
    icons: PropTypes.arrayOf(PropTypes.string),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      row: PropTypes.string,
      specName: PropTypes.string,
      specValue: PropTypes.string,
      icon: PropTypes.string,
      productHeader: PropTypes.string,
      headerImage: PropTypes.string,
      productImage: PropTypes.string,
      headerText: PropTypes.string,
      productTitle: PropTypes.string,
      price: PropTypes.string,
      headerButton: PropTypes.string,
      button: PropTypes.string,
      content: PropTypes.string,
      icons: PropTypes.string,
      title: PropTypes.string,
      bottomContent: PropTypes.string,
      subText: PropTypes.string,
      productSpecs: PropTypes.string,
      closeButton: PropTypes.string,
      buttonBottom: PropTypes.string,
      show: PropTypes.string,
      stickyClosing: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: null,
    specs: [],
    titleIngredients: null,
    textIngredients: null,
    icons: [],
    className: null,
    children: null,
    css: {
      root: styles.root,
      row: styles.row,
      specName: styles.specName,
      specValue: styles.specValue,
      icon: styles.icon,
      productHeader: styles.productHeader,
      headerImage: styles.headerImage,
      productImage: styles.productImage,
      headerText: styles.headerText,
      productTitle: styles.productTitle,
      price: styles.price,
      headerButton: styles.headerButton,
      button: styles.button,
      content: styles.content,
      icons: styles.icons,
      title: styles.title,
      bottomContent: styles.bottomContent,
      subText: styles.subText,
      productSpecs: styles.productSpecs,
      closeButton: styles.closeButton,
      buttonBottom: styles.buttonBottom,
      show: styles.show,
      stickyClosing: styles.stickyClosing,
    },
  };

  constructor(props) {
    super(props);
    this.myRef = React.createRef(); // Create a ref object
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      this.myRef.current.scrollTo(0, 0);
      window.setTimeout(() => {
        window.dispatchEvent(new Event('scroll'));
      }, 0);
    });
  }

  componentDidUpdate() {
    this.myRef.current.scrollTo(0, 0);
    window.setTimeout(() => {
      window.dispatchEvent(new Event('scroll'));
    }, 360);
  }

  renderSpecs = () => {
    const { specs, css } = this.props;
    return specs.map(spec => (
      <div className={css.row} key={spec.name}>
        <div className={css.specName}>
          <Text variant="subheading">{spec.name}</Text>
        </div>
        <div className={css.specValue}>
          <Text variant="body1">{spec.value}</Text>
        </div>
      </div>
    ));
  };

  renderIcons = () => {
    const { icons, css } = this.props;
    return icons.map(icon => (
      <Image src={icon} key={icon} className={css.icon} />
    ));
  };

  renderHeader = () => {
    const { productInfo, isOpen, onClose, css } = this.props;
    const { image, title, price, buttonText, buttonTo } = productInfo;
    const stickyClass = isOpen ? null : css.stickyClosing;

    return (
      <Sticky>
        {({ style }) => (
          <div style={style} className={stickyClass}>
            <div className={css.productHeader}>
              <div className={css.headerImage}>
                <Image className={css.productImage} {...image} />
              </div>
              <div className={css.headerText}>
                <Text className={css.productTitle} variant="body2" align="left">
                  {title}
                </Text>
                <p className={css.price}>
                  <FormattedCurrency currency="EUR" value={price} />
                </p>
              </div>
              <div className={css.headerButton}>
                {this.renderButton(buttonText, buttonTo, css.button, true)}
                <ButtonBase onClick={onClose} className={css.closeButton}>
                  <Text variant="body1">Close</Text>
                  <Icon name="close" />
                </ButtonBase>
              </div>
            </div>
            {this.renderButton(buttonText, buttonTo, css.buttonBottom, true)}
          </div>
        )}
      </Sticky>
    );
  };

  renderButton = (buttonText, buttonTo, buttonClass) => (
    <div className={buttonClass}>
      <Button theme="tertiary" wide to={buttonTo}>
        {buttonText}
      </Button>
    </div>
  );

  render() {
    const {
      isOpen,
      title,
      titleIngredients,
      textIngredients,
      icons,
      className: classNameProp,
      css,
    } = this.props;

    const isVisible = isOpen ? css.show : null;

    const className = classNames(css.root, classNameProp, isVisible);
    return (
      <div className={className} ref={this.myRef}>
        <StickyContainer>{this.renderHeader()}</StickyContainer>
        <div className={css.productSpecs}>
          <div className={css.content}>
            {icons &&
              icons.length && (
                <div className={css.icons}>{this.renderIcons()}</div>
              )}
            <Text variant="display3" className={css.title} gutterBottom>
              {title}
            </Text>
            {this.renderSpecs()}
            <div className={css.bottomContent}>
              <Text variant="display3" className={css.subText} gutterBottom>
                {titleIngredients}
              </Text>
              <Text variant="body1" gutterBottom>
                {textIngredients}
              </Text>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductSpecs;
