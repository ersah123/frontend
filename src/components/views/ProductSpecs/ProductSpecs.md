Basic ProductSpecs component

```js
const icons = [
  require('./assets/icon01.png'),
  require('./assets/icon02.png'),
  require('./assets/icon03.png'),
  require('./assets/icon04.png'),
];

const image = {
  src: require('../../collections/ProductHero/assets/thumb01.png'),
  alt: 'Product Image',
};

const productInfo = {
  id: 1,
  title: 'Care Silver Saviour Shampoo very long title',
  price: 12.34,
  image,
  buttonText: 'Add To Bag',
  buttonTo: '/go',
};

const props = {
  title: 'Full Specifications',
  titleIngredients: 'Ingredients',
  textIngredients:
    'Aqua (Water), Paraf num Liquidum (Mineral Oil), Alcohol Denat., Ceteareth-30, Laureth-2, Ceteth-2, Sorbitol, Cera Alba (Beeswax), Paraf n, Citric Acid, Parfum (Fragrance), Phenoxyethanol, PEG-90M, Sodium Hydroxide, Diazolidinyl Urea, Magnesium Sulfate, BHT, Panthenol, Ethylhexylglycerin, PEG-35 Castor Oil, Aesculus Hippocastanum (Horse Chestnut Seed) Extract, Polysorbate 20, Tocopherol, Calcium Pantothenate, Inositol, Retinyl Palmitate, Linoleic Acid, Biotin, Alpha-Isomethyl Ionone, Benzyl Benzoate, Benzyl Salicylate, Butylphenyl Methylpropional, Citronellol, Coumarin, Geraniol, Limonene, Linalool.',
  icons,
  productInfo,
  specs: [
    { name: 'Color Collection', value: 'Yes' },
    { name: 'Levels of lift (up to)', value: 'Yes' },
    { name: 'Warm', value: 'Yes' },
    { name: 'Cool', value: 'Yes' },
    { name: 'Glaze', value: 'Yes' },
    { name: 'Toner', value: 'Yes' },
    { name: 'Character', value: 'Yes' },
    { name: 'With anti-damage complex', value: 'Yes' },
    { name: 'Ammonia free', value: 'Yes' },
    { name: 'Vegan', value: 'Yes' },
    { name: 'Concentrated formula', value: 'Yes' },
    { name: 'Dermatologically tested and approved', value: 'Yes' },
    { name: 'Contains by-products from food industry (betaine)', value: 'Yes' },
    { name: 'No artificial dyes', value: 'Yes' },
    { name: 'No silicons', value: 'Yes' },
    { name: 'High in natural origin', value: 'Yes' },
    { name: 'Cruelty Free', value: 'Yes' },
    { name: 'No SLES/SLS', value: 'Yes' },
    { name: 'No gluten', value: 'Yes' },
    { name: 'Sustainable palm oil', value: 'Yes' },
    { name: 'Color Safe', value: 'Yes' },
    { name: 'UV-filter', value: 'Yes' },
    { name: 'Paraben', value: 'Yes' },
    { name: 'Essential minerals', value: 'Yes' },
    { name: 'Leave-in', value: 'Yes' },
  ],
};

initialState = { isOpen: false };

const handleOpen = () => {
  setState({ isOpen: true });
};
const handleClose = () => setState({ isOpen: false });

<div>
  <Button theme="tertiary" onClick={handleOpen}>
    Open fly-in
  </Button>
  <ProductSpecs isOpen={state.isOpen} onClose={handleClose} {...props} />
</div>;
```
