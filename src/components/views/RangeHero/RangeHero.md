Basic RangeHero component

```js
const bgImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const props = {
  subtitle: 'Su Pure By Keune',
  title: 'Vital Nutrition',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'More About This Range',
  buttonUrl: '/go',
  theme: 'dark',
  backgroundImage: <ResponsiveImage {...bgImage} />,
};

<RangeHero {...props} />;
```
