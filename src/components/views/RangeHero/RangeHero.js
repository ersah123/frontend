import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './RangeHero.css';
import Hero from '../Hero/Hero';
import { HeroPropTypes, HeroDefaultPropTypes } from '../../../types';

@themableWithStyles(styles)
class RangeHero extends PureComponent {
  static propTypes = {
    ...HeroPropTypes,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    ...HeroDefaultPropTypes,
    align: 'leftTop',
    className: null,
    children: null,
    css: {
      root: styles.root,
      contentWrapper: styles.contentWrapper,
      content: styles.content,
      subtitle: styles.subtitle,
      text: styles.text,
      themeDark: styles.themeDark,
      themeLight: styles.themeLight,
      bgWrapper: styles.bgWrapper,
      bg: styles.bg,
      button: styles.button,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <Hero {...rest} overrideCss={css} hasScrollTo={false}>
          {children}
        </Hero>
      </div>
    );
  }
}

export default RangeHero;
