Basic Promo component

This component is basically the same as `<Hero />` but contains an extra property 'isCompact'.

```js
const bgImage = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const props = {
  title: 'Go Brighter',
  theme: 'dark',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'I am a button',
  buttonUrl: '/go',
  backgroundImage: <ResponsiveImage {...bgImage} />,
};

<Promo {...props} />;
```
