import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './Promo.css';
import Hero from '../Hero/Hero';
import { HeroPropTypes, HeroDefaultPropTypes } from '../../../types';

@themableWithStyles(styles)
class Promo extends PureComponent {
  static propTypes = {
    ...HeroPropTypes,
    /**
     * The full-half width config.
     */
    isCompact: PropTypes.bool,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    ...HeroDefaultPropTypes,
    isCompact: false,
    isFullHeight: false, // this is what distinguishes Promo from Hero
    css: {
      root: styles.root,
      isCompact: styles.isCompact,
    },
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      isCompact,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      { [css.isCompact]: isCompact },
      classNameProp,
    );

    return (
      <div className={className}>
        <Hero {...rest} hasScrollTo={false}>
          {children}
        </Hero>
      </div>
    );
  }
}

export default Promo;
