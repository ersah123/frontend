import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './PromoDouble.css';

@themableWithStyles(styles)
class PromoDouble extends PureComponent {
  static propTypes = {
    /**
     * Two times the properties of a `<Promo />`.
     */
    promo1: PropTypes.shape().isRequired,
    promo2: PropTypes.shape().isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const { promo1, promo2, className: classNameProp, css } = this.props;
    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <promo1.type isCompact {...promo1.props} />
        <promo2.type isCompact {...promo2.props} />
      </div>
    );
  }
}

export default PromoDouble;
