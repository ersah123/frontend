Basic PromoDouble component

This is twice the normal `<Promo />`.

```js
const bgImageOne = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'https://www.keune.com/Portals/2/Keune-Color-Ultimate-Blonde-Carroussel-right-1166x503.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const bgImageTwo = {
  id: 2839,
  componentName: 'ResponsiveImage',
  defaultSize: 'lg',
  sizes: {
    lg: {
      alt: 'KEUNE',
      src:
        'http://www.keune.com/Portals/1/images/Brands/Color/Keune-Color-CC19-Modelduo-1080x1080-3.jpg',
      width: 1920,
      height: 1080,
    },
  },
};

const promoOneProps = {
  title: 'Go Brighter #1',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'I am a button',
  buttonUrl: '/go',
  backgroundImage: <ResponsiveImage {...bgImageOne} />,
};

const promoTwoProps = {
  title: 'Go Brighter #2',
  text:
    'Extended with a super-powerful Ultimate Power Blonde-blonde powder and three bright new toners.',
  buttonText: 'I am a button',
  buttonUrl: '/go',
  backgroundImage: <ResponsiveImage {...bgImageTwo} />,
};

const props = {
  promo1: <Promo isCompact {...promoOneProps} />,
  promo2: <Promo isCompact {...promoTwoProps} />,
};

<PromoDouble {...props} />;
```
