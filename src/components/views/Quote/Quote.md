Quote component

```js
const profileImage = require('./assets/keune.jpg');

const image = [
  {
    md: {
      alt: 'KEUNE',
      src: profileImage,
      width: 225,
      height: 225,
    },
  },
];

const props = {
  id: 188,
  text: 'Sharing The passion with haristylists all over the world',
  author: 'Keune',
  hasMediaRight: false,
  image,
};

<Quote {...props} />;
```
