import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { Text } from '../../elements';
import styles from './Quote.css';

@themableWithStyles(styles)
class Quote extends PureComponent {
  static propTypes = {
    /**
     * A quote from someone
     */
    text: PropTypes.string,
    /**
     * The author of our quote
     */
    author: PropTypes.string,
    /**
     * The media aligning option
     */
    hasMediaRight: PropTypes.bool,
    /**
     * Picture representing the author
     */
    image: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      wrapper: PropTypes.string,
      mediaWrapper: PropTypes.string,
      content: PropTypes.string,
      subHeading: PropTypes.string,
      headline: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    hasMediaRight: false,
    text: null,
    author: null,
    css: {
      root: styles.root,
      wrapper: styles.wrapper,
      mediaWrapper: styles.mediaWrapper,
      content: styles.content,
      subHeading: styles.subHeading,
      headline: styles.headline,
    },
  };

  getImage = image => {
    if (image && image[0]) {
      if (image[0].md) return image[0].md;
      if (image[0].xs) return image[0].md;
      if (image[0].lg) return image[0].lg;
    }
    return null;
  };

  render() {
    const {
      className: classNameProp,
      image,
      css,
      text,
      author,
      hasMediaRight,
    } = this.props;

    const className = classNames(css.root, classNameProp, {
      [styles.hasMediaRight]: hasMediaRight,
    });
    const imgProps = this.getImage(image);

    return (
      <blockquote className={className}>
        <div className={css.wrapper}>
          <figure className={css.mediaWrapper}>
            {imgProps && <img alt={author} {...imgProps} />}
          </figure>
        </div>
        <div className={css.content}>
          <Text variant="headline" className={css.headline} gutterBottom>
            {text}
          </Text>
          <Text variant="subheading" className={css.subHeading}>
            {author}
          </Text>
        </div>
      </blockquote>
    );
  }
}

export default Quote;
