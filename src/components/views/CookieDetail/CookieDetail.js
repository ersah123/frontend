import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Cookies from 'universal-cookie';
import { themableWithStyles } from 'rsk-components';
import { Button, Checkbox, Text } from '../../elements';
import { prepareCookies, selectCookies } from '../../../utils/helperFunc';

import styles from './CookieDetail.css';

const mapStateToProps = state => {
  const { runtime, form } = state;
  return { runtime, formData: form };
};

@connect(mapStateToProps)
@reduxForm({
  form: 'cookieDetails',
})
@themableWithStyles(styles)
class CookieDetail extends PureComponent {
  cookies = new Cookies();

  selectedCookies = null;

  fieldNamePrefix = 'cookieDetail';

  static propTypes = {
    /**
     * Prop for title
     */
    title: PropTypes.string,
    /**
     * Prop for text
     */
    text: PropTypes.string,
    /**
     * Prop for text
     */
    subtitle: PropTypes.string,
    /**
     * Prop for buttonText
     */
    buttonText: PropTypes.string,
    /**
     * Prop for runtime
     */
    runtime: PropTypes.shape({}),
    /**
     * Prop for formData
     */
    formData: PropTypes.shape({}),
    /**
     * Placeholder for submit function
     */
    handleSubmit: PropTypes.func.isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      cookies: PropTypes.string,
      button: PropTypes.string,
      checkbox: PropTypes.string,
      subtitle: PropTypes.string,
      cookieHeading: PropTypes.string,
      cookieDetail: PropTypes.string,
      cookieDetails: PropTypes.string,
      detailDescription: PropTypes.string,
      cookieDetailRow: PropTypes.string,
      cookieDetailName: PropTypes.string,
      cookieDetailValue: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: null,
    text: null,
    subtitle: null,
    buttonText: null,
    runtime: {},
    formData: {},
    className: null,
    css: {
      root: styles.root,
      cookies: styles.cookies,
      button: styles.button,
      checkbox: styles.checkbox,
      subtitle: styles.subtitle,
      cookieHeading: styles.cookieHeading,
      cookieDetail: styles.cookieDetail,
      cookieDetails: styles.cookieDetails,
      detailDescription: styles.detailDescription,
      cookieDetailRow: styles.cookieDetailRow,
      cookieDetailName: styles.cookieDetailName,
      cookieDetailValue: styles.cookieDetailValue,
    },
  };

  submitForm = form => {
    const tmpSelectedCookies = selectCookies(form.cookieDetail).split(',');
    const selectedCookies = [
      ...new Set([...this.selectedCookies, ...tmpSelectedCookies]),
    ].join(',');
    this.cookies.set('cookiesAgreed', selectedCookies, { path: '/' });
    return false;
  };

  getCookiesFromRuntime = () => {
    const { runtime } = this.props;

    return runtime && runtime.siteInit.cookies
      ? runtime.siteInit.cookies
      : null;
  };

  getSelectedCookies = () => {
    let selectedCookies = [];
    const cookiesAgreed = this.cookies.get('cookiesAgreed');
    if (cookiesAgreed) {
      selectedCookies = cookiesAgreed.split(',');
    }
    return selectedCookies;
  };

  renderDetails = cookie => {
    const { runtime, css } = this.props;
    let cookies = null;
    if (runtime.siteInit.cookies) {
      cookies = runtime.siteInit.cookies;
    }
    return cookie.details.map((item, i) => {
      const key = i;
      return (
        <div
          key={`cookie-section-${cookie.title}-${item.name}-${key}`}
          className={css.cookieDetail}
        >
          <div
            key={`cookie-detail-${item.name}`}
            className={css.cookieDetailRow}
          >
            <Text variant="body2" className={css.cookieDetailName}>
              {cookies.nameText}:
            </Text>
            <Text variant="body2" className={css.cookieDetailValue}>
              {item.name}
            </Text>
          </div>
          <div
            key={`cookie-detail-${item.source}`}
            className={css.cookieDetailRow}
          >
            <Text variant="body2" className={css.cookieDetailName}>
              {cookies.supplierText}:
            </Text>
            <Text variant="body2" className={css.cookieDetailValue}>
              {item.source}
            </Text>
          </div>
          <div
            key={`cookie-detail-${item.text}`}
            className={css.cookieDetailRow}
          >
            <Text variant="body2" className={css.cookieDetailName}>
              {cookies.purposeText}:
            </Text>
            <Text variant="body2" className={css.cookieDetailValue}>
              {item.text}
            </Text>
          </div>
          <div
            key={`cookie-detail-${item.duration}`}
            className={css.cookieDetailRow}
          >
            <Text variant="body2" className={css.cookieDetailName}>
              {cookies.expiryText}:
            </Text>
            <Text variant="body2" className={css.cookieDetailValue}>
              {item.duration}
            </Text>
          </div>
          <div
            key={`cookie-detail-${item.type}`}
            className={css.cookieDetailRow}
          >
            <Text variant="body2" className={css.cookieDetailName}>
              {cookies.typeText}:
            </Text>
            <Text variant="body2" className={css.cookieDetailValue}>
              {item.type}
            </Text>
          </div>
        </div>
      );
    });
  };

  componentDidMount = () => {
    this.selectedCookies = this.getSelectedCookies();
  };

  renderCookies = () => {
    const { formData, css } = this.props;

    if (formData.cookieDetails && formData.cookieDetails.values) {
      Object.keys(formData.cookieDetails.values.cookieDetail).forEach(key => {
        if (!formData.cookieDetails.values.cookieDetail[key]) {
          this.selectedCookies = [
            ...this.selectedCookies.filter(k => k !== key),
          ];
        }
      });
    }

    const cookies = this.getCookiesFromRuntime();

    if (!cookies) return null;

    return prepareCookies(cookies).map(cookie => {
      const checked = this.selectedCookies
        ? this.selectedCookies.includes(cookie.title)
        : cookie.checked;
      return (
        <div key={`cookie-${cookie.title}`} className={css.cookieDetails}>
          <div className={css.cookieHeading}>
            <Checkbox
              className={css.checkbox}
              key={`${this.fieldNamePrefix}[${cookie.title}]`}
              name={`${this.fieldNamePrefix}.${cookie.title}`}
              label={`${cookie.title} (${cookie.details.length})`}
              disabled={cookie.disabled}
              checked={checked}
            />
          </div>
          {/*
        <Text variant="body2" className={css.detailDescription}>
          {cookie.details.description}
        </Text>
        */}
          {this.renderDetails(cookie)}
        </div>
      );
    });
  };

  render() {
    const {
      title,
      text,
      subtitle,
      buttonText,
      handleSubmit,
      className: classNameProp,
      css,
    } = this.props;

    const buttonUrl = '/';

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className}>
        <Text variant="display1" gutterBottom>
          {title}
        </Text>
        <Text variant="body2" className={css.text}>
          {text}
        </Text>
        <Text variant="subheading" className={css.subtitle}>
          {subtitle}
        </Text>
        <form name="cookieDetail" onSubmit={handleSubmit(this.submitForm)}>
          <div className={css.cookies}>{this.renderCookies()}</div>
          <Button
            theme="tertiary"
            to={buttonUrl}
            className={css.button}
            onClick={handleSubmit(this.submitForm)}
          >
            {buttonText}
          </Button>
        </form>
      </div>
    );
  }
}

export default CookieDetail;
