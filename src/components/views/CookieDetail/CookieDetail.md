Basic CookieDetail component

```js
const props = {
  id: 1234,
  title: 'Cookies on the Keune website',
  text:
    'Body 2- 20 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae nibh eu neque pharetra tristique. Nullam vitae vulputate nunc. Sed in magna vel nulla semper molestie vel ac odio.',
  buttonText: 'I Agree',
  cookies: {
    title: 'Cookies on keune.com',
    text: 'Lorem ipsum cookie sit amet.',
    linkUrl: '/en/cookies/',
    linkText: 'terms & conditions',
    buttonText: 'I agree',
    nameText: 'Name',
    supplierText: 'Supplier',
    purposeText: 'Purpose',
    expiryText: 'Expiry',
    typeText: 'Type',
    details: [
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Necessary',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Necessary',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Statistics',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Statistics',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Preferences',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Preferences',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Marketing',
      },
      {
        name: 'Google Analytics',
        source: 'Google',
        text:
          'Ullam molestias autem autem ipsam exercitationem. Voluptas ut consequatur enim et et amet dolores. Voluptatem quo facilis vero necessitatibus nobis facilis ea.',
        type: 'Http',
        duration: '1 Year',
        level: 'Marketing',
      },
    ],
  },
};

<CookieDetail {...props} />;
```
