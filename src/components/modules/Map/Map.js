import {
  GoogleMap,
  Marker,
  withGoogleMap,
  withScriptjs,
} from 'react-google-maps';
import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { themableWithStyles } from 'rsk-components';
import activeMarkerIcon from './icons/MAP-ICON_Marker-focussed.svg';
import ambassadorActiveMarkerIcon from './icons/MAP-ICON_KEUNE-Marker-focussed.svg';
import ambassadorMarkerIcon from './icons/MAP-ICON_KEUNE-Marker.svg';
import { googleMapsApiKey } from '../../../constants/index';
import markerIcon from './icons/MAP-ICON_Marker.svg';
import originMarkerIcon from './icons/MAP-ICON_Marker-location-origin.svg';
import styles from './Map.css';

const theme = [
  {
    featureType: 'administrative',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#444444',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry.stroke',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'labels.text',
    stylers: [
      {
        hue: '#fff600',
      },
    ],
  },
  {
    featureType: 'landscape',
    elementType: 'all',
    stylers: [
      {
        color: '#f2f2f2',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'all',
    stylers: [
      {
        saturation: -100,
      },
      {
        lightness: 45,
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'all',
    stylers: [
      {
        visibility: 'simplified',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'all',
    stylers: [
      {
        color: '#cad3d5',
      },
      {
        visibility: 'on',
      },
    ],
  },
];

const MyMapComponent = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultOptions={{
        styles: theme,
        disableDefaultUI: true,
      }}
      defaultZoom={8}
      defaultCenter={props.coords}
      ref={map => props.getRef(map)}
      center={props.coords}
    >
      {props.salons.map(place => (
        <Marker
          icon={props.marker(place.id, place.loyalty)}
          key={place.id}
          position={{ lat: place.latitude, lng: place.longitude }}
          onClick={() => props.handleClick(place)}
        />
      ))}
      <Marker position={props.searchedOrigin} icon={props.originMarker} />
    </GoogleMap>
  )),
);

@themableWithStyles(styles)
class KeuneMap extends Component {
  static propTypes = {
    /**
     * places array to show on map with marker
     */
    salons: PropTypes.arrayOf(PropTypes.shape()),
    /**
     * Origin
     */
    mapCenter: PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
    }),

    searchedOrigin: PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
    }),
    /**
     * marker click handler
     */
    handleMarkerClick: PropTypes.func.isRequired,
    /**
     * Selected place id
     */
    selectedPlaceId: PropTypes.number,
    /**
     * @ignore
     */
    css: PropTypes.shape({
      mapElement: PropTypes.string,
    }),
  };

  static defaultProps = {
    salons: [],
    selectedPlaceId: null,
    mapCenter: { lat: null, lng: null },
    searchedOrigin: { lat: null, lng: null },
    css: {
      mapElement: styles.mapElement,
    },
  };

  state = {
    originMarker: {
      url: originMarkerIcon,
      scaledSize: new window.google.maps.Size(45, 45),
    },
  };

  componentWillUpdate(nextProps) {
    this.updateBounds(nextProps);
  }

  updateBounds = ({ salons, mapCenter }) => {
    const bounds = new window.google.maps.LatLngBounds();
    salons.forEach(bound =>
      bounds.extend(
        new window.google.maps.LatLng(bound.latitude, bound.longitude),
      ),
    );

    bounds.extend(new window.google.maps.LatLng(mapCenter.lat, mapCenter.lng)); // location (town) you searched
    this.mapRef.fitBounds(bounds);
  };

  onMapMounted = ref => {
    if (!this.mapRef) {
      this.mapRef = ref;
      this.updateBounds(this.props);
    }
  };

  getMarker = (placeId, isAmbassador, selectedPlaceId) => {
    const marker = {
      url: '',
      scaledSize: null,
    };
    if (placeId === selectedPlaceId) {
      // active
      marker.url = isAmbassador ? ambassadorActiveMarkerIcon : activeMarkerIcon;
      marker.scaledSize = new window.google.maps.Size(36, 48);
    } else {
      marker.url = isAmbassador ? ambassadorMarkerIcon : markerIcon;
      marker.scaledSize = new window.google.maps.Size(19, 19);
    }

    return marker;
  };

  render() {
    const { originMarker } = this.state;
    const {
      css,
      salons,
      handleMarkerClick,
      selectedPlaceId,
      mapCenter,
      searchedOrigin,
    } = this.props;

    return (
      <MyMapComponent
        isMarkerShown={false}
        coords={mapCenter}
        searchedOrigin={searchedOrigin}
        salons={salons}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?&key=${googleMapsApiKey}&v=3.exp&libraries=geometry,drawing,places`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div className={css.mapElement} />}
        getRef={this.onMapMounted}
        handleClick={handleMarkerClick}
        originMarker={originMarker}
        marker={(placeId, isAmbassador) =>
          this.getMarker(placeId, isAmbassador, selectedPlaceId)
        }
      />
    );
  }
}

export default KeuneMap;
