import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { KeuneIcon, Text, ResponsiveVideo } from '../../elements';

/* eslint css-modules/no-unused-class: [2, { markAsUsed: ['isPlaying'] }] */
import styles from './Media.css';

@themableWithStyles(styles)
class Media extends PureComponent {
  static propTypes = {
    /** Start video automatically */
    autoPlay: PropTypes.bool,
    /** Optional title, renders a caption */
    title: PropTypes.string,
    /** Different responsive video sizes */
    sizes: PropTypes.objectOf(
      PropTypes.shape({
        src: PropTypes.string,
        poster: PropTypes.string,
        width: PropTypes.number,
        height: PropTypes.number,
      }),
    ).isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      icon: PropTypes.string,
    }),
  };

  static defaultProps = {
    autoPlay: false,
    title: null,
    className: null,
    children: null,
    css: {
      root: styles.root,
      icon: styles.icon,
    },
  };

  state = {
    // eslint-disable-next-line react/destructuring-assignment
    isPlaying: this.props.autoPlay,
  };

  handleClick = ev => {
    const { isPlaying } = this.state;
    ev.preventDefault();

    this.setState({
      isPlaying: !isPlaying,
    });
  };

  handleKeyPress = ev => {
    const { isPlaying } = this.state;
    // space bar
    if (ev.keyCode === 0) {
      this.setState({
        isPlaying: !isPlaying,
      });
    }
  };

  handlePlayPause = play => {
    this.setState({
      isPlaying: play,
    });
  };

  render() {
    const {
      title,
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;
    const { isPlaying } = this.state;

    const className = classNames(
      css.root,
      { [css.isPlaying]: isPlaying },
      classNameProp,
    );

    return (
      <div
        role="button"
        tabIndex={0}
        onClick={this.handleClick}
        onKeyPress={this.handleKeyPress}
        className={className}
      >
        <figure className={styles.media}>
          {ResponsiveVideo && (
            <ResponsiveVideo
              playVideo={isPlaying}
              controls={isPlaying}
              onPlayPause={this.handlePlayPause}
              {...rest}
            />
          )}
          <span className={css.icon}>
            <KeuneIcon name="play" />
          </span>
          {title && (
            <figcaption className={styles.caption}>
              <Text variant="body1">{title}</Text>
            </figcaption>
          )}
        </figure>
      </div>
    );
  }
}

export default Media;
