Basic Media component

```js
const poster = require('./assets/poster-16x9.png');
const video = require('./assets/video-16x9.mp4');

const props = {
  defaultSize: 'lg',
  autoPlay: false,
  title: 'An elephant at sunset',
  sizes: {
    lg: {
      poster: poster,
      src: video,
      width: 1920,
      height: 1080,
    },
  },
};

<Media {...props} />;
```
