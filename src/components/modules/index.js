/* eslint-disable import/prefer-default-export */

/**
 * MODULES
 */
export { default as LanguageSwitcher } from './LanguageSwitcher';
export { default as ProductSwiper } from './ProductSwiper';
export { default as Media } from './Media';
export { default as Map } from './Map/Map';

/**
 * DIRECT RSK Imports
 */
export { Accordion, AccordionContent, Modal, Swiper } from 'rsk-components';
