import React, { PureComponent } from 'react';
import { Swiper, themableWithStyles } from 'rsk-components';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Image } from '../../elements';
import SwiperProps from './SwiperProps';
import styles from './ProductSwiper.css';

@themableWithStyles(styles)
class ProductSwiper extends PureComponent {
  // Proptypes
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * Slides array object for swiper
     */
    slides: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        src: PropTypes.string,
        alt: PropTypes.string,
        width: PropTypes.number,
        height: PropTypes.number,
      }),
    ).isRequired,
    /**
     * Thumbs arrays that corresponds with slides
     */
    thumbs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        src: PropTypes.string,
        alt: PropTypes.string,
        width: PropTypes.number,
        height: PropTypes.number,
      }),
    ),
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      swiperWrapper: PropTypes.string,
      thumbSwiper: PropTypes.string,
      mainSwiper: PropTypes.string,
      slideItem: PropTypes.string,
    }),
    swiperParams: SwiperProps,
  };

  static defaultProps = {
    className: null,
    children: null,
    thumbs: [],
    css: {
      root: styles.root,
      swiperWrapper: styles.swiperWrapper,
      thumbSwiper: styles.thumbSwiper,
      mainSwiper: styles.mainSwiper,
    },
    swiperParams: {
      loop: false,
      containerClass: 'swiper-container',
      wrapperClass: 'swiper-wrapper',
      slideClass: 'swiper-slide',
      ContainerEl: 'div',
      WrapperEl: 'div',
    },
  };

  constructor(props) {
    super(props);
    this.swiper = null;
  }

  state = {
    selectedSlide: 0,
  };

  swiperRef = ref => {
    if (ref) this.swiper = ref.swiper;
  };

  gallerySwiperParams = swiperParams => ({
    ...swiperParams,
    slidesPerView: 1,
    spaceBetween: 200, // this amount of spacing required for preventing overlapping items on 1024px browser width
    scrollbar: {
      el: '.swiper-scrollbar',
    },
    slideToClickedSlide: true,
    breakpoints: {
      1280: {
        spaceBetween: 300,
      },
      1024: {
        spaceBetween: 0,
      },
    },
  });

  goToSlide(index) {
    this.setState({
      selectedSlide: index,
    });
  }

  renderThumbs = slides => {
    if (!slides.length) return null;
    return slides.map((slide, i) => {
      const guid = `thumb${slide.id}${i}`;
      return (
        <div
          key={guid}
          role="button"
          tabIndex="0"
          onClick={() => this.goToSlide(i)}
          onKeyDown={e => {
            if (e.keyCode === 13) this.goToSlide(i);
          }}
          className={styles.thumbItem}
        >
          <Image {...slide} />
        </div>
      );
    });
  };

  renderSlides = slides =>
    slides.map((slide, i) => {
      const guid = `slide${slide.id}${i}`;
      return (
        <div key={guid} className={styles.slideItem}>
          <figure className={styles.mediaWrapper}>
            <Image {...slide} />
          </figure>
        </div>
      );
    });

  render() {
    const {
      className: classNameProp,
      children,
      css,
      swiperParams,
      slides,
      thumbs,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    const { selectedSlide } = this.state;

    return (
      <div className={className} {...rest}>
        <div className={css.swiperWrapper}>
          <div className={css.thumbSwiper}>{this.renderThumbs(thumbs)}</div>
          <div className={css.mainSwiper}>
            <Swiper
              swiperParams={this.gallerySwiperParams(swiperParams)}
              selected={selectedSlide}
              ref={this.swiperRef}
            >
              {this.renderSlides(slides)}
            </Swiper>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductSwiper;
