Basic ProductSwiper component

```js
const mediaSlides = [
  require('./assets/media-01.jpg'),
  require('./assets/media-02.jpg'),
  require('./assets/media-03.jpg'),
  require('./assets/media-04.jpg'),
];

const mediaThumbs = [
  require('./assets/thumb01.png'),
  require('./assets/thumb02.png'),
  require('./assets/thumb03.png'),
  require('./assets/thumb04.png'),
];

const props = {
  slides: [
    {
      id: 0,
      src: mediaSlides[0],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 1,
      src: mediaSlides[1],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 2,
      src: mediaSlides[2],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 3,
      src: mediaSlides[3],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
  ],
  thumbs: [
    {
      id: 0,
      src: mediaThumbs[0],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 1,
      src: mediaThumbs[1],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 2,
      src: mediaThumbs[2],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
    {
      id: 3,
      src: mediaThumbs[3],
      alt: 'alt',
      width: 1024,
      height: 757,
    },
  ],
};

<ProductSwiper {...props} />;
```
