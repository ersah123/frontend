import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './LanguageSwitcher.css';
import { KeuneIcon, Text } from '../../elements';

@themableWithStyles(styles)
class LanguageSwitcher extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const {
      className: classNameProp,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className} {...rest}>
        <KeuneIcon name="global" />
        <Text variant="body1">Nederlands</Text>
      </div>
    );
  }
}

export default LanguageSwitcher;
