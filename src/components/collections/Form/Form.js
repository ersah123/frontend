import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { reduxForm, FormSection } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import { Text, Input, FormRow, Button } from '../../elements';
import PersonalInfo from '../PersonalInfo';
import { ButtonGroup } from '../../views';
import valFunc from '../../globals/validation';

import styles from './Form.css';

const FORM_NAME = 'test-form';

@reduxForm({
  form: FORM_NAME,
})
@themableWithStyles(styles)
class Form extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
    handleSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
    },
  };

  submitForm = ev => {
    ev.preventDefault();
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      handleSubmit,
      // The rest values
      ...rest
    } = this.props;
    const isStacked = true;

    const className = classNames(styles.root, classNameProp);
    return (
      <div className={className} {...rest}>
        <form onSubmit={handleSubmit(this.submitForm)}>
          <Text variant="headline" gutterBottom>
            <FormattedMessage
              id="form.exampleForm"
              description="Heading on top of the example form"
              defaultMessage="Example form"
            />
          </Text>
          <Text variant="body1" paragraph>
            This is an example form, see /components/collections/Form. It is
            part of a specific template, see /components/templates/FormExample.
            Note that not everything is translated, just the headings.
          </Text>
          <Text variant="headline" gutterBottom className={styles.text}>
            <FormattedMessage
              id="form.personalInfo"
              description="Personal info heading of example form"
              defaultMessage="Personal info"
            />
          </Text>
          <PersonalInfo />
          <Text variant="headline" gutterBottom className={styles.text}>
            <FormattedMessage
              id="form.address"
              description="Address heading of example form"
              defaultMessage="Address"
            />
          </Text>
          <FormSection name="address">
            <FormRow isStacked={isStacked}>
              <Input
                name="postalCode"
                label="Postcode"
                type="text"
                onBlur={this.findAddress}
                required
                validate={[valFunc.required, valFunc.postalCode]}
              />
            </FormRow>
            <FormRow isStacked={isStacked}>
              <Input
                name="houseNumber"
                label="Number"
                type="text"
                onBlur={this.findAddress}
                required
                validate={[valFunc.required, valFunc.alphaNumeric]}
              />
              <Input name="houseAddition" label="Toevoeging" type="text" />
            </FormRow>
            <FormRow isStacked={isStacked}>
              <Input
                name="street"
                disabled
                label="Street"
                type="text"
                required
                validate={[valFunc.required]}
              />
            </FormRow>
            <FormRow isStacked={isStacked}>
              <Input
                name="city"
                disabled
                label="City"
                type="text"
                required
                validate={[valFunc.required]}
              />
            </FormRow>
          </FormSection>
          <ButtonGroup>
            <Button view="outline" theme="primary" type="submit">
              Send
            </Button>
          </ButtonGroup>
        </form>
      </div>
    );
  }
}

export default Form;
