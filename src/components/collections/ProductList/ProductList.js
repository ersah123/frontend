import React, { Fragment, PureComponent } from 'react';
import {
  ResponsiveImage as ResponsiveImageComponent,
  themableWithStyles,
} from 'rsk-components';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { connect } from 'react-redux';

import * as productActions from '../../../actions/product.action';
import { getProductsByFilter } from '../../../reducers/product.reducer';

import Tile from './Tile/Tile';
import DoubleTile from './DoubleTile/DoubleTile';
import ProductListHeader from './ProductListHeader/ProductListHeader';
import { FlexiblePromo } from '../../views';
import ResponsiveImage from '../../../models/ResponsiveImage';

// eslint-disable-next-line css-modules/no-unused-class
import promoStyles from './PLPPromo.css';
import styles from './ProductList.css';

const mapStateToProps = ({ product }) => ({
  products: product.products,
  loading: product.loading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(productActions, dispatch);

@connect(
  mapStateToProps,
  mapDispatchToProps,
)
@themableWithStyles(styles)
class ProductList extends PureComponent {
  static propTypes = {
    /**
     * List with products.
     */
    products: PropTypes.shape().isRequired,
    /**
     * List with promos.
     */
    promos: PropTypes.arrayOf(PropTypes.shape()),
    /**
     * Are products loading.
     */
    loading: PropTypes.bool,
    /**
     * Gets products from state (or loads them).
     */
    getProducts: PropTypes.func.isRequired,
    /**
     * Filter by brand, category etc.
     */
    filters: PropTypes.shape(),
    /**
     * Productlist title
     */
    title: PropTypes.string,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      fakeItem: PropTypes.string,
    }),
  };

  currentDoubleTilePlace = 0;

  placeCounter = 1;

  doubleTileCount = 0;

  state = {
    products: this.props.products, // eslint-disable-line react/destructuring-assignment
  };

  static defaultProps = {
    filters: null,
    title: undefined,
    promos: [],
    loading: true,
    className: null,
    children: null,
    css: {
      root: styles.root,
      fakeItem: styles.fakeItem,
    },
  };

  componentDidMount() {
    const { getProducts, filters } = this.props;
    if (filters) getProducts(filters);
    else getProducts({});
  }

  static getDerivedStateFromProps(nextProps) {
    const { products, filters } = nextProps;
    let results = [];
    if (filters) {
      results = getProductsByFilter(products, filters);
    }

    return {
      products: results,
    };
  }

  /**
   * Fill last row to the 4n
   */
  fillSlots = products => {
    const { css } = this.props;
    const itemCount = products.length;
    const actualGridLength = itemCount + this.doubleTileCount * 2; // 1 double tile = 2 tile
    const lastRowItemCount = actualGridLength % 4;
    const fakeItemCount = 4 - lastRowItemCount;
    return new Array(fakeItemCount).fill().map((_, i) => {
      const key = `fake${i}`;
      return <div key={key} className={css.fakeItem} />;
    });
  };

  renderPromo = promos => {
    const tilePositions = [4, 8];
    let currentPromo = null;
    if (this.placeCounter === tilePositions[this.currentDoubleTilePlace]) {
      this.currentDoubleTilePlace = 1 - this.currentDoubleTilePlace;
      this.placeCounter = 0;
      const responsiveImage = new ResponsiveImage({
        media: promos[this.doubleTileCount].media,
      });
      currentPromo = {
        ...promos[this.doubleTileCount],
        backgroundImage: <ResponsiveImageComponent {...responsiveImage} />,
      };
      this.doubleTileCount += 1;
      this.placeCounter += 1;
      return (
        <DoubleTile>
          <FlexiblePromo {...currentPromo} css={promoStyles} />
        </DoubleTile>
      );
    }
    this.placeCounter += 1;
    return null;
  };

  renderProducts = (products, promos) => {
    this.currentDoubleTilePlace = 0;
    this.placeCounter = 1;
    this.doubleTileCount = 0;

    return products.map(product => {
      const finalProduct = { ...product, media: product.media[0] };

      return (
        <Fragment>
          <Tile product={finalProduct} key={product.id} />
          {promos && promos.length > 0 && this.renderPromo(promos)}
        </Fragment>
      );
    });
  };

  render() {
    const {
      filters,
      loading,
      promos,
      title,
      className: classNameProp,
      css,
    } = this.props;
    const { products } = this.state;

    const className = classNames(css.root, classNameProp);

    if (loading) {
      return (
        <div className={className}>
          <ProductListHeader title="Loading products..." />
        </div>
      );
    }
    return (
      <Fragment>
        <ProductListHeader title={title} filters={filters} />
        <div className={className}>
          {products && this.renderProducts(products, promos)}
          {products && this.fillSlots(products)}
        </div>
      </Fragment>
    );
  }
}

export default ProductList;
