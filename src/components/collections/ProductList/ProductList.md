Basic ProductList component

```js
const image = require('../../views/ProductTile/assets/model.jpg');

const productTile = {
  product: {
    sku: '27431',
    name: 'Style Root Volumizer',
    description: 'Style Root Volumizer',
    price: 9.5,
    media: {
      md: {
        width: 652,
        height: 652,
        alt: 'Keune',
        src: image,
      },
    },
  },
};

const filters = [{ category: 'Care' }];

const tile = {
  ...productTile,
  component: {
    componentName: 'Tile',
    ...productTile,
  },
};

const doubleTile = {
  component: {
    componentName: 'DoubleTile',
  },
};

<ProductList filters={filters}>
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <DoubleTile {...doubleTile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <DoubleTile {...doubleTile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
  <Tile {...tile} />
</ProductList>;
```
