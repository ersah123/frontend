import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './Tile.css';
import ProductTile from '../../../views/ProductTile/ProductTile';

@themableWithStyles(styles)
class Tile extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const {
      className: classNameProp,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);

    return (
      <div className={className}>
        <ProductTile {...rest} />
      </div>
    );
  }
}

export default Tile;
