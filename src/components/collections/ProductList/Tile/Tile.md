Basic Tile component

```js
const productImage = require('../../../views/ProductTile/assets/model.jpg');

const props = {
  product: {
    sku: '27431',
    name: 'Style Root Volumizer',
    description: 'Style Root Volumizer',
    price: 0.0,
    media: {
      md: {
        width: 652,
        height: 652,
        alt: 'Keune',
        src: productImage,
      },
    },
  },
};

<Tile {...props} />;
```
