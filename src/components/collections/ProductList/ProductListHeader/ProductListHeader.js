import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { Text } from '../../../elements';

import styles from './ProductListHeader.css';

@themableWithStyles(styles)
class ProductListHeader extends PureComponent {
  static propTypes = {
    /**
     * Productlist title
     */
    title: PropTypes.string,
    /**
     * Filter by brand, category etc.
     */
    filters: PropTypes.shape(),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    title: 'Productlist',
    filters: null,
    className: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const { filters, title, className: classNameProp, css } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className} title={JSON.stringify(filters)}>
        <Text variant="display3">{title}</Text>
      </div>
    );
  }
}

export default ProductListHeader;
