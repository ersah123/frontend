Basic ProductListHeader component

```js
const props = {
  title: 'Product List Header',
};

<ProductListHeader {...props} />;
```
