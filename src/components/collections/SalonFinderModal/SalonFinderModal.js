import { Modal, themableWithStyles } from 'rsk-components';
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { SalonFinder } from '../../views';
import { Text } from '../../elements';
import styles from './SalonFinderModal.css';

@themableWithStyles(styles)
class SalonFinderModal extends PureComponent {
  static propTypes = {
    handleClose: PropTypes.func.isRequired,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      modalCover: PropTypes.string,
      overlay: PropTypes.string,
      closeText: PropTypes.string,
    }),
  };

  static defaultProps = {
    css: {
      modalCover: styles.modalCover,
      overlay: styles.overlay,
      closeText: styles.closeText,
    },
  };

  render() {
    const { css, handleClose } = this.props;

    return (
      <Modal className={css.modalCover} overlayClassName={css.overlay}>
        <div
          className={css.closeText}
          onClick={handleClose}
          onKeyUp={e => (e.keyCode === 27 ? handleClose : null)}
          role="button"
          tabIndex="-1"
        >
          <Text variant="body1">Close</Text>
        </div>
        <SalonFinder isModal handleClose={handleClose} />
      </Modal>
    );
  }
}

export default SalonFinderModal;
