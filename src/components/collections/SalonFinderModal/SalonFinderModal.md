Basic SalonFinderModal component

```js
initialState = { isOpen: false };

const handleOpen = () => setState({ isOpen: true });
const handleClose = () => setState({ isOpen: false });

<div>
  <Button theme="tertiary" onClick={handleOpen}>
    Open Salon Finder
  </Button>
  {state.isOpen ? <SalonFinderModal /> : null}
</div>;
```
