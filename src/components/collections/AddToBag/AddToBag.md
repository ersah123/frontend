Basic AddToBag component

```js
const props = {
  buttonText: 'ADD TO BAG',
};

<AddToBag {...props} />;
```
