import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './AddToBag.css';
import { Button, KeuneIcon } from '../../elements';

@themableWithStyles(styles)
class AddToBag extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    children: null,
    css: {
      root: styles.root,
      // itemCountControls: styles.itemCountControls,
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      itemCount: 1,
    };
  }

  incrementItem = itemCount => {
    this.setState({ itemCount: itemCount + 1 });
  };

  decrementItem = itemCount => {
    if (itemCount >= 1) {
      this.setState({ itemCount: itemCount - 1 });
    }
  };

  render() {
    const {
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    const { itemCount } = this.state;

    return (
      <div className={className} {...rest}>
        <div className={css.itemCountControls}>
          <span
            role="button"
            tabIndex="0"
            onClick={() => this.decrementItem(itemCount)}
            onKeyDown={e => {
              if (e.keyCode === 13) this.incrementItem(itemCount);
            }} // enter key
          >
            <KeuneIcon name="minus" />
          </span>
          <span>{itemCount}</span>
          <span
            role="button"
            tabIndex="0"
            onClick={() => this.incrementItem(itemCount)}
            onKeyDown={e => {
              if (e.keyCode === 13) this.incrementItem(itemCount);
            }} // enter key
          >
            <KeuneIcon name="plus" />
          </span>
        </div>
        <Button theme="tertiary" size="large">
          Add To Bag
        </Button>
      </div>
    );
  }
}

export default AddToBag;
