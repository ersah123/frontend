import { Button, ScrollTo, themableWithStyles } from 'rsk-components';
import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FormattedCurrency, KeuneIcon, RichText, Text } from '../../elements';

// import AddToBag from '../AddToBag';
import GraphBar from './GraphBar';
import Product from '../../../models/Product';
import ProductSwiper from '../../modules/ProductSwiper/ProductSwiper';
import { SalonFinderModal } from '..';
import { StickyHeader } from '../../views';
import styles from './ProductHero.css';

@themableWithStyles(styles)
class ProductHero extends PureComponent {
  static propTypes = {
    /**
     * Prop for buttonText
     */
    buttonText: PropTypes.string,
    /**
     * Prop for buttonUrl
     */
    buttonUrl: PropTypes.string,
    /**
     * Prop for holdFactorText
     */
    holdFactorText: PropTypes.string,
    /**
     * Prop for shineFactorText
     */
    shineFactorText: PropTypes.string,
    /**
     * Prop for suitableForText
     */
    suitableForText: PropTypes.string,
    /**
     * A `Product` object.
     */
    product: PropTypes.shape(Product).isRequired,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      richText: PropTypes.string,
      content: PropTypes.string,
      category: PropTypes.string,
      productInfo: PropTypes.string,
      quantity: PropTypes.string,
      price: PropTypes.string,
      reference: PropTypes.string,
      suitable: PropTypes.string,
      swiper: PropTypes.string,
      labelContainer: PropTypes.string,
      packshotWrapper: PropTypes.string,
      scrollTo: PropTypes.string,
      locationBtn: PropTypes.string,
      contentInner: PropTypes.string,
      priceSection: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    buttonText: '',
    buttonUrl: '',
    holdFactorText: '',
    shineFactorText: '',
    suitableForText: '',
    css: {
      root: styles.root,
      richText: styles.richText,
      content: styles.content,
      category: styles.category,
      productInfo: styles.productInfo,
      quantity: styles.quantity,
      price: styles.price,
      reference: styles.reference,
      suitable: styles.suitable,
      swiper: styles.swiper,
      labelContainer: styles.labelContainer,
      packshotWrapper: styles.packshotWrapper,
      scrollTo: styles.scrollTo,
      locationBtn: styles.locationBtn,
      contentInner: styles.contentInner,
      priceSection: styles.priceSection,
    },
  };

  state = { isOpen: false };

  handleOpen = e => {
    e.preventDefault();
    this.setState({ isOpen: true });
  };

  handleClose = () => this.setState({ isOpen: false });

  renderLabels = labels =>
    labels.map((label, i) => {
      const id = `id${i}`;
      return (
        <Text key={id} variant="body1">
          {label}
        </Text>
      );
    });

  render() {
    const {
      product,
      holdFactorText,
      shineFactorText,
      suitableForText,
      buttonText,
      buttonUrl,
      className: classNameProp,
      css,
    } = this.props;

    const {
      name,
      subtitle,
      text,
      labels,
      price,
      size,
      ean,
      hold,
      shine,
      suitableFor,
      media,
    } = product;

    const { isOpen } = this.state;

    const slides = media.map(slide => {
      if (slide.md) return slide.md;
      if (slide.lg) return slide.lg;
      return slide[Object.keys(slide)[0]];
    });

    const thumbs = media.map(slide => {
      if (slide.xs) return slide.xs;
      if (slide.sm) return slide.sm;
      if (slide.md) return slide.md;
      return slide[Object.keys(slide)[0]];
    });

    const className = classNames(css.root, classNameProp);

    return (
      <Fragment>
        <StickyHeader
          name={name}
          price={price}
          buttonUrl={buttonUrl}
          buttonText={buttonText}
          image={thumbs[0]}
        />
        <article className={className}>
          <div className={css.packshotWrapper}>
            <ProductSwiper
              slides={slides}
              thumbs={thumbs}
              className={css.swiper}
            />
            <div className={css.content}>
              <Text
                variant="subheading"
                className={css.category}
                headlineMapping={{ subheading: 'h2' }}
              >
                {subtitle}
              </Text>
              <Text variant="display3" headlineMapping={{ display3: 'h1' }}>
                {name}
              </Text>
              <div className={css.productInfo}>
                <Text
                  variant="display1"
                  className={css.quantity}
                  headlineMapping={{ display1: 'span' }}
                >
                  {size}
                </Text>
                <Text
                  variant="body1"
                  className={css.reference}
                  headlineMapping={{ body1: 'span' }}
                >
                  {ean}
                </Text>
              </div>
              {labels && (
                <div className={css.labelContainer}>
                  {this.renderLabels(labels)}
                </div>
              )}
              {/* reverse point */}
              <div className={css.contentInner}>
                <div>
                  <RichText html={text} className={css.richText} />
                  {suitableFor && (
                    <div className={css.suitable}>
                      <Text variant="subheading">{suitableForText}</Text>
                      <Text variant="body1" gutterBottom>
                        {suitableFor}
                      </Text>
                    </div>
                  )}
                  {hold && (
                    <Fragment>
                      <Text variant="subheading">{holdFactorText}</Text>
                      <GraphBar value={hold} />
                    </Fragment>
                  )}
                  {shine && (
                    <Fragment>
                      <Text variant="subheading">{shineFactorText}</Text>
                      <GraphBar value={shine} />
                    </Fragment>
                  )}
                </div>
                <div className={css.priceSection}>
                  {price > 0 && (
                    <Text variant="subheading" className={css.price}>
                      <FormattedCurrency currency="EUR" value={price} />
                    </Text>
                  )}
                  {/* <AddToBag buttonText={buttonText} /> */}
                  {buttonText && (
                    <Button
                      theme="tertiary"
                      size="large"
                      iconAfter="location"
                      className={css.locationBtn}
                      onClick={this.handleOpen}
                    >
                      <Text variant="subheading">{buttonText}</Text>
                      <KeuneIcon name="location" />
                    </Button>
                  )}
                </div>
              </div>
              {/* reverse point */}
            </div>
          </div>
          {isOpen ? <SalonFinderModal handleClose={this.handleClose} /> : null}
          <div className={css.scrollTo}>
            <a href="#test">
              <KeuneIcon name="chevron" title="Scroll down" />
            </a>
            <ScrollTo offset={80} anchor="test" />
          </div>
        </article>
      </Fragment>
    );
  }
}

export default ProductHero;
