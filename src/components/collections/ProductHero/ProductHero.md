Basic ProductHero component

```js
const slides = [
  require('./assets/media-02.jpg'),
  require('./assets/media-03.jpg'),
  require('./assets/media-04.jpg'),
  require('./assets/media-01.jpg'),
];

const thumbs = [
  require('./assets/thumb02.png'),
  require('./assets/thumb03.png'),
  require('./assets/thumb04.png'),
  require('./assets/thumb01.png'),
];

const props = {
  product: {
    ean: '283845',
    name: 'Care Silver Saviour Shampoo',
    subtitle: 'Silver Saviour',
    text:
      'Care Silver Savior Shampoo contains brass-busting violet pigments to neutralize unwanted warm tones and nourishing Provitamin B5 to keep your hair looking and feeling silky soft. And some extra for just in case who knows what.',
    size: '200ml',
    hold: 5,
    shine: 3,
    suitableFor: 'All hair types. And some extra for just in case.',
    labels: ['New fragrance', 'Provitamin B5'],
    price: 12.95,
    buttonText: 'SALONFINDER',
    buttonUrl: '/add',
    media: [
      {
        md: {
          width: 1024,
          height: 757,
          alt: 'Hairspray Keune Style Volume ',
          src: slides[0],
        },
        sm: {
          width: 640,
          height: 473,
          alt: 'Hairspray Keune Style Volume',
          src: thumbs[0],
        },
      },
      {
        md: {
          width: 1024,
          height: 757,
          alt: 'Hairspray Keune Style Volume',
          src: slides[1],
        },
        xs: {
          width: 128,
          height: 95,
          alt: 'Hairspray Keune Style Volume',
          src: thumbs[1],
        },
      },
      {
        md: {
          width: 1024,
          height: 757,
          alt: 'Hairspray Keune Style Volume',
          src: slides[2],
        },
        sm: {
          width: 640,
          height: 473,
          alt: 'Hairspray Keune Style Volume',
          src: thumbs[2],
        },
      },
      {
        md: {
          width: 1024,
          height: 757,
          alt: 'Hairspray Keune Style Volume',
          src: slides[3],
        },
        sm: {
          width: 640,
          height: 473,
          alt: 'Hairspray Keune Style Volume',
          src: thumbs[3],
        },
      },
    ],
  },
};

<ProductHero {...props} />;
```
