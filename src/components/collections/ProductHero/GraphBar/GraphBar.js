import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import styles from './GraphBar.css';

@themableWithStyles(styles)
class GraphBar extends PureComponent {
  static propTypes = {
    value: PropTypes.number,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      box: PropTypes.string,
      full: PropTypes.string,
    }),
  };

  static defaultProps = {
    value: 0,
    className: null,
    children: null,
    css: {
      root: styles.root,
      box: styles.box,
      full: styles.full,
    },
  };

  renderBoxes = value => {
    const { css } = this.props;

    return Array.from(Array(10).keys()).map(k => {
      const className =
        k <= value - 1 ? classNames(css.box, css.full) : css.box;
      return <span className={className} key={k} />;
    });
  };

  render() {
    const {
      value,
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className} {...rest}>
        {this.renderBoxes(value)}
        {children}
      </div>
    );
  }
}

export default GraphBar;
