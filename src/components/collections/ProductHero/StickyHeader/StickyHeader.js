import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Sticky } from 'react-sticky';
import { Image, themableWithStyles } from 'rsk-components';
import {
  /* KeuneIcon, */ Button,
  FormattedCurrency,
  Text,
} from '../../../elements';
import { SalonFinderModal } from '../..';

import styles from './StickyHeader.css';

@themableWithStyles(styles)
class StickyHeader extends PureComponent {
  static propTypes = {
    /**
     * Prop for id
     */
    id: PropTypes.number,
    /**
     * Prop for name
     */
    name: PropTypes.string,
    /**
     * Prop for price
     */
    price: PropTypes.number,
    buttonText: PropTypes.string,
    buttonUrl: PropTypes.string,
    /**
     * Prop for image
     */
    image: PropTypes.shape({
      id: PropTypes.number,
      src: PropTypes.string,
      alt: PropTypes.string,
      width: PropTypes.number,
      height: PropTypes.number,
    }),
    /**
     * The top offset of the component
     */
    topOffset: PropTypes.number,
    /**
     * The bottom offset of the component
     */
    bottomOffset: PropTypes.number,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      image: PropTypes.string,
      title: PropTypes.string,
      price: PropTypes.string,
      rightColumn: PropTypes.string,
      favourite: PropTypes.string,
      button: PropTypes.string,
      buttonMobile: PropTypes.string,
      content: PropTypes.string,
      show: PropTypes.string,
      hide: PropTypes.string,
    }),
  };

  static defaultProps = {
    id: null,
    name: null,
    price: null,
    buttonText: null,
    buttonUrl: null,
    image: null,
    className: null,
    children: null,
    topOffset: 750, // will change according to ProductHero page content
    bottomOffset: 1200, // will change according to ProductHero page content
    css: {
      root: styles.root,
      image: styles.image,
      title: styles.title,
      price: styles.price,
      rightColumn: styles.rightColumn,
      favourite: styles.favourite,
      button: styles.button,
      buttonMobile: styles.buttonMobile,
      content: styles.content,
      show: styles.show,
      hide: styles.hide,
    },
  };

  state = { isOpen: false };

  handleOpen = e => {
    e.preventDefault();
    this.setState({ isOpen: true });
  };

  handleClose = () => this.setState({ isOpen: false });

  renderButton = (buttonText, buttonUrl, buttonClass, isWide = false) => (
    <div className={buttonClass}>
      <Button
        theme="tertiary"
        wide={isWide}
        to={buttonUrl}
        onClick={this.handleOpen}
      >
        {buttonText}
      </Button>
    </div>
  );

  show = distanceFromTop => {
    const { topOffset, css } = this.props;
    if (distanceFromTop < -topOffset) {
      return css.show;
    }
    return null;
  };

  hide = distanceFromBottom => {
    const { bottomOffset, css } = this.props;
    if (distanceFromBottom < bottomOffset) {
      return css.hide;
    }
    return null;
  };

  render() {
    const {
      id,
      name,
      price,
      image,
      topOffset,
      bottomOffset,
      buttonText,
      buttonUrl,
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const { isOpen } = this.state;

    const className = classNames(css.root, classNameProp);
    return (
      <Sticky topOffset={topOffset}>
        {({ style, distanceFromTop, distanceFromBottom }) => (
          <div
            className={classNames(
              className,
              this.show(distanceFromTop),
              this.hide(distanceFromBottom),
            )}
            {...rest}
            style={style}
          >
            <div className={css.content}>
              <Image className={css.image} {...image} />
              <Text className={css.title} variant="body2" align="left">
                {name}
              </Text>
              <div className={css.rightColumn}>
                {price > 0 && (
                  <div className={css.price}>
                    <FormattedCurrency currency="EUR" value={price} />
                  </div>
                )}
                {buttonText &&
                  this.renderButton(buttonText, buttonUrl, css.button)}
                {/* <KeuneIcon className={css.favourite} name="favourite-fill" /> */}
              </div>
            </div>
            {buttonText &&
              this.renderButton(buttonText, buttonUrl, css.buttonMobile, true)}
            {isOpen ? (
              <SalonFinderModal handleClose={this.handleClose} />
            ) : null}
          </div>
        )}
      </Sticky>
    );
  }
}

export default StickyHeader;
