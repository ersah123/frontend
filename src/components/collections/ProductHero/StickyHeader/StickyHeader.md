Basic StickyHeader component

```js
const image = {
  src: require('../assets/thumb01.png'),
  alt: 'Product Image',
};

const props = {
  id: 1,
  title: 'Care Silver Saviour Shampoo very long title',
  price: 12.34,
  image,
};

<StickyHeader {...props} />;
```
