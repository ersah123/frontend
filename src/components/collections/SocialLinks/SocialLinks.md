Basic SocialLinks component

```js
const socialLinks = [
  {
    title: 'Facebook',
    href: 'https://www.facebook.com/KeuneHaircosmetics',
  },
  {
    title: 'Twitter',
    href: 'http://www.twitter.com/keune_hair',
  },
  {
    title: 'Instagram',
    href: 'https://www.instagram.com/keunehaircosmetics/',
  },
  {
    title: 'LinkedIn',
    href: 'http://www.linkedin.com/company/keune-haircosmetics',
  },
  {
    title: 'Pinterest',
    href: 'http://www.pinterest.com/keunehair/',
  },
  {
    title: 'Youtube',
    href: 'http://www.youtube.com/user/KeuneHaircosmetics1',
  },
];

<SocialLinks links={socialLinks} />;
```
