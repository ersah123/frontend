import React, { Fragment, PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './SocialLinks.css';
import { KeuneIcon, Link } from '../../elements';

/**
 * Social links on Footer
 *
 * @class SocialLinks
 * @extends {PureComponent}
 */
@themableWithStyles(styles)
class SocialLinks extends PureComponent {
  static propTypes = {
    /**
     * A json structure of social links
     */
    links: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    css: {
      root: styles.root,
    },
  };

  renderSocialLinks = () => {
    const icons = [
      'facebook',
      'twitter',
      'instagram',
      'linkedin',
      'pinterest',
      'youtube',
    ];

    const { links } = this.props;
    if (links.length === 0) return null;

    return icons.map((icon, i) => {
      if (!links[i]) return null;
      const guid = `${icon}${i}`;

      return (
        <li key={guid} className={classNames(styles.item)}>
          <Link
            openNewWindow
            isExternal
            to={links[i].href}
            title={links[i].title}
          >
            <KeuneIcon size="small" name={icon} />
          </Link>
        </li>
      );
    });
  };

  render() {
    return (
      <Fragment>
        <h2>Follow us</h2>
        <ul>{this.renderSocialLinks()}</ul>
      </Fragment>
    );
  }
}

export default SocialLinks;
