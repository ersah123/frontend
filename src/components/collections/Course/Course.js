import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ScrollTo, themableWithStyles } from 'rsk-components';
import { ApplicationForm } from '..';

import styles from './Course.css';

@themableWithStyles(styles)
class Course extends PureComponent {
  state = { isFormOpen: false };

  anchor = null;

  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      form: PropTypes.string,
      visible: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    css: {
      root: styles.root,
      form: styles.form,
      visible: styles.visible,
    },
  };

  constructor(props) {
    super(props);
    this.applicationForm = React.createRef();
  }

  componentDidMount() {
    const { anchor } = this.props;
    this.anchor = anchor;
    document.addEventListener('click', this.anchorClicked, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.anchorClicked);
  }

  anchorClicked = e => {
    const target = e.path.find(i => i.tagName === 'A');
    const href = target ? target.getAttribute('href') : null;
    if (href && href === `#${this.anchor}`) {
      this.setState({ isFormOpen: true });
      e.stopPropagation();
      e.preventDefault();
    }
  };

  render() {
    const { anchor, form, className: classNameProp, css } = this.props;
    const { isFormOpen } = this.state;

    const className = classNames(css.root, classNameProp);
    const formClassNames = classNames(
      css.form,
      isFormOpen ? css.visible : null,
    );
    return (
      <div className={className}>
        <ScrollTo offset={1} anchor={anchor} />
        <div className={formClassNames}>
          <ApplicationForm
            {...form}
            anchor={anchor}
            ref={this.applicationForm}
          />
        </div>
      </div>
    );
  }
}

export default Course;
