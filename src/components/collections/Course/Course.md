Course component

```js
const image = require('../../views/Content/assets/model2.jpg');

const respImage = {
  id: 1234,
  componentName: 'ResponsiveImage',
  title: 'Test ResponsiveImage Title',
  alt: 'Test ResponsiveImage Alt',
  defaultSize: 'xl',
  sizes: {
    xl: {
      src: image,
      width: 900,
      height: 733,
    },
  },
};

const props = {
  anchor: 'uniqueCourseNameToScroll',
  form: {
    header: {
      title: 'Rotterdam ma 21 jan + zo 17 feb',
      subtitle: 'Inschrijven voor de cursus',
    },
    addParticipant: 'ADD PARTiCIPANT',
    companyName: 'Bedijfsnaam',
    postcode: 'Postcode',
    city: 'Plaats',
    email: 'E-mail',
    phone: 'Telefoon',
    acceptText: 'Ik ga akkoord met de',
    acceptLinkText: 'algemene voorwaarden',
    thankTitle: 'Thank you!',
    thankText: 'You are now subscribed to this course',
    buttonText: 'INSCHRIJVEN',
    policyText:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
  },
};

<Course {...props} />;
```
