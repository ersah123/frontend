/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Modal, themableWithStyles } from 'rsk-components';
import { Title } from '../../../views';
import {
  Link,
  Button,
  Checkbox,
  Input,
  Text,
  KeuneIcon,
} from '../../../elements';

import styles from './ApplicationForm.css';

@reduxForm({ form: 'applicationForm' })
@themableWithStyles(styles)
class ApplicationForm extends PureComponent {
  state = { inputCount: 1, sent: false, isModalOpen: false };

  participantLimit = 6;

  static propTypes = {
    /**
     * Prop for anchor
     */
    anchor: PropTypes.string,
    /**
     * Prop for header
     */
    header: PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
    }),
    /**
     * Prop for companyName
     */
    companyName: PropTypes.string,
    /**
     * Prop for postcode
     */
    postcode: PropTypes.string,
    /**
     * Prop for city
     */
    city: PropTypes.string,
    /**
     * Prop for email
     */
    email: PropTypes.string,
    /**
     * Prop for phone
     */
    phone: PropTypes.string,
    /**
     * Prop for acceptText
     */
    acceptText: PropTypes.string,
    /**
     * Prop for acceptLinkText
     */
    acceptLinkText: PropTypes.string,
    /**
     * Prop for policyText
     */
    policyText: PropTypes.string,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      form: PropTypes.string,
      leftCell: PropTypes.string,
      formCell: PropTypes.string,
      formRow: PropTypes.string,
      checkbox: PropTypes.string,
      button: PropTypes.string,
      plusCell: PropTypes.string,
      plusIcon: PropTypes.string,
      rightCol: PropTypes.string,
      leftCol: PropTypes.string,
      submitMessage: PropTypes.string,
    }),
  };

  static defaultProps = {
    anchor: null,
    header: null,
    companyName: null,
    postcode: null,
    city: null,
    email: null,
    phone: null,
    acceptText: null,
    acceptLinkText: null,
    policyText: null,
    className: null,
    css: {
      root: styles.root,
      form: styles.form,
      leftCell: styles.leftCell,
      formCell: styles.formCell,
      formRow: styles.formRow,
      checkbox: styles.checkbox,
      button: styles.button,
      plusCell: styles.plusCell,
      plusIcon: styles.plusIcon,
      rightCol: styles.rightCol,
      leftCol: styles.leftCol,
      submitMessage: styles.submitMessage,
    },
  };

  addParticipant = () => {
    const { inputCount } = this.state;
    if (inputCount !== this.participantLimit) {
      this.setState({ inputCount: inputCount + 1 });
    }
  };

  handleSubmit = el => {
    el.preventDefault();
    this.setState({ sent: true });
  };

  handleOpenModal = el => {
    el.preventDefault();
    this.setState({ isModalOpen: true });
  };

  handleCloseModal = () => {
    this.setState({ isModalOpen: false });
  };

  renderNameFields = () => {
    const { inputCount } = this.state;
    return new Array(inputCount).fill().map((_, i) => {
      const key = `nameField${i}`;
      return <Input key={key} name={`name[${i}]`} placeholder="Naam" />;
    });
  };

  renderAddParticipant = () => {
    const { addParticipant, css } = this.props;
    const { inputCount } = this.state;
    if (inputCount < this.participantLimit) {
      return (
        <div className={css.plusCell} onClick={this.addParticipant}>
          <Text variant="subheading">{addParticipant}</Text>
          <KeuneIcon name="open" className={css.plusIcon} />
        </div>
      );
    }
    return null;
  };

  renderAction = () => {
    const { thankTitle, thankText, css, buttonText } = this.props;
    const { sent } = this.state;

    if (sent) {
      return (
        <div className={css.submitMessage}>
          <Text variant="headline">{thankTitle}</Text>
          <Text variant="body1">{thankText}</Text>
        </div>
      );
    }
    return (
      <Button
        theme="tertiary"
        className={css.button}
        onClick={this.handleSubmit}
      >
        {buttonText}
      </Button>
    );
  };

  render() {
    const {
      anchor,
      header,
      companyName,
      postcode,
      city,
      email,
      phone,
      acceptText,
      acceptLinkText,
      policyText,
      className: classNameProp,
      css,
    } = this.props;

    const { isModalOpen } = this.state;

    const className = classNames(css.root, classNameProp);
    return (
      <div key={`form-${anchor}`} className={className}>
        <Title {...header} />
        <div className={css.form}>
          <div
            key={`form-left-${anchor}`}
            className={classNames(css.formCell, css.leftCell)}
          >
            {this.renderNameFields()}
            {this.renderAddParticipant()}
          </div>
          <div key={`form-right-${anchor}`} className={css.formCell}>
            <Input
              key={`company_name-${anchor}`}
              name="company_name"
              placeholder={companyName}
            />
            <div className={css.formRow}>
              <Input
                key={`postcode-${anchor}`}
                name="postcode"
                placeholder={postcode}
                className={css.leftCol}
              />
              <Input
                key={`plaats-${anchor}`}
                name="plaats"
                placeholder={city}
                className={css.rightCol}
              />
            </div>
            <Input key={`email-${anchor}`} name="email" placeholder={email} />
            <Input
              key={`telefoon-${anchor}`}
              name="telefoon"
              placeholder={phone}
            />
            <div className={css.checkbox}>
              <Checkbox key={`akkoord-${anchor}`} name="akkoord" label="" />
              <Text variant="body1">
                {acceptText}{' '}
                <Link to="/" onClick={this.handleOpenModal}>
                  {acceptLinkText}
                </Link>
              </Text>
            </div>
            {this.renderAction()}
            {isModalOpen ? (
              <Modal onClose={this.handleCloseModal}>{policyText}</Modal>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default ApplicationForm;
