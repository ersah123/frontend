import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { SalonFinderModal } from '..';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import styles from './VisitLocation.css';
import { KeuneIcon, Text } from '../../elements';

/**
 * Social links on Footer
 *
 * @class VisitLocation
 * @extends {PureComponent}
 */
@themableWithStyles(styles)
class VisitLocation extends PureComponent {
  static propTypes = {
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    css: {
      root: styles.root,
    },
  };

  state = { isOpen: false };

  handleOpen = () => this.setState({ isOpen: true });

  handleClose = () => this.setState({ isOpen: false });

  render() {
    const {
      css,
      // The rest values
      ...rest
    } = this.props;

    const { isOpen } = this.state;

    return (
      <div className={classNames(css.root)} {...rest}>
        <Text
          variant="subheading"
          className={classNames(styles.link)}
          onClick={this.handleOpen}
        >
          <span>Visit a Keune Salon</span>
          <KeuneIcon name="location" className={classNames(styles.icon)} />
        </Text>
        {isOpen ? <SalonFinderModal handleClose={this.handleClose} /> : null}
      </div>
    );
  }
}

export default VisitLocation;
