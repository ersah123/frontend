import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import valFunc from '../../globals/validation';

import { Input, FormRow, Select } from '../../elements';

class PersonalInfo extends PureComponent {
  static propTypes = {
    hidePhone: PropTypes.bool,
    onChange: PropTypes.func,
    /**
     * Component is or isn't stacked
     */
    isStacked: PropTypes.bool,
  };

  static defaultProps = {
    hidePhone: false,
    onChange: () => {},
    isStacked: false,
  };

  render() {
    const { hidePhone, onChange, isStacked } = this.props;

    return (
      <div>
        <FormRow isStacked={isStacked}>
          <Select
            name="gender"
            label="Aanhef"
            emptyOption="Kies aanhef"
            options={[
              { key: 'M', value: 'Dhr.' },
              { key: 'V', value: 'Mevr.' },
            ]}
            required
            validate={[valFunc.required]}
            onChange={onChange}
          />
        </FormRow>
        <FormRow isStacked={isStacked}>
          <Input
            name="initials"
            label="Voorletter(s)"
            type="text"
            required
            validate={[valFunc.required]}
            onChange={onChange}
          />
          <Input
            name="lastName"
            label="Achternaam"
            type="text"
            required
            validate={[valFunc.required]}
            onChange={onChange}
          />
        </FormRow>
        <FormRow isStacked={isStacked}>
          <Input
            name="email"
            label="Email"
            type="email"
            required
            validate={[valFunc.required, valFunc.email]}
            onChange={onChange}
          />
        </FormRow>
        {!hidePhone && (
          <FormRow isStacked={isStacked}>
            <Input
              name="phone.home"
              label="Telefoonnummer"
              type="phone"
              required
              validate={[valFunc.required, valFunc.phoneNumber]}
              onChange={onChange}
            />
          </FormRow>
        )}
      </div>
    );
  }
}

export default PersonalInfo;
