Basic PersonalInfo component

```js
const { reduxForm } = require('redux-form');

const WrappedPersonalInfo = reduxForm({
  form: 'simple', // a unique identifier for this form
})(PersonalInfo);

<WrappedPersonalInfo />;
```
