```js
const bigImage = require('../../../views/ProductTile/assets/bigger-bottle-example.png');
const product = require('../fixtures/product.json');

const productData = {
  ...product,
  media: [
    {
      ...product.media[0],
    },
  ],
};
productData.media[0].md.src = bigImage;

<BigCarouselTile product={productData} theme="light" />;
```
