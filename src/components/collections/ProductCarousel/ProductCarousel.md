Basic ProductCarousel component

```js
const product = require('./fixtures/product.json');
const image = require('../../views/ProductTile/assets/model.jpg');

const productWithImg = {
  ...product,
  media: [
    {
      ...product.media[0],
      src: image,
    },
  ],
};

const products = [];
const itemCount = 10;

for (let i = 0; i < itemCount; i++) {
  products.push(productWithImg);
}

const props = {
  title: 'See other Refresh products',
  text:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla rutrum erat sed imperdiet feugiat. Sed tincidunt nibh eget orci finibus, vel porttitor libero consequat. Duis malesuada, lacus eget molestie ornare, leo purus aliquet augue, ac varius augue ipsum ut diam.',
  hasBigTiles: false,
  buttonText: 'LOREM IPSUM GALOR',
  buttonUrl: '/go',
  theme: 'light',
  products,
};

<ProductCarousel {...props} />;
```
