import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Swiper, themableWithStyles } from 'rsk-components';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button, KeuneIcon, Text } from '../../elements';

import BigCarouselTile from './BigCarouselTile/BigCarouselTile';
import CarouselTile from './CarouselTile/CarouselTile';
import { capitalize } from '../../../utils/helperFunc';
import styles from './ProductCarousel.css';

import * as productActions from '../../../actions/product.action';
import { getProductsByFilter } from '../../../reducers/product.reducer';

const mapStateToProps = ({ product }) => ({
  products: product.products,
  loading: product.loading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(productActions, dispatch);

// TODO: Separate this into to component: ProductCarouselContent, ProductCarouselSwiper
@connect(
  mapStateToProps,
  mapDispatchToProps,
)
@themableWithStyles(styles)
class ProductCarousel extends PureComponent {
  static propTypes = {
    /**
     * Product carousel title
     */
    title: PropTypes.string,
    /**
     * Carousel text
     */
    text: PropTypes.string,
    /**
     * Button text
     */
    buttonText: PropTypes.string,
    /**
     * Button link or url
     */
    buttonUrl: PropTypes.string,
    /**
     * List with products.
     */
    products: PropTypes.shape().isRequired,
    /**
     * Gets products from state (or loads them).
     */
    getProducts: PropTypes.func.isRequired,
    /**
     * Filter by brand, category etc.
     */
    filters: PropTypes.shape(),
    /**
     * The big tile carousel or small tile carousel
     */
    hasBigTiles: PropTypes.bool,
    /**
     * The theme for carousel
     */
    theme: PropTypes.oneOf(['light', 'dark']),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      contentWrapper: PropTypes.string,
      swiperWrapper: PropTypes.string,
      ctaButton: PropTypes.string,
      hasBigTiles: PropTypes.string,
      prevButton: PropTypes.string,
      nextButton: PropTypes.string,
      prevWrapperClass: PropTypes.string,
      nextWrapperClass: PropTypes.string,
      contentText: PropTypes.string,
      mobileCTAWrapper: PropTypes.string,
      themeDark: PropTypes.string,
      themeLight: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    buttonText: null,
    buttonUrl: null,
    title: '',
    text: '',
    filters: null,
    hasBigTiles: false,
    theme: 'dark', // default, does nothing
    css: {
      root: styles.root,
      contentWrapper: styles.contentWrapper,
      swiperWrapper: styles.swiperWrapper,
      ctaButton: styles.ctaButton,
      prevButton: styles.prevButton,
      nextButton: styles.nextButton,
      prevWrapperClass: styles.prevWrapperClass,
      nextWrapperClass: styles.nextWrapperClass,
      contentText: styles.contentText,
      mobileCTAWrapper: styles.mobileCTAWrapper,
      themeDark: styles.themeDark,
      themeLight: styles.themeLight,
    },
  };

  state = {
    products: this.props.products, // eslint-disable-line react/destructuring-assignment
  };

  componentDidMount() {
    const { getProducts, filters } = this.props;

    if (filters) getProducts(filters);
    else getProducts({});
  }

  static getDerivedStateFromProps(nextProps) {
    const { products, filters } = nextProps;
    let results = [];
    if (filters) {
      results = getProductsByFilter(products, filters);
    }

    return {
      products: results,
    };
  }

  swiperParams = () => ({
    slidesPerView: 4,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
    },
  });

  renderControlButton = (wrapperClass, buttonClass) => (
    <div className={wrapperClass}>
      <Button className={buttonClass} theme="default">
        <KeuneIcon name="arrow" />
      </Button>
    </div>
  );

  renderCTA = (buttonText, buttonUrl, buttonClass) =>
    buttonText &&
    buttonUrl && (
      <Button
        theme="default"
        iconAfter="arrow"
        to={buttonUrl}
        className={buttonClass}
      >
        {buttonText}
      </Button>
    );

  renderTiles = products => {
    const { hasBigTiles, theme } = this.props;
    if (hasBigTiles) {
      return products.map(product => (
        <BigCarouselTile key={product.id} product={product} theme={theme} />
      ));
    }
    return products.map(product => (
      <CarouselTile key={product.id} product={product} theme={theme} />
    ));
  };

  render() {
    const {
      className: classNameProp,
      css,
      title,
      text,
      buttonText,
      buttonUrl,
      theme,
      filters,
    } = this.props;
    const { products } = this.state;
    const filterStr = JSON.stringify(filters);

    const className = classNames(
      css.root,
      classNameProp,
      css[`theme${capitalize(theme)}`],
    );

    return (
      <div className={className}>
        <section className={css.contentWrapper} title={filterStr}>
          <Text variant="display3" gutterBottom>
            {title}
          </Text>
          <div className={css.contentText}>
            <Text variant="body1" gutterBottom>
              {text}
            </Text>
          </div>
          {this.renderCTA(buttonText, buttonUrl, css.ctaButton)}
        </section>
        {products && (
          <div className={css.swiperWrapper}>
            <Swiper
              showNavigation
              prevComponent={this.renderControlButton(
                css.prevWrapperClass,
                css.prevButton,
              )}
              nextComponent={this.renderControlButton(
                css.nextWrapperClass,
                css.nextButton,
              )}
              swiperParams={this.swiperParams()}
            >
              {this.renderTiles(products)}
            </Swiper>
          </div>
        )}
        <div className={css.mobileCTAWrapper}>
          {this.renderCTA(buttonText, buttonUrl, css.ctaButton)}
        </div>
      </div>
    );
  }
}

export default ProductCarousel;
