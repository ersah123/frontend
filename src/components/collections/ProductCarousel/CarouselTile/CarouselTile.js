import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import ProductTile from '../../../views/ProductTile/ProductTile';
import { capitalize } from '../../../../utils/helperFunc';
import styles from './CarouselTile.css';

@themableWithStyles(styles)
class CarouselTile extends PureComponent {
  static propTypes = {
    /**
     * The tile theme
     */
    theme: PropTypes.oneOf(['light', 'dark']),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      themeDark: PropTypes.string,
      themeLight: PropTypes.string,
    }),
  };

  static defaultProps = {
    className: null,
    theme: 'dark',
    css: {
      root: styles.root,
      themeDark: styles.themeDark,
      themeLight: styles.themeLight,
    },
  };

  render() {
    const {
      className: classNameProp,
      css,
      theme,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(
      css.root,
      classNameProp,
      css[`theme${capitalize(theme)}`],
    );

    return (
      <div className={className}>
        <ProductTile {...rest} />
      </div>
    );
  }
}

export default CarouselTile;
