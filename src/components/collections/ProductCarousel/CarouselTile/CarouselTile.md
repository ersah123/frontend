```js
const smallImage = require('../../../views/ProductTile/assets/model.jpg');
const product = require('../fixtures/product.json');

const productData = {
  ...product,
  media: [
    {
      ...product.media[0],
    },
  ],
};
productData.media[0].md.src = smallImage;

<CarouselTile product={productData} theme="dark" />;
```
