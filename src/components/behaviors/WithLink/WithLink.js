import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';
import { Link } from '../../elements';

import styles from './WithLink.css';

@themableWithStyles(styles)
class WithLink extends PureComponent {
  static propTypes = {
    /** The link of the page where you linking to. */
    to: PropTypes.string,
    /** If the link should be handled as a history.push event or not. */
    isExternal: PropTypes.bool,
    /** If the link should be opened in a new window. */
    openNewWindow: PropTypes.bool,
    /** Function that will be fired on click. */
    onClick: PropTypes.func,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The content of the component.
     */
    children: PropTypes.node,
  };

  static defaultProps = {
    to: null,
    isExternal: false,
    openNewWindow: false,
    onClick: null,
    className: null,
    children: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const {
      to,
      className: classNameProp,
      children,
      css,
      // The rest values
      ...rest
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return to ? (
      <Link to={to} className={className} {...rest}>
        {children}
      </Link>
    ) : (
      children
    );
  }
}

export default WithLink;
