Basic WithLink component

Wrap any component with Link component.

```js
const props = {
  link: '/go',
};

<WithLink {...props}>
  <Text>Test</Text>
</WithLink>;
```
