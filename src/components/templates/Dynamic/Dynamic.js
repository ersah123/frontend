import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { themableWithStyles } from 'rsk-components';

import RenderComponent from 'components/globals/RenderComponent';

import styles from './Dynamic.css';

@themableWithStyles(styles)
class Dynamic extends PureComponent {
  static propTypes = {
    componentData: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  render() {
    const { componentData } = this.props;
    return (
      <main className={styles.root}>
        {componentData.map((comp, i, fullList) => {
          if (!comp) return null;
          return (
            <RenderComponent
              component={comp}
              rootListIndex={i}
              rootList={fullList}
              key={comp.id}
            />
          );
        })}
      </main>
    );
  }
}

export default Dynamic;
