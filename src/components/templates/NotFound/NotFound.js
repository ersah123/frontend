import React, { PureComponent } from 'react';
import { themableWithStyles } from 'rsk-components';

import styles from './NotFound.css';

import { Text, Button } from '../../elements';

@themableWithStyles(styles)
class NotFound extends PureComponent {
  render() {
    return (
      <main className={styles.root}>
        <div className={styles.container}>
          <Text variant="display3">Page not found</Text>
          <Text variant="display1">
            Sorry, the page you were trying to view does not exist.
          </Text>
          <br />
          <Button iconAfter="chevron-right" to="/">
            HOMEPAGE
          </Button>
        </div>
      </main>
    );
  }
}

export default NotFound;
