import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { themableWithStyles } from 'rsk-components';

import RenderComponent from 'components/globals/RenderComponent';
import { Form } from '../../collections';
import styles from './FormExample.css';

@themableWithStyles(styles)
class FormExample extends PureComponent {
  static propTypes = {
    componentData: PropTypes.arrayOf(PropTypes.object).isRequired,
    templateData: PropTypes.shape({
      subnav: PropTypes.object.isRequired,
    }).isRequired,
  };

  render() {
    const {
      componentData,
      templateData: { subnav },
    } = this.props;

    return (
      <main className={styles.root}>
        {subnav && (
          <div className={classNames(styles.subnav)}>
            <RenderComponent component={subnav} />
          </div>
        )}
        <div className={styles.form}>
          <Form />
        </div>
        <div className={styles.container}>
          {componentData.map((comp, i, fullList) => (
            <RenderComponent
              component={comp}
              rootListIndex={i}
              rootList={fullList}
              key={comp.id}
            />
          ))}
        </div>
      </main>
    );
  }
}

export default FormExample;
