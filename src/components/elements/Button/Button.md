Sizable Button

```js
<div className="sg__buttons">
  <Button size="small">Small button</Button>
  <Button size="normal">Normal button</Button>
  <Button size="large">Large button</Button>
</div>
```

It has different types of themes. You can change themes by changing `theme` prop.

```js
<div className="sg__buttons">
  <Button theme="default">Default</Button>
  <Button theme="primary">Primary</Button>
  <Button theme="secondary">Secondary</Button>
  <Button theme="tertiary">Tertiary</Button>
</div>
```

Outlined Button:

```js
<div className="sg__buttons">
  <Button theme="default" view="outline">
    Default
  </Button>
  <Button theme="primary" view="outline">
    Primary
  </Button>
  <Button theme="secondary" view="outline">
    Secondary
  </Button>
</div>
```

Link Button:

```js
<div className="sg__buttons">
  <Button theme="default" view="link">
    Default
  </Button>
  <Button theme="primary" view="link">
    Primary
  </Button>
  <Button theme="secondary" view="link">
    Secondary
  </Button>
  <Button theme="tertiary" view="link">
    Tertiary
  </Button>
</div>
```

Wide Button:

```js
<div className="sg__buttons">
  <Button theme="primary" size="small" wide>
    Small
  </Button>
  <Button theme="primary" wide>
    Default
  </Button>
  <Button theme="primary" size="large" wide>
    Large
  </Button>
  <Button theme="primary" disabled wide>
    Disabled
  </Button>
</div>
```

Different sized buttons with icons:

```js
<div className="sg__buttons">
  <Button iconAfter="arrow">Go there</Button>
  <Button iconBefore="location">You're here</Button>
</div>

<div className="sg__buttons">
  <Button iconAfter="arrow" size="small">Lorem ipsum dolor sit</Button>
  <Button iconAfter="arrow" size="normal">Lorem ipsum dolor sit</Button>
  <Button iconAfter="arrow" size="large">Lorem ipsum dolor sit</Button>
</div>
```

Different sized buttons with theme primary and an arrow icon:

```js
<div className="sg__buttons">
  <Button iconAfter="arrow" theme="primary" size="small">
    Lorem ipsum dolor sit
  </Button>
  <Button iconAfter="arrow" theme="primary" size="normal">
    Lorem ipsum dolor sit
  </Button>
  <Button iconAfter="arrow" theme="primary" size="large">
    Lorem ipsum dolor sit
  </Button>
</div>
```

Loading Button:

```js
<Button theme="primary" loading>
  Loading
</Button>
```

Rounded Button:

```js
<Button theme="primary" rounded>
  Rounded
</Button>
```
