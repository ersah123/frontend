/* eslint-disable css-modules/no-unused-class */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Button as RskButton } from 'rsk-components';
import { KeuneIcon as Icon, Link } from '..';

import styles from './Button.theme.css';

class Button extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The children of the component.
     */
    children: PropTypes.node,
    /**
     * The text of the button.
     */
    text: PropTypes.string,
    /**
     * If `true`, the button will be disabled.
     */
    disabled: PropTypes.bool,
    /**
     * If `true`, the button will be the full width.
     */
    wide: PropTypes.bool,
    /**
     * If `true`, the button will have round corners.
     */
    rounded: PropTypes.bool,
    /**
     * If `true`, the button will be in loading status.
     */
    loading: PropTypes.bool,
    /**
     * The button sizes.
     */
    size: PropTypes.oneOf(['small', 'normal', 'large']),
    /**
     * The different button themes.
     */
    theme: PropTypes.oneOf(['default', 'primary', 'secondary', 'tertiary']),
    /**
     * The different button views.
     */
    view: PropTypes.oneOf(['button', 'link', 'outline']),
    /**
     * The icon to be shown before see Icon component
     */
    iconBefore: PropTypes.string,
    /**
     * The icon to be shown after the text see Icon component
     */
    iconAfter: PropTypes.string,
  };

  static defaultProps = {
    className: null,
    children: null,
    theme: 'default',
    text: null,
    view: 'button',
    size: 'normal',
    wide: false,
    rounded: false,
    loading: false,
    disabled: false,
    iconBefore: null,
    iconAfter: null,
  };

  render() {
    const { children, text, iconBefore, iconAfter, ...rest } = this.props;

    let component = 'button';

    if (rest.to) {
      component = Link;
    } else if (rest.href) {
      component = 'a';
    }

    return (
      <RskButton {...rest} component={component} css={styles}>
        {iconBefore && <Icon name={iconBefore} size="small" />}
        <span>{text || children}</span>
        {iconAfter && <Icon name={iconAfter} size="small" />}
      </RskButton>
    );
  }
}

export default Button;
