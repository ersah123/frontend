Basic Checkbox component

```js
const { reduxForm } = require('redux-form');

const WrappedCheckbox = reduxForm({
  form: 'simple',
})(Checkbox);

<WrappedCheckbox label="I am checkboxxed" />;
```
