import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Checkbox as RSKCheckbox, themableWithStyles } from 'rsk-components';

import styles from './Checkbox.css';

@themableWithStyles(styles)
class Checkbox extends PureComponent {
  static propTypes = {
    /**
     * Prop for name
     */
    name: PropTypes.string,
    /**
     * Prop for label
     */
    label: PropTypes.string,
    /**
     * Prop for disabled
     */
    disabled: PropTypes.bool,
    /**
     * Prop for checked
     */
    checked: PropTypes.bool,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
  };

  static defaultProps = {
    name: null,
    label: null,
    disabled: false,
    checked: false,
    className: null,
    css: {
      root: styles.root,
    },
  };

  render() {
    const {
      name,
      label,
      disabled,
      checked,
      className: classNameProp,
      css,
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <RSKCheckbox
        className={className}
        name={name}
        label={label}
        disabled={disabled}
        checked={checked}
      />
    );
  }
}

export default Checkbox;
