import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Icon, themableWithStyles, SvgIcon } from 'rsk-components';

// these have viewBox="0 0 160 128"
import iconList from './assets/keune-icons.json';
import styles from './KeuneIcon.css';
/**
 * KEUNE icons differ from the 'normal' `<Icon />`
 * and have a viewBox of '0 0 160 128'.
 *
 * @class KeuneIcon
 * @extends {PureComponent}
 */
@themableWithStyles(styles)
class KeuneIcon extends PureComponent {
  static propTypes = {
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
    }),
    svgIconList: PropTypes.objectOf(PropTypes.string), // Required only because SVG Icon is the only option at this moment. When, for instance, Font Icon is supported this option will be depending on the type of icon.
    name: PropTypes.string.isRequired,
    size: PropTypes.string,
    /**
     * Title attribute of this icon (in stead of name)
     */
    title: PropTypes.string,
  };

  static defaultProps = {
    className: null,
    css: {
      root: styles.root,
      small: styles.small,
    },
    svgIconList: iconList,
    size: null,
    title: null,
  };

  render() {
    const {
      className: classNameProp,
      css,
      name,
      size,
      title,
      svgIconList,
      ...rest
    } = this.props;

    const svgIcon = svgIconList[name];
    if (typeof svgIcon === 'undefined') {
      return <Icon name={name} />;
    }

    const className = classNames(css.root, classNameProp, css[size]);
    const viewBox = '0 0 160 128';

    return (
      <i className={className} title={title || name} {...rest}>
        <SvgIcon path={svgIcon} viewBox={viewBox} />
      </i>
    );
  }
}

KeuneIcon.list = Object.keys(iconList);

export default KeuneIcon;
