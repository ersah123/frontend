Keune Icons

```js
const styles = {
  ul: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: 0,
    margin: 0,
    fontSize: '3rem',
  },
  li: {
    boxSizing: 'border-box',
    width: '20%',
    padding: '1rem',
    textAlign: 'center',
  },
  name: {
    display: 'block',
    fontSize: '1.6rem',
    fontFamily: 'sans-serif',
  },
  html: {
    display: 'block',
  },
};

const svgIconList = KeuneIcon.list;

<ul style={styles.ul}>
  {svgIconList.map((name, index) => {
    return (
      <li key={index} style={styles.li}>
        <span style={styles.html}>
          <KeuneIcon name={name} />
        </span>
        <span style={styles.name}>{name}</span>
      </li>
    );
  })}
</ul>;
```
