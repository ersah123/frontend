/* eslint-env jest */

import React from 'react';
import { render } from 'enzyme';
import toJson from 'enzyme-to-json';

import KeuneIcon from './KeuneIcon';
import iconList from './assets/keune-icons';

describe('<KeuneIcon />', () => {
  const props = {
    name: KeuneIcon.list[0],
    svgIconList: iconList,
  };

  const wrapper = render(<KeuneIcon {...props} />);

  describe('is rendering', () => {
    it('title attribute', () => {
      expect(wrapper.prop('title')).toBe(KeuneIcon.list[0]);
    });

    it('svg path', () => {
      expect(wrapper.find('path').prop('d')).toBe(iconList[KeuneIcon.list[0]]);
    });

    it('correctly', () => {
      expect(toJson(wrapper)).toMatchSnapshot();
    });
  });
});
