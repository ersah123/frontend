import React, { Component } from 'react';
import PropTypes from 'prop-types';
import history from '../../../history';

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

/**
 * Link component for interal linking with the router.
 * If baseUrl is set (in config.js) all links are prefixed with the baseUrl
 */
class Link extends Component {
  static propTypes = {
    /** The link of the page where you linking to. */
    to: PropTypes.string.isRequired,
    /** If the link should be handled as a history.push event or not. */
    isExternal: PropTypes.bool,
    /** If the link should be opened in a new window. */
    openNewWindow: PropTypes.bool,
    /** The inner childs. */
    children: PropTypes.node,
    /** Function that will be fired on click. */
    onClick: PropTypes.func,
  };

  static contextTypes = {
    baseUrl: PropTypes.string.isRequired,
  };

  static defaultProps = {
    children: null,
    isExternal: false,
    openNewWindow: false,
    onClick: null,
  };

  handleClick = event => {
    const { baseUrl } = this.context;
    const { to, onClick, isExternal, openNewWindow } = this.props;

    if (isExternal || openNewWindow) {
      return;
    }

    if (onClick) {
      onClick(event);
    }

    if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();
    if (baseUrl) {
      history.push(`${baseUrl}${to}`);
    } else {
      history.push(to);
    }
  };

  render() {
    const { baseUrl } = this.context;
    const { children, to, isExternal, openNewWindow, ...props } = this.props;

    let url = to;
    if (baseUrl) {
      url = `${baseUrl}${url}`;
    }

    if (isExternal || openNewWindow) {
      url = `${to}`;
      if (openNewWindow) props.target = '_blank';
      props.rel = 'noopener';
    }

    return (
      <a href={url} {...props} onClick={this.handleClick}>
        {children}
      </a>
    );
  }
}

export default Link;
