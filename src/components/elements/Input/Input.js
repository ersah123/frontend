import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Input as RSKInput, themableWithStyles } from 'rsk-components';

import styles from './Input.css';

@reduxForm({
  form: 'input',
})
@themableWithStyles(styles)
class Input extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * CSS API override all the class names injected
     */
    css: PropTypes.shape({
      root: PropTypes.string,
      input: PropTypes.string,
    }),
  };

  static defaultProps = {
    name: null,
    label: '',
    placeholder: '',
    value: '',
    className: null,
    css: {
      root: styles.root,
      input: styles.input,
    },
  };

  render() {
    const {
      name,
      placeholder,
      label,
      value,
      className: classNameProp,
      css,
    } = this.props;

    const className = classNames(css.root, classNameProp);
    return (
      <div className={className}>
        <RSKInput
          className={css.input}
          type="text"
          name={name}
          label={label}
          placeholder={placeholder}
          value={value}
        />
      </div>
    );
  }
}

export default Input;
