KEUNE text styles

```js
const s = {
  width: '100%',
  maxWidth: 960,
  padding: 40,
  background: '#f9fafb',
};

<div style={s}>
  <Text variant="display3" gutterBottom>
    Product Title - 26 PT Futura PT - Medium Line Spacing 30 : display3
  </Text>
  <Text variant="display2" gutterBottom>
    Title over Category Image - 34 PT Futura PT - Medium Line Spacing 38 :
    display2
  </Text>
  <Text variant="display1" gutterBottom>
    Title General - 40 PT Futura PT - Medium Line Spacing 42 : display1
  </Text>
  <Text variant="headline" gutterBottom>
    Subtitle - 24 PT BentonModDisp - RegularItalic : headline
  </Text>
  <Text variant="subheading" gutterBottom>
    Subtitle - 16 PT Futura PT - Medium : subheading
  </Text>
  <Text variant="title" gutterBottom>
    Title : title
  </Text>
  <Text variant="body2" gutterBottom>
    Body Text - 20 PT Futura PT Book Line Spacing 30 : body2
  </Text>
  <Text variant="body1" gutterBottom>
    Body Text Product pages - 16 PT Futura PT - Book Line Spacing 22 : body1
  </Text>
  <Text variant="caption" gutterBottom>
    Caption : caption
  </Text>
  <Text variant="body2" gutterBottom noWrap>
    {`
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    `}
  </Text>
  <Text variant="body1" gutterBottom>
    {`
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    `}
  </Text>
</div>;
```

```js
const demoBg = require('./fixtures/demo-bg.png');

const s = {
  width: '100%',
  maxWidth: 500,
  backgroundColor: 'transparent',
  backgroundImage: `url(${demoBg})`,
};

<div style={s}>
  <Text variant="display3" gutterBottom>
    Display 3
  </Text>
  <Text variant="display2" gutterBottom>
    Display 2
  </Text>
  <Text variant="display1" gutterBottom>
    Display 1
  </Text>
  <Text variant="headline" gutterBottom>
    Headline
  </Text>
  <Text variant="subheading" gutterBottom>
    Subheading
  </Text>
  <Text variant="title" gutterBottom>
    Title
  </Text>
  <Text variant="body2" gutterBottom>
    Body 2
  </Text>
  <Text variant="body1" gutterBottom align="right">
    Body 1
  </Text>
  <Text variant="caption" gutterBottom align="center">
    Caption
  </Text>
  <Text variant="body1" gutterBottom noWrap>
    {`
    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    `}
  </Text>
</div>;
```
