/* eslint-disable css-modules/no-unused-class */
import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { Text as RskText } from 'rsk-components';
import styles from './Text.theme.css';

class Text extends PureComponent {
  static propTypes = {
    /**
     * This will overwrite the children property. Try not to use this other than CMS implementation.
     */
    text: PropTypes.string,
    /**
     * Set the text-align on the component.
     */
    align: PropTypes.oneOf(['inherit', 'left', 'center', 'right', 'justify']),
    /**
     * The content of the component.
     */
    children: PropTypes.node,
    /**
     * @ignore
     */
    className: PropTypes.string,
    /**
     * The component used for the root node.
     * Either a string to use a DOM element or a component.
     * By default, it maps the variant to a good default headline component.
     */
    component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    /**
     * If `true`, the text will have a bottom margin.
     */
    gutterBottom: PropTypes.bool,
    /**
     * We are empirically mapping the variant property to a range of different DOM element types.
     * For instance, h1 to h6. If you wish to change that mapping, you can provide your own.
     * Alternatively, you can use the `component` property.
     */
    headlineMapping: PropTypes.shape({
      display3: PropTypes.string,
      display2: PropTypes.string,
      display1: PropTypes.string,
      headline: PropTypes.string,
      title: PropTypes.string,
      subheading: PropTypes.string,
      caption: PropTypes.string,
      body2: PropTypes.string,
      body1: PropTypes.string,
    }),
    /**
     * If `true`, the text will not wrap, but instead will truncate with an ellipsis.
     */
    noWrap: PropTypes.bool,
    /**
     * If `true`, the text will have a bottom margin.
     */
    paragraph: PropTypes.bool,
    /**
     * Applies the theme typography styles.
     */
    variant: PropTypes.oneOf([
      'display3',
      'display2',
      'display1',
      'headline',
      'title',
      'subheading',
      'body2',
      'body1',
      'caption',
    ]),
  };

  static defaultProps = {
    text: '',
    align: 'inherit',
    className: '',
    children: null,
    component: null,
    gutterBottom: false,
    headlineMapping: {
      display3: 'h3',
      display2: 'h2',
      display1: 'h1',
      headline: 'strong',
      title: 'h4',
      subheading: 'strong',
      caption: 'caption',
      body2: 'p',
      body1: 'p',
    },
    noWrap: false,
    paragraph: false,
    variant: 'title',
  };

  render() {
    const { text, children, ...rest } = this.props;

    return (
      <RskText {...rest} css={styles}>
        {text || children}
      </RskText>
    );
  }
}

export default Text;
