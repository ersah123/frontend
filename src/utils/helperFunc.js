// @flow
import { width } from 'react-sizes';

export function capitalize(string) {
  if (typeof string !== 'string') {
    throw new Error('capitalize(string) expects a string argument.');
  }
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function capitalizeOnDev(string) {
  if (process.env.NODE_ENV !== 'production') {
    throw new Error('should be on development environment');
  }
  return capitalize(string);
}

export function contains(obj: Object, pred: Object) {
  return Object.keys(pred).every(
    key => ({}.hasOwnProperty.call(obj, key) && obj[key] === pred[key]),
  );
}

export function findIndex(arr: Array<any>, pred: any) {
  const predType = typeof pred;
  for (let i = 0; i < arr.length; i += 1) {
    if (predType === 'function' && !!pred(arr[i], i, arr) === true) {
      return i;
    }
    if (predType === 'object' && contains(arr[i], pred)) {
      return i;
    }
    if (['string', 'number', 'boolean'].indexOf(predType) !== -1) {
      return arr.indexOf(pred);
    }
  }
  return -1;
}

export function goTo(url) {
  window.open(url);
}

export function find(arr: Array<any>, pred: any) {
  const index = findIndex(arr, pred);
  return index > -1 ? arr[index] : undefined;
}

/**
 * Calculates current number of pixels from rem,
 * multiplied by 10 (default) or current factor.
 *
 * @export
 * @param {*} str a rem value like '17.1rem'
 * @returns {Number} corresponding pixels
 */
export function remToPixels(str) {
  const rem = parseFloat(str);
  const isXXL = width > 1920;
  const pixels = isXXL ? Math.ceil(rem * 12) : Math.ceil(rem * 10);
  return pixels;
}

export function prepareCookies(cookies) {
  const tmp = [];
  cookies.details.forEach(cookie => {
    if (!tmp[cookie.level]) {
      tmp[cookie.level] = [];
    }
    tmp[cookie.level].push(cookie);
  });

  const result = [];
  Object.keys(tmp).forEach(cookieSection => {
    const checked = cookieSection === 'Necessary';
    const disabled = cookieSection === 'Necessary';
    result.push({
      title: cookieSection,
      checked,
      disabled,
      details: tmp[cookieSection],
    });
  });

  return result;
}

export function selectCookies(cookies) {
  const formCookies = { ...cookies, Necessary: true };
  return Object.keys(formCookies)
    .filter(key => formCookies[key])
    .join(',');
}
