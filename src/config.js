/* eslint-disable max-len */

if (process.env.BROWSER) {
  throw new Error(
    'Do not import `config.js` from inside the client-side code.',
  );
}

module.exports = {
  locales: ['nl-NL', 'en-US'],
  // Node.js app
  port: process.env.PORT || 3000,

  // https://expressjs.com/en/guide/behind-proxies.html
  trustProxy:
    process.env.TRUST_PROXY !== 'true'
      ? process.env.TRUST_PROXY
      : true || 'loopback',

  // API Gateway
  api: {
    // local: !process.env.API_SERVER_URL, // default false (or nonexistend)
    // API URL to be used in the fetch code
    url:
      process.env.API_SERVER_URL ||
      'https://keune-backend-test.azurewebsites.net',
    // `http://localhost:${process.env.PORT || 3000}`,
    // API server key
    key: process.env.API_SERVER_KEY || 'amjUhLnK1UhWBamWQqLrS7seZCHl1aYj',
  },

  // fill these reducers in server.js because of SSI
  runtimeReducers: [
    {
      name: 'siteInit',
      url: `/api/application`,
    },
    {
      name: 'mainNavigation',
      url: '/api/navigation?url=/',
    },
    // {
    //   name: 'messages',
    //   url: '/translations',
    // },
  ],
};
