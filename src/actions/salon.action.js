/* eslint-disable import/prefer-default-export */

import {
  FETCH_SALONS_FULFILLED,
  FETCH_SALONS_PENDING,
  FETCH_SALONS_REJECTED,
} from '../constants';

export function getPlaces(lat, lng, country) {
  return async (dispatch, store, { fetch }) => {
    dispatch({ type: FETCH_SALONS_PENDING });

    try {
      const resp = await fetch(
        `/api/salon?country=${country}&lat=${lat}&lon=${lng}`,
      );

      if (resp.status === 404) return undefined;
      if (resp.status !== 200) throw new Error(resp.statusText);

      const json = await resp.json();
      return dispatch({
        type: FETCH_SALONS_FULFILLED,
        status: resp.status,
        payload: json.data,
      });
      // https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places&callback=initMap
      // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${search}&key=${apiKey}
    } catch (e) {
      console.error('ERR', e);
      const json = await e.json();
      return dispatch({
        type: FETCH_SALONS_REJECTED,
        status: e.status,
        messages: json.messages,
      });
    }
  };
}
