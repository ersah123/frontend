/* eslint-disable import/prefer-default-export */

import {
  FETCH_SEARCH_FULFILLED,
  FETCH_SEARCH_PENDING,
  FETCH_SEARCH_REJECTED,
} from '../constants';

export function search(q = '') {
  return async (dispatch, store, { fetch }) => {
    dispatch({ type: FETCH_SEARCH_PENDING });

    try {
      const resp = await fetch(`/api/search?lang=en&q=${q}`);

      console.info('INFO', resp);
      if (resp.status === 404) return undefined;
      if (resp.status !== 200) throw new Error(resp.statusText);

      const json = await resp.json();

      return dispatch({
        type: FETCH_SEARCH_FULFILLED,
        status: resp.status,
        searchTerm: q,
        payload: json.data,
      });
    } catch (e) {
      console.error('ERR', e);
      const json = await e.json();
      return dispatch({
        type: FETCH_SEARCH_REJECTED,
        status: e.status,
        messages: json.messages,
      });
    }
  };
}
