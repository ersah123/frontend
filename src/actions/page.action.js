/* eslint-disable import/prefer-default-export */
import queryString from 'query-string';

import { isLocalhost } from '../reducers/runtime.reducer';
import {
  FETCH_PAGE_FULFILLED,
  FETCH_PAGE_PENDING,
  FETCH_PAGE_REJECTED,
  RESET_PAGE,
} from '../constants';

export function getPage({ slug, query }) {
  const hasQuery = Object.keys(query).length !== 0;

  return async (dispatch, store, { fetch }) => {
    if (store().page.slug === slug) return false;

    dispatch({ type: FETCH_PAGE_PENDING, slug });
    let resp = {};

    try {
      if (isLocalhost(store().runtime)) {
        const page = slug === '/' ? 'home' : slug;
        resp = await fetch(`/api/content`, {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            path: `/pages/${page}`,
          }),
        });
      } else {
        const urlQuery = queryString.stringify({
          url: slug,
        });
        if (hasQuery) {
          // path plus query, but a bit weird: /en/styling?brand=Keune-1922
          resp = await fetch(
            `/api/content?${urlQuery}?${queryString.stringify(query)}`,
          );
        } else {
          // just path: /en/styling
          resp = await fetch(`/api/content?${urlQuery}`);
        }
      }

      console.info('INFO', resp);
      if (resp.status === 404) return undefined;
      if (resp.status !== 200) throw new Error(resp.statusText);

      const json = await resp.json();

      return dispatch({
        type: FETCH_PAGE_FULFILLED,
        status: resp.status,
        slug,
        payload: json.data,
      });
    } catch (e) {
      console.error('ERR', e);
      const json = await e.json();
      return dispatch({
        type: FETCH_PAGE_REJECTED,
        status: e.status,
        messages: json.messages,
      });
    }
  };
}

export function clearPage() {
  return dispatch => dispatch({ type: RESET_PAGE });
}
