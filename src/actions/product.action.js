/* eslint-disable import/prefer-default-export */
import queryString from 'query-string';

import {
  FETCH_PRODUCTS_FULFILLED,
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_REJECTED,
} from '../constants';
// import { getProductsByFilter } from '../reducers/product.reducer';

/**
 * Get products
 * @param {Object} filters object: { brand, category, productgroup, subline }
 * @param {String} sort by new|priceup|pricedown
 */
export function getProducts(
  { brand, category, productgroup, subline },
  max,
  sort = 'new',
) {
  return async (dispatch, getStore, { fetch }) => {
    dispatch({ type: FETCH_PRODUCTS_PENDING });

    const filters = {
      brand,
      category,
      productgroup,
      subline,
    };
    // delete the empty ones
    Object.keys(filters).forEach(
      key => filters[key] === undefined && delete filters[key],
    );
    // @TODO maybe check if already there
    // const state = getStore();
    // const { product } = state;
    // getProductsByFilter(product.products, filters);

    if (!brand && !category) {
      const err = 'Parameter brand or category missing.';
      console.error('ERROR', err);
      return dispatch({ type: FETCH_PRODUCTS_REJECTED, error: err });
    }

    // const state = getStore();
    // const lang = getLanguage(state.runtime); // should be in: state.runtime.siteInit.lang or somewhere
    const urlQuery = queryString.stringify({
      lang: 'en',
      sort,
      max,
      ...filters,
    });

    try {
      const resp = await fetch(`/api/products?${urlQuery}`);

      console.info('INFO', resp);
      if (resp.status === 404) return undefined;
      if (resp.status !== 200) throw new Error(resp.statusText);

      const json = await resp.json();

      return dispatch({
        type: FETCH_PRODUCTS_FULFILLED,
        status: resp.status,
        payload: json.data,
        filters,
      });
    } catch (e) {
      console.error('ERR', e);
      const json = await e.json();
      return dispatch({
        type: FETCH_PRODUCTS_REJECTED,
        status: e.status,
        messages: json.messages,
      });
    }
  };
}
