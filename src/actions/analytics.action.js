/* eslint-disable import/prefer-default-export */
import {
  SET_DATALAYER_DEFAULTDATA,
  UPDATE_DATALAYER_DEFAULTDATA,
  CLEAR_DATALAYER_DEFAULTDATA,
  ADD_DATALAYER_DATA,
  CLEAR_DATALAYER_DATA,
  PUSHED_TO_DATALAYER,
} from '../constants';

/**
 * SET the default dataLayer data object
 *
 * @export
 * @param {Object} payload
 * @param {String} [name]
 * @returns
 */
export function setDataLayerDefault(payload, name) {
  return async dispatch => {
    dispatch({
      type: SET_DATALAYER_DEFAULTDATA,
      name,
      payload,
    });
  };
}

/**
 * UPDATE the default dataLayer data object
 *
 * @export
 * @param {Object} payload
 * @param {String} [name]
 * @returns
 */
export function updateDataLayerDefault(payload, name) {
  return async dispatch => {
    dispatch({
      type: UPDATE_DATALAYER_DEFAULTDATA,
      name,
      payload,
    });
  };
}

/**
 * RESET the default dataLayer data object
 *
 * @export
 * @param {String} name
 * @returns
 */
export function resetDataLayerDefault(name) {
  return async dispatch => {
    dispatch({
      type: CLEAR_DATALAYER_DEFAULTDATA,
      name,
    });
  };
}

/**
 * ADD the dataLayer data object
 *
 * @export
 * @param {Object} payload
 * @param {String} [name]
 * @returns
 */
export function addDataLayerData(payload, name) {
  return async dispatch => {
    dispatch({
      type: ADD_DATALAYER_DATA,
      name,
      payload,
    });
  };
}

/**
 * RESET the dataLayer data
 *
 * @export
 * @param {String} [name]
 * @returns
 */
export function resetDataLayerData(name) {
  return async dispatch => {
    dispatch({
      type: CLEAR_DATALAYER_DATA,
      name,
    });
  };
}

/**
 * SEND all current data to the dataLayer
 *
 * @export
 * @param {String} [name]
 * @returns
 */
export function pushedToDataLayer(name) {
  return async dispatch => {
    dispatch({
      type: PUSHED_TO_DATALAYER,
      name,
    });
  };
}
