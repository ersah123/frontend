import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import nodeFetch from 'node-fetch';
import React from 'react';
import ReactDOM from 'react-dom/server';
import PrettyError from 'pretty-error';
import helmet from 'helmet';

import './serverIntlPolyfill';
import App from './components/App';
import Html from './components/Html';
import { ErrorPageWithoutStyle } from './components/templates/ErrorPage';
import errorPageStyle from './components/templates/ErrorPage/ErrorPage.css';
import createFetch from './createFetch';
import localContentAPI from './localContentAPI';
// import cmsMiddleware from './cmsMiddleware';
import router from './router';
// import assets from './asset-manifest.json'; // eslint-disable-line import/no-unresolved
import chunks from './chunk-manifest.json'; // eslint-disable-line import/no-unresolved
import configureStore from './store/configureStore';
import { setRuntimeVariable } from './actions/runtime.action';
import config from './config';
// example content
import application from '../content/application';
import navigation from '../content/navigation';
import translations from '../content/translations';

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection at:', p, 'reason:', reason);
  // send entire app down. Process manager will restart it
  process.exit(1);
});

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

const app = express();

//
// If you are using proxy from external machine, you can set TRUST_PROXY env
// Default is to trust proxy headers only from loopback interface.
// -----------------------------------------------------------------------------
app.set('trust proxy', config.trustProxy);

//
// Put on helmet
// -----------------------------------------------------------------------------
app.use(helmet());

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.resolve(__dirname, 'public'), { maxAge: '1d' }));

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//
// Register our own middleware
// -----------------------------------------------------------------------------
// app.use(cmsMiddleware);

//
// Register local content API
// -----------------------------------------------------------------------------
if (config.api.local) {
  app.use('/api', localContentAPI);
}

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
  try {
    const baseUrl = req.customBaseUrl || req.baseUrl;
    const siteInitLanguages =
      req.siteInit &&
      req.siteInit.url &&
      req.siteInit.url.options &&
      req.siteInit.url.options.languages
        ? req.siteInit.url.options.languages
        : [];
    const lang =
      (req.customParams && req.customParams.languages) ||
      siteInitLanguages[0] ||
      'en-US';
    const css = new Set();

    // Enables critical path CSS rendering
    // https://github.com/kriasoft/isomorphic-style-loader
    const insertCss = (...styles) => {
      // eslint-disable-next-line no-underscore-dangle
      styles.forEach(style => css.add(style._getCss()));
    };

    const authFromCookie = req.cookies.auth && JSON.parse(req.cookies.auth);
    const createFetchOptions = {
      apiUrl: config.api.url,
      cookie: req.headers.cookie,
      referer: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
      lang,
      auth: authFromCookie,
    };
    /* if (
      !authFromCookie ||
      new Date(authFromCookie.expires).getTime() - Date.now() < 5000
    ) {
      const authUrl = config.api.key
        ? `${config.api.url}${config.api.authPath}?apiKey=${config.api.key}`
        : `${config.api.url}${config.api.authPath}`;
      const authResp = await nodeFetch(authUrl);

      // TODO hanlde errors
      const { data: auth } = await authResp.json();

      res.cookie('auth', JSON.stringify(auth), {
        maxAge: new Date(auth.expires).getTime() - Date.now(),
        secure: !__DEV__,
      });

      createFetchOptions.auth = auth;
    } */

    // Universal HTTP client
    const fetch = createFetch(nodeFetch, createFetchOptions);

    const initialState = {
      // Initial state is empty
    };

    const store = configureStore(initialState, {
      fetch,
      // I should not use `history` on server.. but how I do redirection? follow universal-router
    });

    // runtime reducers are configured in config.js
    if (!config.api.local) {
      const getReducerNameByUrl = url => {
        for (let i = 0; i < config.runtimeReducers.length; i += 1) {
          if (url.indexOf(config.runtimeReducers[i].url) > -1) {
            return config.runtimeReducers[i].name;
          }
        }
        return null;
      };
      const responseArr = await Promise.all(
        config.runtimeReducers.map(reduc => fetch(`${reduc.url}`)),
      );
      responseArr.forEach(resp => {
        // find back the runtime reducer that got this link
        const name = getReducerNameByUrl(resp.url);
        // save our responses with our runtime reducer name
        resp.json().then(results => {
          const { data } = results;
          store.dispatch(
            setRuntimeVariable({
              name,
              value: data,
            }),
          );
        });
      });
    } else {
      // example content: get navigation, translations from import
      // eslint-disable-next-line no-lonely-if
      if (application) {
        store.dispatch(
          setRuntimeVariable({
            name: 'siteInit',
            value: application.data,
          }),
        );
      }
      if (navigation) {
        store.dispatch(
          setRuntimeVariable({
            name: 'mainNavigation',
            value: navigation.data,
          }),
        );
      }
      if (translations) {
        store.dispatch(
          setRuntimeVariable({
            name: 'messages',
            value: translations.data,
          }),
        );
      }
    }

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      insertCss,
      fetch,
      // The twins below are wild, be careful!
      pathname: req.customPath || req.path,
      query: req.query,
      // You can access redux through react-redux connect
      store,
      storeSubscription: null,
      baseUrl,
      rootUrl: baseUrl,
      lang,
    };

    const route = await router.resolve(context);

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };
    data.children = ReactDOM.renderToString(
      <App context={context}>{route.component}</App>,
    );

    data.styles = [{ id: 'css', cssText: [...css].reverse().join('') }];

    const scripts = new Set();
    const addChunk = chunk => {
      if (chunks[chunk]) {
        chunks[chunk].forEach(asset => scripts.add(asset));
      } else if (__DEV__ || __TEST__) {
        throw new Error(`Chunk with name '${chunk}' cannot be found`);
      }
    };
    addChunk('client');
    if (route.chunk) addChunk(route.chunk);
    if (route.chunks) route.chunks.forEach(addChunk);

    data.scripts = Array.from(scripts);
    data.app = {
      baseUrl: context.baseUrl,
      apiUrl: config.api.url,
      state: context.store.getState(),
      lang,
    };
    data.analytics = data.app.state.runtime.analytics;

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);

    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.error(pe.render(err));
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      styles={[{ id: 'css', cssText: errorPageStyle._getCss() }]} // eslint-disable-line no-underscore-dangle
    >
      {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err} />)}
    </Html>,
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
if (!module.hot) {
  app.listen(config.port, () => {
    console.info(`The server is running at http://localhost:${config.port}/`);
  });
}

//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module.hot) {
  app.hot = module.hot;
  module.hot.accept('./router');
}

export default app;
